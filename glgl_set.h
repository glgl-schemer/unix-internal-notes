#ifndef GLGL_SET_H
#define GLGL_SET_H

#include "sub/glgl_tree_base.h"

namespace glglT {

template <typename K, typename Comp = less<K>, typename Alloc = allocator<K>>
class set: public tree_base<set<K, Comp, Alloc>, K, Comp, Alloc> {
    friend class tree_base<set, K, Comp, Alloc>;
    typedef typename tree_base<set, K, Comp, Alloc>::rebind_ptr rebind_ptr;
    typedef typename tree_base<set, K, Comp, Alloc>::rebind_traits rebind_traits;
    typedef typename tree_base<set, K, Comp, Alloc>::insert_return_type insert_return_type;
public:
    typedef Comp value_compare;
    typedef typename tree_base<set, K, Comp, Alloc>::key_compare key_compare;
    typedef typename tree_base<set, K, Comp, Alloc>::key_type key_type;
    typedef typename tree_base<set, K, Comp, Alloc>::value_type value_type;
    typedef typename tree_base<set, K, Comp, Alloc>::size_type size_type;
    typedef typename tree_base<set, K, Comp, Alloc>::iterator iterator;
    typedef typename tree_base<set, K, Comp, Alloc>::const_iterator const_iterator;

    explicit set(const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<set, K, Comp, Alloc>(cp, ac) {}

    explicit set(const Alloc &ac): tree_base<set, K, Comp, Alloc>(ac) {}

    template <typename It>
    set(It b, It e, const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<set, K, Comp, Alloc>(b, e, cp, ac) {}

    set(const set &o): tree_base<set, K, Comp, Alloc>(o) {}

    set(const set &o, const Alloc &ac): tree_base<set, K, Comp, Alloc>(o, ac) {}

    set(set &&o): tree_base<set, K, Comp, Alloc>(glglT::move(o)) {}

    set(set &&o, const Alloc &ac):
        tree_base<set, K, Comp, Alloc>(glglT::move(o), ac) {}

    set(std::initializer_list<K> il,
        const Comp &cp = Comp(), const Alloc &ac = Alloc()):
            tree_base<set, K, Comp, Alloc>(il, cp, ac) {}

    set &operator=(const set &o)
    {
        tree_base<set, K, Comp, Alloc>::operator=(o);
        return *this;
    }

    set &operator=(set &&o)
    {
        tree_base<set, K, Comp, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    set &operator=(std::initializer_list<K> il)
    {
        tree_base<set, K, Comp, Alloc>::operator=(il);
        return *this;
    }

    size_type count(const key_type &k) const
    {
        return this->find(k).base() != this->eot;
    }

    pair<const_iterator, const_iterator> equal_range(const key_type &k) const
    {
        const_iterator itr1 = this->not_smaller(k), itr2 = itr1;
        if (!this->c(k, *itr1))
            ++itr2;
        return glglT::make_pair(glglT::move(itr1), glglT::move(itr2));
    }

    key_compare key_comp() const { this->c; }
    value_compare value_comp() const { this->c; }
private:
    static const key_type &get_key(const value_type &v) noexcept { return v; }
    static iterator get(insert_return_type &&r) noexcept
    {
        return glglT::move(r.first);
    }

    insert_return_type insert_equal(rebind_ptr root, rebind_ptr node)
    {
        if (this->len == 0) {
            ++this->len;
            this->eot->p = this->eot->l = this->eot->r = node;
            node->p = node->l = node->r = this->eot;
            node->is_red = false;
            return glglT::make_pair(iterator(node, this->eot), true);
        } else {
            const auto old_len = this->len;
            this->insert_equal(root, node, !this->c(node->v, root->v));
            return glglT::make_pair(iterator(node, this->eot), old_len != this->len);
        }
    }

    void insert_equal(rebind_ptr root, rebind_ptr &node, bool node_not_smaller)
    {
        for (; ; node_not_smaller = !this->c(node->v, root->v)) {
            const bool node_not_bigger = !this->c(root->v, node->v);
            if (node_not_bigger && node_not_smaller) {
                rebind_traits::destroy(this->a, node);
                rebind_traits::deallocate(this->a, node, 1);
                node = root;
                return;
            } else if (node_not_bigger) {
                if (root->l == this->eot) {
                    ++this->len;
                    root->l = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->r == root)
                        this->eot->r = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->l;
            } else {
                if (root->r == this->eot) {
                    ++this->len;
                    root->r = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->l == root)
                        this->eot->l = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->r;
            }
        }
    }
};

template <typename K, typename C, typename A> inline
void swap(set<K, C, A> &b1, set<K, C, A> &b2)
{
    b1.swap(b2);
}

template <typename K, typename Comp = less<K>, typename Alloc = allocator<K>>
class multiset: public tree_base<multiset<K, Comp, Alloc>, K, Comp, Alloc> {
    friend class tree_base<multiset, K, Comp, Alloc>;
    typedef typename tree_base<multiset, K, Comp, Alloc>::rebind_ptr rebind_ptr;
    typedef typename tree_base<multiset, K, Comp, Alloc>::rebind_traits rebind_traits;
    typedef typename tree_base<multiset, K, Comp, Alloc>::insert_return_type insert_return_type;
public:
    typedef Comp value_compare;
    typedef typename tree_base<multiset, K, Comp, Alloc>::key_compare key_compare;
    typedef typename tree_base<multiset, K, Comp, Alloc>::key_type key_type;
    typedef typename tree_base<multiset, K, Comp, Alloc>::value_type value_type;
    typedef typename tree_base<multiset, K, Comp, Alloc>::size_type size_type;
    typedef typename tree_base<multiset, K, Comp, Alloc>::iterator iterator;
    typedef typename tree_base<multiset, K, Comp, Alloc>::const_iterator const_iterator;

    explicit multiset(const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<multiset, K, Comp, Alloc>(cp, ac) {}

    explicit multiset(const Alloc &ac): tree_base<multiset, K, Comp, Alloc>(ac) {}

    template <typename It>
    multiset(It b, It e, const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<multiset, K, Comp, Alloc>(b, e, cp, ac) {}

    multiset(const multiset &o): tree_base<multiset, K, Comp, Alloc>(o) {}

    multiset(const multiset &o, const Alloc &ac):
        tree_base<multiset, K, Comp, Alloc>(o, ac) {}

    multiset(multiset &&o): tree_base<multiset, K, Comp, Alloc>(glglT::move(o)) {}

    multiset(multiset &&o, const Alloc &ac):
        tree_base<multiset, K, Comp, Alloc>(glglT::move(o), ac) {}

    multiset(std::initializer_list<K> il,
        const Comp &cp = Comp(), const Alloc &ac = Alloc()):
            tree_base<multiset, K, Comp, Alloc>(il, cp, ac) {}

    multiset &operator=(const multiset &o)
    {
        tree_base<multiset, K, Comp, Alloc>::operator=(o);
        return *this;
    }

    multiset &operator=(multiset &&o)
    {
        tree_base<multiset, K, Comp, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    multiset &operator=(std::initializer_list<K> il)
    {
        tree_base<multiset, K, Comp, Alloc>::operator=(il);
        return *this;
    }

    size_type count(const key_type &k) const
    {
        const const_iterator itr = this->find(k);
        if (itr.base() != this->eot) {
            size_type n = 1;
            for (const_iterator prev = itr; (--prev).base() != this->eot; ++n)
                if (this->c(*prev, *itr))
                    break;
            for (const_iterator next = itr; (++next).base() != this->eot; ++n)
                if (this->c(*itr, *next))
                    break;
            return n;
        } else
            return 0;
    }

    pair<const_iterator, const_iterator> equal_range(const key_type &k) const
    {
        return glglT::make_pair(this->not_smaller(k), this->bigger(k));
    }

    key_compare key_comp() const { return this->c; }
    value_compare value_comp() const { return this->c; }
private:
    static iterator get(insert_return_type &&r) noexcept { return glglT::move(r); }
    static const key_type &get_key(const value_type &v) noexcept { return v; }

    insert_return_type insert_equal(rebind_ptr root, rebind_ptr node)
    {
        if (this->len == 0) {
            ++this->len;
            this->eot->p = this->eot->l = this->eot->r = node;
            node->p = node->l = node->r = this->eot;
            node->is_red = false;
        } else
            this->insert_equal(root, node, !this->c(node->v, root->v));
        return iterator(node, this->eot);
    }

    void insert_equal(rebind_ptr root, rebind_ptr &node, bool node_not_smaller)
    {
        for (; ; node_not_smaller = !this->c(node->v, root->v)) {
            if (node_not_smaller) {
                if (root->r == this->eot) {
                    ++this->len;
                    root->r = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->l == root)
                        this->eot->l = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->r;
            } else {
                if (root->l == this->eot) {
                    ++this->len;
                    root->l = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->r == root)
                        this->eot->r = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->l;
            }
        }
    }
};

template <typename K, typename C, typename A> inline
void swap(multiset<K, C, A> &b1, multiset<K, C, A> &b2)
{
    b1.swap(b2);
}

} // namespace glglT

#endif // GLGL_SET_H
