#ifndef GLGL_DISJOINT_SET_H
#define GLGL_DISJOINT_SET_H

#include "sub/glgl_utility_function.h"
#include "sub/glgl_function_object.h"

namespace glglT {

template <typename Iter, typename EquivIter, typename RankT>
class disjoint_set_v {
    EquivIter equiv;
public:
    struct vertex_type {
        Iter data_iter;
        RankT rank;
        vertex_type *parent, *next;
    };

    explicit disjoint_set_v(const EquivIter &c): equiv(c) {}

    template <typename ForwardIter, typename OutputIter>
    void make_set(Iter data_iter, ForwardIter fiter, OutputIter oiter) const
    {
        *oiter = vertex_type{data_iter, RankT(), nullptr, nullptr};
        fiter->parent = fiter->next = glglT::addressof(*fiter);
    }

    bool is_equal(const vertex_type &v1, const vertex_type &v2) const
    {
        return this->equiv(v1.data_iter, v2.data_iter);
    }

    vertex_type &find_set(vertex_type &v) const
    {
        vertex_type *root = v.parent;
        while (!this->equiv(root->data_iter, root->parent->data_iter))
            root = root->parent;
        if (v.parent != root) {
            vertex_type *curr = v.parent;
            v.parent = root;
            while (curr->parent != v.parent) {
                root = curr->parent;
                curr->parent = v.parent;
                curr = root;
            }
        }
        return *(v.parent);
    }

    void union_set(vertex_type &v1, vertex_type &v2) const
    {
        this->link(this->find_set(v1), this->find_set(v2));
    }

    template <typename OutputIter, typename Getter>
    OutputIter copy_set(vertex_type &v, OutputIter oiter, Getter getter) const
    {
        const vertex_type *iiter = v.parent;
        do {
            *oiter++ = getter(iiter->data_iter);
            iiter = iiter->next;
        } while (iiter != v.parent);
        return oiter;
    }
private:
    void link(vertex_type &v1, vertex_type &v2) const
    {
        if ((v1.rank) < (v2.rank)) {
            v1.parent = &v2;
        } else {
            if (v1.rank == v2.rank)
                ++(v1.rank);
            v2.parent = &v1;
        }
        using glglT::swap;
        swap(v1.next, v2.next);
    }
};

template <typename RankRF, typename RankWF, typename ParentRF, typename ParentWF>
class disjoint_set_f {
    RankRF rank_getter;
    RankWF rank_setter;
    ParentRF parent_getter;
    ParentWF parent_setter;
public:
    disjoint_set_f(const RankRF &rrf, const RankWF &rwf,
                   const ParentRF &prf, const ParentWF &pwf):
        rank_getter(rrf), rank_setter(rwf), parent_getter(prf), parent_setter(pwf) {}

    template <typename RankType, typename ElementType>
    void make_set(const ElementType &e) const
    {
        this->rank_setter(e, RankType());
        this->parent_setter(e, e);
    }

    template <typename ElementType, typename Equiv = glglT::equal_to<ElementType>>
    ElementType find_set(ElementType e, Equiv equiv = Equiv()) const
    {
        ElementType root = this->parent_getter(e), curr = root;
        while (!equiv(root, e)) {
            e = root;
            root = this->parent_getter(root);
        }
        while (!equiv(curr, root)) {
            e = this->parent_getter(curr);
            this->parent_setter(curr, root);
            curr = glglT::move(e);
        }
        return root;
    }

    template <typename ElementType, typename Equiv = glglT::equal_to<ElementType>>
    void union_set(const ElementType &e1, const ElementType &e2, Equiv equiv = Equiv()) const
    {
        this->link(this->find_set(e1, equiv), this->find_set(e2, equiv));
    }
private:
    template <typename ElementType>
    void link(const ElementType &e1, const ElementType &e2) const
    {
        auto r1 = this->rank_getter(e1), r2 = this->rank_getter(e2);
        if (r1 < r2) {
            this->parent_setter(e1, e2);
        } else {
            if (r1 == r2)
                this->rank_setter(e1, glglT::move(++r1));
            this->parent_setter(e2, e1);
        }
    }
};

template <typename RRF, typename RWF, typename PRF, typename PWF> inline
disjoint_set_f<RRF, RWF, PRF, PWF>
make_disjoint_set_handle_by_functions(const RRF &rrf, const RWF &rwf,
                                      const PRF &prf, const PWF &pwf)
{
    return disjoint_set_f<RRF, RWF, PRF, PWF>(rrf, rwf, prf, pwf);
}


} // namespace glglT

#endif // GLGL_DISJOINT_SET_H
