#ifndef GLGL_234_HEAP_H
#define GLGL_234_HEAP_H

#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_memory_tool.h"
#include "sub/glgl_function_object.h"
#include "sub/glgl_mutable_heap_base.h"

namespace glglT {

template <typename Ptr>
struct _2_3_4_node_t {
    size_t height;
    _2_3_4_node_t *p_or_l, *r;
    Ptr m;
    _2_3_4_node_t *c;

    _2_3_4_node_t() noexcept {}
    explicit _2_3_4_node_t(Ptr p): m(p) {}
};

template <typename T, typename Ptr>
class _2_3_4_const_iterator: public iterator<bidirectional_iterator_tag, const T> {
    _2_3_4_node_t<Ptr> *curr;
    _2_3_4_node_t<Ptr> *eoh;
public:
    constexpr _2_3_4_const_iterator() noexcept {}
    _2_3_4_const_iterator(_2_3_4_node_t<Ptr> *c, _2_3_4_node_t<Ptr> *h):
        curr(c), eoh(h) {}

    const T &operator*() const noexcept { return *(curr->m); }
    const T *operator->() const noexcept { return curr->m; }
    _2_3_4_node_t<Ptr> * const &base() const noexcept { return curr; }
    _2_3_4_const_iterator &operator++() noexcept
    {
        if (curr != eoh)
            while (curr->r->p_or_l != curr && curr->r != eoh)
                curr = curr->r->p_or_l;
        curr = curr->r;
        while (curr->height)
            curr = curr->c;
        return *this;
    }

    _2_3_4_const_iterator &operator--() noexcept
    {
        if (curr != eoh)
            while (curr->p_or_l->r != curr && curr->p_or_l != eoh)
                curr = curr->p_or_l;
        curr = curr->p_or_l;
        while (curr->height) {
            curr = curr->c;
            while (curr->r->p_or_l == curr)
                curr = curr->r;
        }
        return *this;
    }

    _2_3_4_const_iterator operator++(int) noexcept
    {
        _2_3_4_const_iterator r = *this;
        this->operator++();
        return r;
    }

    _2_3_4_const_iterator operator--(int) noexcept
    {
        _2_3_4_const_iterator r = *this;
        this->operator--();
        return r;
    }

    bool operator==(const _2_3_4_const_iterator &r) const noexcept
    {
        return curr == r.curr;
    }

    bool operator!=(const _2_3_4_const_iterator &r) const noexcept
    {
        return curr != r.curr;
    }
};

template <typename T, typename Comp = less<T>, typename Alloc = allocator<T>>
class _2_3_4_heap: public mutable_heap_base<T, Comp, Alloc, _2_3_4_heap<T, Comp, Alloc>> {
    friend class mutable_heap_base<T, Comp, Alloc, _2_3_4_heap>;
    typedef typename heap_iter_adaptor<_2_3_4_heap>::rebind_alloc rebind_alloc;
    typedef typename heap_iter_adaptor<_2_3_4_heap>::rebind_traits rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    struct move_tag{};

    Comp comp;
    rebind_alloc a;
    _2_3_4_node_t<rebind_ptr> *eoh;
    size_t len;
public:
    typedef _2_3_4_const_iterator<T, rebind_ptr> iterator;
    typedef iterator const_iterator;

    explicit _2_3_4_heap(const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(_2_3_4_heap::make_node()), len() { this->init_heap(); }

    explicit _2_3_4_heap(const Alloc &ac):
        comp(), a(ac), eoh(_2_3_4_heap::make_node()), len() { this->init_heap(); }

    template <typename It>
    _2_3_4_heap(It b, It e, const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        this->push(b, e);
    }

    template <typename It, typename OutputIter>
    _2_3_4_heap(It b, It e, OutputIter oiter,
                const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        this->push(b, e, oiter);
    }

    _2_3_4_heap(const _2_3_4_heap &o): comp(o.comp),
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), glglT::ignore, o.top());
    }

    template <typename OutputIter>
    _2_3_4_heap(const _2_3_4_heap &o, OutputIter oiter): comp(o.comp),
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), oiter, o.top());
    }

    template <typename OutputIter = tuple<>>
    _2_3_4_heap(const _2_3_4_heap &o, const Alloc &ac, OutputIter may = glglT::ignore):
        comp(o.comp), a(ac), eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top());
    }

    _2_3_4_heap(_2_3_4_heap &&o): comp(glglT::move(o.comp)),
        a(glglT::move(o.a)), eoh(_2_3_4_heap::make_node()), len(o.len)
    {
        this->init_heap();
        glglT::swap(eoh, o.eoh);
        o.len = 0;
    }

    template <typename OutputIter = tuple<>>
    _2_3_4_heap(_2_3_4_heap &&o, const Alloc &ac, OutputIter may = glglT::ignore):
        comp(glglT::move(o.comp)), a(ac), eoh(_2_3_4_heap::make_node()), len()
    {
        this->init_heap();
        if (a == o.a) {
            glglT::swap(len, o.len);
            glglT::swap(eoh, o.eoh);
        } else if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top(), move_tag());
    }

    _2_3_4_heap(std::initializer_list<T> il,
                const Comp &c = Comp(), const Alloc &ac = Alloc()):
        _2_3_4_heap(il.begin(), il.end(), c, ac) {}

    template <typename OutputIter>
    _2_3_4_heap(std::initializer_list<T> il, OutputIter oiter,
                const Comp &c = Comp(), const Alloc &ac = Alloc()):
        _2_3_4_heap(il.begin(), il.end(), oiter, c, ac) {}

    ~_2_3_4_heap() { this->free(); delete eoh; }

    _2_3_4_heap &operator=(const _2_3_4_heap &o)
    {
        this->assign(o);
        return *this;
    }

    _2_3_4_heap &operator=(_2_3_4_heap &&o)
    {
        this->assign(glglT::move(o));
        return *this;
    }

    _2_3_4_heap &operator=(std::initializer_list<T> il)
    {
        this->assign(il.begin(), il.end());
        return *this;
    }

    template <typename OutputIter = tuple<>>
    void assign(std::initializer_list<T> il, OutputIter may = glglT::ignore)
    {
        this->assign(il.begin(), il.end(), may);
    }

    template <typename It, typename OutputIter = tuple<>>
    void assign(It b, It e, OutputIter may = glglT::ignore)
    {
        this->clear();
        this->push(b, e, may);
    }

    template <typename OutputIter = tuple<>>
    void assign(const _2_3_4_heap &o, OutputIter may = glglT::ignore)
    {
        if (eoh == o.eoh)
            return;
        this->clear();
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        comp = o.comp;
        if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top());
    }

    template <typename OutputIter = tuple<>>
    void assign(_2_3_4_heap &&o, OutputIter may = glglT::ignore)
    {
        if (eoh == o.eoh)
            return;
        this->clear();
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        comp = o.comp;
        if (a != o.a) {
            glglT::swap(len, o.len);
            glglT::swap(eoh, o.eoh);
        } else if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top(), move_tag());
    }

    const_iterator begin() const noexcept { return iterator(eoh->r, eoh); }
    const_iterator end() const noexcept { return iterator(eoh, eoh); }

    bool empty() const noexcept { return len == 0; }
    size_t size() const noexcept { return len; }
    const T &top() const noexcept { return *(eoh->m); }

    template <typename It, typename OutputIter = tuple<>>
    void push(It b, It e, OutputIter may = glglT::ignore)
    {
        *may++ = this->emplace(*b);
        for (; ++b != e; ++len) {
            const auto node = this->make_leaf_node(*b);
            try {
                this->insert_in_non_empty_heap(node, eoh->r,
                                               this->comp(*(node->m), *(eoh->m)));
            } catch(...) {
                this->free_leaf(node);
                throw;
            }
            *may++ = iterator(node, eoh);
        }
    }

    template <typename... Args>
    iterator emplace(Args&&... args)
    {
        const auto node = this->make_leaf_node(glglT::forward<Args>(args)...);
        if (len == 0) {
            eoh->p_or_l = eoh->r = eoh->c = node;
            eoh->m = node->m;
            node->p_or_l = node->r = eoh;
        } else try {
            this->insert_in_non_empty_heap(node, eoh->r,
                                           this->comp(*(node->m), *(eoh->m)));
        } catch(...) {
            this->free_leaf(node);
            throw;
        }   ++len;
        return iterator(node, eoh);
    }

    iterator erase(const_iterator itr)
    {
        const auto ori_m = itr.base()->m;
        const auto is_left = eoh->r == itr.base();
        const auto is_right = eoh->p_or_l == itr.base();
        auto tmp = itr;
        if (is_right)
            --tmp;
        this->check_merge(itr++.base(), ori_m, ori_m == eoh->m, true);
        rebind_traits::destroy(a, ori_m);
        rebind_traits::deallocate(a, ori_m, 1);
        --len;
        if (is_left)
            eoh->r = itr.base();
        if (is_right)
            eoh->p_or_l = tmp.base();
        return itr;
    }

    void pop()
    {
        auto node = eoh->c;
        for (; node->m != eoh->m; node = node->r);
        for (; node->height; )
            for (node = node->c; node->m != eoh->m; node = node->r);
        this->erase(iterator(node, eoh));
    }

    void clear() noexcept
    {
        this->free();
        eoh->p_or_l = eoh->r = eoh->c = eoh;
        len = 0;
    }

    template <typename... Args>
    void update(const_iterator itr, Args&&... args)
    {
        T t(glglT::forward<Args>(args)...);
        if (this->comp(t, *(itr.base()->m)))
            this->decrease_key(itr.base(), &t, itr.base()->m);
        else if (this->comp(*(itr.base()->m), t))
            this->increase_key(itr.base(), &t, itr.base()->m);
        else return;
        *(itr.base()->m) = glglT::move(t);
    }

    template <typename... Args>
    void decrease(const_iterator itr, Args&&... args)
    {
        T t(glglT::forward<Args>(args)...);
        this->decrease_key(itr.base(), &t, itr.base()->m);
        *(itr.base()->m) = glglT::move(t);
    }

    template <typename... Args>
    void increase(const_iterator itr, Args&&... args)
    {
        T t(glglT::forward<Args>(args)...);
        this->increase_key(itr.base(), &t, itr.base()->m);
        *(itr.base()->m) = glglT::move(t);
    }

    void merge(_2_3_4_heap &&o) { this->merge(o); }
    void merge(_2_3_4_heap &o)
    {
        if (o.len == 0)
            return;
        if (len == 0)
            *this = glglT::move(o);
        else if (a == o.a) {
            const auto o_is_min = this->comp(*(o.eoh->m), *(eoh->m));
            if (eoh->c->height < o.eoh->c->height) {
                o.merge_low_to_high(*this, !o_is_min);
                *this = glglT::move(o);
            } else
                this->merge_low_to_high(o, o_is_min);
        } else
            this->p_insert(o.begin(), o.end(), glglT::ignore, o.top(), move_tag());
    }

    void swap(_2_3_4_heap &o)
    {
        using glglT::swap;
        glglT::alloc_swap(a, o.a,
                          typename rebind_traits::propagate_on_container_swap());
        swap(comp, o.comp);
        glglT::swap(eoh, o.eoh);
        glglT::swap(len, o.len);
    }
private:
    void init_heap() noexcept
    {
        eoh->p_or_l = eoh->r = eoh->c = eoh;
        eoh->height = 0;
    }

    template <typename It, typename OutputIter, typename... MoveTag>
    void p_insert(It b, It e, OutputIter oiter, const T &tmin, MoveTag... movetag)
    {
        *oiter++ = this->emplace(*b);
        const auto may_min = this->comp(tmin, *(eoh->m));
        for (; ++b != e; ++len) {
            const auto node = this->make_leaf_node_from_it(b, movetag...);
            try {
                this->insert_in_non_empty_heap(node, eoh->r, may_min &&
                                               this->comp(*(node->m), *(eoh->m)));
            } catch(...) {
                this->free_leaf(node);
                throw;
            }
            *oiter++ = iterator(node, eoh);
        }
    }

    template <typename It>
    _2_3_4_node_t<rebind_ptr> *make_leaf_node_from_it(It it)
    {
        return this->make_leaf_node(*it);
    }

    template <typename It>
    _2_3_4_node_t<rebind_ptr> *make_leaf_node_from_it(It it, move_tag)
    {
        return this->make_leaf_node(glglT::move(*it));
    }

    void insert_in_non_empty_heap(_2_3_4_node_t<rebind_ptr> *node,
                                  _2_3_4_node_t<rebind_ptr> *b, bool is_min)
    {
        this->check_split(node, b, is_min, true);
        eoh->r = node;
    }

    void check_split(_2_3_4_node_t<rebind_ptr> *node,
                     _2_3_4_node_t<rebind_ptr> *b, bool is_min, bool node_new)
    {
        const auto old_p = node_new ? b->p_or_l : node->p_or_l;
        const auto old_p_m = old_p->m;
        const auto is_highest = old_p == eoh;
        const auto node_min = is_min || (this->comp(*(node->m), *old_p_m));
        const auto overflow = node_new &&
            ((is_highest ? this->length_in_root(b) : _2_3_4_heap::length(b)) == 4);
        if (overflow) {
            const auto b_old_m = b->m == old_p_m;
            const auto b_new_m = !node_min && (b_old_m ||
                                               this->comp(*(b->m), *(node->m)));
            auto mid = b->r;
            const auto old_which =// !b_old_m && (old_p_m != node->m) ? 0 :
                (this->comp(*(mid->r->m), *(mid->m))
                 ? (this->comp(*(mid->r->r->m), *(mid->r->m)) ? 3 : 2)
                 : (this->comp(*(mid->r->r->m), *(mid->m)) ? 3 : 1));
            const auto new_p = _2_3_4_heap::make_node();
            auto b_p = old_p;
            if (is_highest)
                try {
                    b_p = _2_3_4_heap::make_node();
                    b_p->height = eoh->c->height + 1;
                    b_p->m = eoh->m;
                } catch(...) {
                    delete new_p;
                    throw;
                }
            new_p->m = b_new_m ? b->m : node->m;
            switch (old_which) {
            case 0: break;
            case 3: mid = mid->r;
            case 2: mid = mid->r;
            case 1:
                b_p->m = mid->m;
            }
            if (!is_highest) {
                try {
                    this->check_split(new_p, old_p, is_min, true);
                } catch(...) {
                    old_p->m = old_p_m;
                    delete new_p;
                    throw;
                }
            } else {
                try {
                    const auto is_new = is_min || this->comp(*(new_p->m), *(b_p->m));
                    eoh->m = is_new ? new_p->m : b_p->m;
                } catch(...) {
                    delete new_p;
                    delete b_p;
                    throw;
                }
                new_p->p_or_l = b_p->r = eoh;
                new_p->r = b_p;
                b_p->p_or_l = eoh->c = new_p;
            }
            b_p->c = b->r;
            b->r->r->r->r = b->r;
            b->r->p_or_l = b_p;
            b->r = node;
            node->r = b;
            b->p_or_l = node;
            node->p_or_l = new_p;
            new_p->c = node;
            new_p->height = b_p->height;
        } else {
            if (node_min)
                old_p->m = node->m;
            if (!is_highest && node_min)
                try {
                    this->check_split(old_p, old_p->r, is_min, false);
                } catch(...) {
                    old_p->m = old_p_m;
                    throw;
                }
            if (node_new) {
                b->p_or_l = node;
                node->r = b;
                old_p->c = node;
                node->p_or_l = old_p;
                if (!is_highest) {
                    for (; b->r->p_or_l == b; b = b->r);
                    b->r = node;
                }
            }
        }
    }

    void check_merge(_2_3_4_node_t<rebind_ptr> *node, rebind_ptr ori_m,
                     bool is_min, bool need_rm)
    {
        auto p = node->p_or_l;
        for (auto b = node; p->r == b && p != eoh; b = p, p = p->p_or_l);
        const auto p_ori_m = p->m;
        const auto need_change_p_m = is_min || p->m == ori_m;
        if (need_change_p_m) {
            auto new_m = need_rm ? node->r : node;
            if (new_m == eoh)
                new_m = eoh->c;
            for (auto b = new_m->r; ; b = b->r) {
                if (b == eoh)
                    b = eoh->c;
                if (b == node)
                    break;
                if (this->comp(*(b->m), *(new_m->m)))
                    new_m = b;
            }   p->m = new_m->m;
        }
        if (p == eoh) {
            if (need_rm) {
                if (node->r != eoh)
                    node->r->p_or_l = node->p_or_l;
                if (node->p_or_l == eoh) {
                    eoh->c = node->r;
                    if (node->r == eoh)
                        eoh->r = eoh->p_or_l = eoh;
                } else
                    node->p_or_l->r = node->r;
                delete node;
            }   return;
        }
        const auto p_lack_child = need_rm && _2_3_4_heap::length(node) == 2;
        const auto u = p->r == eoh ? p->r->c : p->r;
        const auto u_lack_child = p_lack_child && _2_3_4_heap::length(u->c) == 2;
        const auto p_need_rm = p_lack_child && u_lack_child;
        const auto u_ori_m = u->m;
        if (p_lack_child) {
            if (!u_lack_child) {
                if (u->m == u->c->m)
                    for (auto new_m = u->c->r, b = new_m->r; ; b = b->r) {
                        if (b->m == u->m) {
                            u->m = new_m->m;
                            break;
                        }
                        if (this->comp(*(b->m), *(new_m->m)))
                            new_m = b;
                    }
                if (this->comp(*(u->c->m), *(p->m)))
                    p->m = u->c->m;
            } else if (this->comp(*(p->m), *(u->m)))
                u->m = p->m;
        }
        if (need_change_p_m || p_need_rm)
            try {
                this->check_merge(p, ori_m, is_min, p_need_rm);
            } catch(...) {
                p->m = p_ori_m;
                u->m = u_ori_m;
                throw;
            }
        if (!need_rm)
            return;
        if (p_lack_child) {
            if (u_lack_child) {
                node->r->r = u->c;
                u->c->p_or_l = node->r;
                u->c->r->r = node->r;
                u->c = node->r;
                node->r->p_or_l = u;
                if (eoh->c == u && u->r == eoh) {
                    eoh->c = u->c;
                    u->c->p_or_l = eoh;
                    u->c->r->r->r = eoh;
                    delete u;
                }
            } else {
                auto b = u->c->r;
                b->p_or_l = u;
                for (; b->r->p_or_l == b; b = b->r);
                b->r = u->c->r;
                b = u->c;
                u->c = u->c->r;
                b->r = node->r;
                if (node->p_or_l == p)
                    node->p_or_l->c = node->r->r = node->r->p_or_l = b;
                else
                    node->p_or_l->r = b;
                b->p_or_l = node->p_or_l;
            }
        } else {
            if (node->r->p_or_l == node)
                node->r->p_or_l = node->p_or_l;
            if (node->p_or_l == p) {
                node->p_or_l->c = node->r;
                auto b = node->r;
                for (; b->r->p_or_l == b; b = b->r);
                b->r = node->r;
            } else
                node->p_or_l->r = node->r;
        }   delete node;
    }

    void merge_low_to_high(_2_3_4_heap &o, bool o_is_min)
    {
        auto b = eoh->r, node = o.eoh->c;
        while (b->height != node->height)
            b = b->p_or_l;
        for (auto o_rest_m = o.eoh->m, n = node->r; ; b = node, node = n, n = n->r) {
            const auto no_more = n == o.eoh;
            if (o_rest_m == node->m && !no_more) {
                auto n_rest_m = n->m;
                for (auto it = n->r; it != o.eoh; it = it->r)
                    try {
                        if (this->comp(*(it->m), *n_rest_m))
                            n_rest_m = it->m;
                    } catch(...) {
                        this->rev_when_merge(o, node, o_rest_m);
                        throw;
                    }
                o_rest_m = n_rest_m;
            }
            try {
                this->check_split(node, b, o_is_min &&
                                  (node->m == o.eoh->m ||
                                   this->comp(*(node->m), *(eoh->m))), true);
            } catch (...) {
                node->r = n;
                this->rev_when_merge(o, node, o_rest_m);
                throw;
            }
            if (no_more)
                break;
        }
        for (; node->height; node = node->c);
        eoh->r = node;
        len += o.len;
        o.len = 0;
        o.eoh->p_or_l = o.eoh->r = o.eoh->c = o.eoh;
    }

    void rev_when_merge(_2_3_4_heap &o, _2_3_4_node_t<rebind_ptr> *node,
                        rebind_ptr o_rest_m) noexcept
    {
        o.eoh->m = o_rest_m;
        o.eoh->c = node;
        node->p_or_l = o.eoh;
        for (; node->height; node = node->c);
        o.eoh->r = node;
        size_t o_sz = 0;
        for (auto it = o.begin(), eit = o.end(); it != eit; ++it, ++o_sz);
        len += o.len - o_sz;
        o.len = o_sz;
        for(node = eoh->c; node->height; node = node->c);
        eoh->r = node;
    }

    void increase_key(_2_3_4_node_t<rebind_ptr> *node, T *t, rebind_ptr ori_m)
    {
        auto p = node->p_or_l;
        for (auto b = node; p->r == b && p != eoh; b = p, p = p->p_or_l);
        if (p->m != ori_m)
            return;
        p->m = node->m;
        const auto new_m_eoh = node->r == eoh;
        for (auto new_m = new_m_eoh ? eoh->c : node->r, b = new_m->r; ; b = b->r) {
            if (b == eoh)
                b = eoh->c;
            if (b == node) {
                if (this->comp(*(new_m->m), *t)) {
                    p->m = new_m->m;
                    t = glglT::addressof(*(new_m->m));
                }   break;
            }
            if (this->comp(*(b->m), *(new_m->m)))
                new_m = b;
        }
        if (p == eoh)
            return;
        try {
            this->increase_key(p, t, ori_m);
        } catch(...) {
            p->m = ori_m;
            throw;
        }
    }

    void decrease_key(_2_3_4_node_t<rebind_ptr> *node, T *t, rebind_ptr ori_m)
    {
        auto p = node->p_or_l;
        for (auto b = node; p->r == b && p != eoh; b = p, p = p->p_or_l);
        if (p->m != ori_m) {
            if (!this->comp(*t, *(p->m)))
                return;
            ori_m = p->m;
            p->m = node->m;
        } else if (node->m != ori_m)
            p->m = node->m;
        if (p == eoh)
            return;
        try {
            this->decrease_key(p, t, ori_m);
        } catch(...) {
            p->m = ori_m;
            throw;
        }
    }

    template <typename... Args>
    _2_3_4_node_t<rebind_ptr> *make_leaf_node(Args&&... args)
    {
        const auto tp = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, tp, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, tp, 1);
            throw;
        }
        try {
            const auto p = _2_3_4_heap::make_node(tp);
            p->height = 0;
            return p;
        } catch(...) {
            rebind_traits::destroy(a, tp);
            rebind_traits::deallocate(a, tp, 1);
            throw;
        }
    }

    void free_leaf(_2_3_4_node_t<rebind_ptr> *leaf) noexcept
    {
        rebind_traits::destroy(a, leaf->m);
        rebind_traits::deallocate(a, leaf->m, 1);
        delete leaf;
    }

    void free() noexcept
    {
        for (_2_3_4_node_t<rebind_ptr> *p = eoh->c, *next, *tmp; p != eoh; p = next) {
            if (p->height) {
                next = tmp = p->c;
                while (tmp->r != next)
                    tmp = tmp->r;
                tmp->r = p->r;
            } else {
                next = p->r;
                rebind_traits::destroy(a, p->m);
                rebind_traits::deallocate(a, p->m, 1);
            }   delete p;
        }
    }

    static int length(_2_3_4_node_t<rebind_ptr> *node) noexcept
    {
        const auto mid = node->r->r;
        if (mid == node)
            return 2;
        return 3 + (mid->r != node);
    }

    int length_in_root(_2_3_4_node_t<rebind_ptr> *node) noexcept
    {
        int i = 1;
        for (auto p = node->r; p != eoh; p = p->r, ++i);
        return i;
    }

    template <typename... Ptr> static
    _2_3_4_node_t<rebind_ptr> *make_node(Ptr&&... ptr)
    {
        return new _2_3_4_node_t<rebind_ptr>(glglT::forward<Ptr>(ptr)...);
    }
};

template <typename T, typename C, typename A> inline
void swap(_2_3_4_heap<T, C, A> &h1, _2_3_4_heap<T, C, A> &h2)
{
    h1.swap(h2);
}

} // namespace glglT

#endif // GLGL_234_HEAP_H
