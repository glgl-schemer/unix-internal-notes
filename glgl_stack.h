#ifndef GLGL_STACK_H
#define GLGL_STACK_H

#include "sub/glgl_container_adaptor.h"

namespace glglT {

template <typename, typename> class deque;
template <typename, typename> class stack;

template <typename T, typename C>
bool operator==(const stack<T, C> &, const stack<T, C> &);

template <typename T, typename C>
bool operator<(const stack<T, C> &, const stack<T, C> &);

template <typename T, typename C, typename A>
struct uses_allocator<stack<T, C>, A>: uses_allocator<C, A>::type {};

template <typename T, typename C = deque<T, allocator<T>>>
struct stack: container_adaptor_base<C> {
    friend bool operator==<>(const stack &, const stack &);
    friend bool operator< <>(const stack &, const stack &);
protected:
    C c;
public:
    explicit stack(const C &cn): c(cn) {}
    explicit stack(C &&cn = C()): c(glglT::move(cn)) {}
    stack(const stack &) = default;
    stack(stack &&) = default;
    stack &operator=(const stack &) = default;
    stack &operator=(stack &&) = default;

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    explicit stack(const Alloc &a): c(a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    stack(const C &cn, const Alloc &a): c(cn, a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    stack(C &&cn, const Alloc &a): c(glglT::move(cn), a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    stack(const stack &o, const Alloc &a):
        c(o.c, a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    stack(stack &&o, const Alloc &a):
        c(glglT::move(o.c), a) {}

    bool empty() const noexcept { return c.empty(); }
    decltype(c.size()) size() const noexcept { return c.size(); }

    decltype(declval<C &>().back()) top() noexcept(noexcept(declval<C &>().back()))
    {
        return c.back();
    }

    decltype(declval<const C &>().back()) top() const noexcept(noexcept(declval<const C &>().back()))
    {
        return c.back();
    }

    void push(const typename C::value_type &v) { c.push_back(v); }
    void push(typename C::value_type &&v) { c.push_back(glglT::move(v)); }
    void pop() { this->c.pop_back(); }

    template <typename... Args> void emplace(Args&&... args)
    {
        c.emplace_back(glglT::forward<Args>(args)...);
    }

    void swap(stack &o)
    {
        using glglT::swap;
        swap(c, o.c);
    }
};

template <typename T, typename C> inline
bool operator==(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return s1.c == s2.c;
}

template <typename T, typename C> inline
bool operator<(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return s1.c < s2.c;
}

template <typename T, typename C> inline
bool operator!=(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return !(s1 == s2);
}

template <typename T, typename C> inline
bool operator>=(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return !(s1 < s2);
}

template <typename T, typename C> inline
bool operator>(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return s2 < s1;
}

template <typename T, typename C> inline
bool operator<=(const stack<T, C> &s1, const stack<T, C> &s2)
{
    return !(s1 > s2);
}

template <typename T, typename C> inline
void swap(stack<T, C> &s1, stack<T, C> &s2)
{
    s1.swap(s2);
}

} // namespace glglT

#endif // GLGL_STACK_H
