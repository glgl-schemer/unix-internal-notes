#ifndef GLGL_LIST_H
#define GLGL_LIST_H

#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_allocator_traits.h"
#include "sub/glgl_base_algo.h"
#include "sub/glgl_other_iterator.h"

namespace glglT {

template <typename T, typename Alloc>
struct list_use_node_t {
    list_use_node_t() = default;
    template <typename ...Args>
    explicit list_use_node_t(Args&&... args): t(glglT::forward<Args>(args)...) {}
    T t;
    typename allocator_traits<Alloc>::
        template rebind_traits<list_use_node_t>::pointer prev;
    typename allocator_traits<Alloc>::
        template rebind_traits<list_use_node_t>::pointer next;
};

template <typename, typename> class list;

template <typename It, typename T, typename RPtr>
class list_use_iterator_base {
    template <typename, typename> friend class list;
    template <typename, typename, typename> friend class list_use_iterator_base;
protected:
    RPtr curr;

    constexpr list_use_iterator_base() noexcept {}
    explicit list_use_iterator_base(RPtr rp) noexcept: curr(rp) {}
    T &operator*() const noexcept { return curr->t; }
    T *operator->() const noexcept { return &(curr->t); }
public:
    const RPtr &base() const noexcept { return curr; }
    It &operator++() noexcept
    {
        curr = curr->next;
        return static_cast<It &>(*this);
    }

    It &operator--() noexcept
    {
        curr = curr->prev;
        return static_cast<It &>(*this);
    }

    It operator++(int) noexcept
    {
        It tmp(curr);
        curr = curr->next;
        return tmp;
    }

    It operator--(int) noexcept
    {
        It tmp(curr);
        curr = curr->prev;
        return tmp;
    }

    template <typename OIt>
    bool operator==(const list_use_iterator_base<OIt, T, RPtr> &r) const noexcept
    {
        return curr == r.curr;
    }
};

template <typename It1, typename It2, typename T, typename P> inline
bool operator!=(const list_use_iterator_base<It1, T, P> &r1,
                const list_use_iterator_base<It2, T, P> &r2) noexcept
{
    return !(r1 == r2);
}

template <typename, typename> class list_use_iterator;

template <typename T, typename RPtr>
struct list_use_const_iterator:
    list_use_iterator_base<list_use_const_iterator<T, RPtr>, T, RPtr>,
    iterator<bidirectional_iterator_tag, const T> {
    template <typename, typename> friend class list;

    constexpr list_use_const_iterator() noexcept {}
    explicit list_use_const_iterator(RPtr rp) noexcept:
        list_use_iterator_base<list_use_const_iterator, T, RPtr>(rp) {}

    const T &operator*() const noexcept
    {
        return list_use_iterator_base<list_use_const_iterator, T, RPtr>::
            operator*();
    }

    const T *operator->() const noexcept
    {
        return list_use_iterator_base<list_use_const_iterator, T, RPtr>::
            operator->();
    }

    explicit operator list_use_iterator<T, RPtr> () const noexcept;
};

template <typename T, typename RPtr>
struct list_use_iterator:
    list_use_iterator_base<list_use_iterator<T, RPtr>, T, RPtr>,
    iterator<bidirectional_iterator_tag, T> {
    template <typename, typename> friend class list;

    constexpr list_use_iterator() noexcept {}
    explicit list_use_iterator(RPtr rp) noexcept:
        list_use_iterator_base<list_use_iterator, T, RPtr>(rp) {}

    T &operator*() const noexcept
    {
        return list_use_iterator_base<list_use_iterator, T, RPtr>::
            operator*();
    }

    T *operator->() const noexcept
    {
        return list_use_iterator_base<list_use_iterator, T, RPtr>::
            operator->();
    }

    operator list_use_const_iterator<T, RPtr> () const noexcept
    {
        return list_use_const_iterator<T, RPtr>(this->curr);
    }
};

template <typename T, typename P> inline
list_use_const_iterator<T, P>::operator list_use_iterator<T, P> () const noexcept
{
    return list_use_iterator<T, P>(this->curr);
}

template <typename T, typename Alloc = allocator<T>>
class list {
    typedef typename allocator_traits<Alloc>::
        template rebind_alloc<list_use_node_t<T, Alloc>> rebind_alloc;
    typedef typename allocator_traits<Alloc>::
        template rebind_traits<list_use_node_t<T, Alloc>> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    static constexpr size_t SL_LIM = 16;

    rebind_alloc a;
    rebind_ptr eol;
    size_t len;
public:
    typedef T value_type;
    typedef Alloc allcator_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef list_use_iterator<T, rebind_ptr> iterator;
    typedef list_use_const_iterator<T, rebind_ptr> const_iterator;
    typedef glglT::reverse_iterator<iterator> reverse_iterator;
    typedef glglT::reverse_iterator<const_iterator> const_reverse_iterator;

    explicit list(const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
    }

    list(size_type n, const T &t, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        this->insert(const_iterator(eol), n, t);
    }

    explicit list(size_type n): a(), eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        while (n-- != 0)
            this->emplace_back();
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    list(It b, It e, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        this->insert(const_iterator(eol), b, e);
    }


    list(const list &o):
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        this->insert(const_iterator(eol), o.begin(), o.end());
    }

    list(const list &o, const Alloc &ac): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        this->insert(const_iterator(eol), o.begin(), o.end());
    }

    list(list &&o): a(glglT::move(o.a)),
        eol(rebind_traits::allocate(a, 1)), len(o.len)
    {
        eol->prev = eol;
        eol->next = eol;
        glglT::swap(eol, o.eol);
    }

    list(list &&o, const Alloc &ac): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        if (a == o.a) {
            glglT::swap(eol, o.eol);
            len = o.len;
        } else
            this->insert_move(const_iterator(eol), o.begin(), o.end());
    }

    list(std::initializer_list<T> il, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->prev = eol;
        eol->next = eol;
        this->insert(const_iterator(eol), il.begin(), il.end());
    }

    ~list() { this->free(); rebind_traits::deallocate(a, eol, 1);  }

    list &operator=(const list &o)
    {
        if (eol == o.eol)
            return *this;
        this->clear();
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        this->insert(const_iterator(eol), o.begin(), o.end());
        return *this;
    }

    list &operator=(list &&o)
    {
        if (eol == o.eol)
            return *this;
        this->clear();
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        if (a == o.a) {
            glglT::swap(eol, o.eol);
            len = o.len;
        } else
            this->insert_move(const_iterator(eol), o.begin(), o.end());
        return *this;
    }

    list &operator=(std::initializer_list<T> il)
    {
        this->assign(il);
        return *this;
    }

    void assign(size_type n, const T &t)
    {
        for (auto b = eol->next; ;b = b->next) {
            if (b == eol) {
                this->insert(const_iterator(eol), n, t);
                break;
            } else if (n-- == 0) {
                this->erase(const_iterator(b), this->cend());
                break;
            }
            b->t = t;
        }
    }

    void assign(std::initializer_list<T> il) { assign(il.begin(), il.end()); }
    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    void assign(It b, It e)
    {
        for (auto f = eol->next; ;f = f->next, ++b) {
            if (f == eol) {
                this->insert(const_iterator(eol), b, e);
                break;
            } else if (b == e) {
                this->erase(const_iterator(f), this->cend());
                break;
            }
            f->t = *b;
        }
    }

    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    reference front() noexcept { return eol->next->t; }
    const_reference front() const noexcept { return eol->next->t; }

    reference back() noexcept { return eol->prev->t; }
    const_reference back() const noexcept { return eol->prev->t; }

    iterator begin() noexcept { return iterator(eol->next); }
    const_iterator begin() const noexcept { return const_iterator(eol->next); }
    const_iterator cbegin() const noexcept { return const_iterator(eol->next); }

    iterator end() noexcept { return iterator(eol); }
    const_iterator end() const noexcept { return const_iterator(eol); }
    const_iterator cend() const noexcept { return const_iterator(eol); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(this->end()); }
    const_reverse_iterator rbegin() const noexcept { return this->crbegin(); }
    const_reverse_iterator crbegin() const noexcept
    {
        return const_reverse_iterator(this->cend());
    }

    reverse_iterator rend() noexcept { return reverse_iterator(this->begin()); }
    const_reverse_iterator rend() const noexcept { return this->crend(); }
    const_reverse_iterator crend() const noexcept
    {
        return const_reverse_iterator(this->cbegin());
    }

    bool empty() const noexcept { return len; }
    size_type size() const noexcept { return len; }
    size_type max_size() const noexcept { return rebind_traits::max_size(a); }

    void clear() noexcept
    {
        this->free();
        eol->prev = eol;
        eol->next = eol;
        len = 0;
    }

    iterator insert(const_iterator itr, const T &t) { return this->emplace(itr, t); }
    iterator insert(const_iterator itr, T &&t)
    {
        return this->emplace(itr, glglT::move(t));
    }

    iterator insert(const_iterator itr, size_type n, const T &t)
    {
        const rebind_ptr bef = itr.curr->prev;
        for (rebind_ptr new_node; n-- != 0; ++len) {
            try {
                new_node = this->make_new_node(t);
            } catch(...) {
                for (rebind_ptr tmpc = bef->next, tmpn; tmpc != itr.curr; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpc);
                    rebind_traits::deallocate(a, tmpc, 1);
                    tmpc = tmpn;
                }
                bef->next = itr.curr;
                itr.curr->prev = bef;
                throw;
            }
            new_node->prev = itr.curr->prev;
            new_node->next = itr.curr;
            itr.curr->prev->next = new_node;
            itr.curr->prev = new_node;
        }   return iterator(bef->next);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    iterator insert(const_iterator itr, It b, It e)
    {
        const rebind_ptr bef = itr.curr->prev;
        for (rebind_ptr new_node; b != e; ++b, ++len) {
            try {
                new_node = this->make_new_node(*b);
            } catch(...) {
                for (rebind_ptr tmpc = bef->next, tmpn; tmpc != itr.curr; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpc);
                    rebind_traits::deallocate(a, tmpc, 1);
                    tmpc = tmpn;
                }
                bef->next = itr.curr;
                itr.curr->prev = bef;
                throw;
            }
            new_node->prev = itr.curr->prev;
            new_node->next = itr.curr;
            itr.curr->prev->next = new_node;
            itr.curr->prev = new_node;
        }   return iterator(bef->next);
    }

    iterator insert(const_iterator itr, std::initializer_list<T> il)
    {
        return this->insert(itr, il.begin(), il.end());
    }

    template <typename... Args>
    iterator emplace(const_iterator itr, Args&&... args)
    {
        const auto new_node = this->make_new_node(glglT::forward<Args>(args)...);
        new_node->prev = itr.curr->prev;
        new_node->next = itr.curr;
        itr.curr->prev->next = new_node;
        itr.curr->prev = new_node;
        ++len;
        return iterator(new_node);
    }

    iterator erase(const_iterator itr)
    {
        if (len == 0)
            return iterator(eol);
        const auto n = itr.curr->next;
        this->delete_p(itr.curr, n);
        return iterator(n);
    }

    iterator erase(const_iterator b, const_iterator e)
    {
        e.curr->prev = b.curr->prev;
        b.curr->prev->next = e.curr;
        for (rebind_ptr tmp; b.curr != e.curr; --len) {
            tmp = b.curr->next;
            rebind_traits::destroy(a, b.curr);
            rebind_traits::deallocate(a, b.curr, 1);
            b.curr = tmp;
        }   return iterator(e.curr);
    }

    void pop_back() { this->delete_p(eol->prev, eol); }
    void push_back(const T &t) { this->emplace_back(t); }
    void push_back(T &&t) { this->emplace_back(glglT::move(t)); }
    template <typename... Args>
    void emplace_back(Args&&... args)
    {
        const auto new_node = this->make_new_node(glglT::forward<Args>(args)...);
        new_node->prev = eol->prev;
        eol->prev->next = new_node;
        new_node->next = eol;
        eol->prev = new_node;
        ++len;
    }

    void push_front(const T &t) { this->emplace_front(t); }
    void push_front(T &&t) { this->emplace_front(glglT::move(t)); }
    template <typename... Args>
    void emplace_front(Args&&... args)
    {
        const auto new_node = this->make_new_node(glglT::forward<Args>(args)...);
        new_node->next = eol->next;
        eol->next->prev = new_node;
        new_node->prev = eol;
        eol->next = new_node;
        ++len;
    }

    void pop_front()
    {
        const auto n = eol->next;
        eol->next = n->next;
        n->next->prev = eol;
        rebind_traits::destroy(a, n);
        rebind_traits::deallocate(a, n, 1);
        --len;
    }

    void resize(size_type n)
    {
        if (n < len) {
            const_iterator b = this->end();
            glglT::advance(b, n - len);
            this->erase(b, this->end());
        } else
            while (n-- != 0)
                this->emplace_back();
    }

    void resize(size_type n, const T &t)
    {
        if (n < len) {
            const_iterator b = this->end();
            glglT::advance(b, n - len);
            this->erase(b, this->end());
        } else
            this->insert(this->end(), n, t);
    }

    void swap(list &o)
    {
        glglT::alloc_swap(a, o.a,
                          typename rebind_traits::propagate_on_container_swap());
        glglT::swap(eol, o.eol);
        glglT::swap(len, o.len);
    }

    void merge(list &&o) { this->merge(o); }
    void merge(list &o)
    {
        if (eol == o.eol)
            return;
        len += o.len;
        for (rebind_ptr b = eol->next, ob = o.eol->next, otmp; ;) {
            if (b == eol) {
                if (ob != o.eol) {
                    ob->prev = b;
                    b->next = ob;
                    o.eol->prev->next = eol;
                    eol->prev = o.eol->prev;
                }   break;
            }
            if (ob == o.eol)
                break;
            if (ob->t < b->t) {
                otmp = ob->next;
                b->prev->next = ob;
                ob->prev = b->prev;
                ob->next = b;
                b->prev = ob;
                ob = otmp;
            } else
                b = b->next;
        }
        o.eol->prev = o.eol;
        o.eol->next = o.eol;
        o.len = 0;
    }

    template <typename Comp>
    void merge(list &&o, Comp c) { this->merge(o, c); }
    template <typename Comp>
    void merge(list &o, Comp c)
    {
        if (eol == o.eol)
            return;
        len += o.len;
        for (rebind_ptr b = eol->next, ob = o.eol->next, otmp; ;) {
            if (b == eol) {
                if (ob != o.eol) {
                    ob->prev = b;
                    b->next = ob;
                    o.eol->prev->next = eol;
                    eol->prev = o.eol->prev;
                }   break;
            }
            if (ob == o.eol)
                break;
            if (c(ob->t, b->t)) {
                otmp = ob->next;
                b->prev->next = ob;
                ob->prev = b->prev;
                ob->next = b;
                b->prev = ob;
                ob = otmp;
            } else
                b = b->next;
        }
        o.eol->prev = o.eol;
        o.eol->next = o.eol;
        o.len = 0;
    }

    void splice(const_iterator itr, list &&o) { this->splice(itr, o); }
    void splice(const_iterator itr, list &o)
    {
        len += o.len;
        itr.curr->prev->next = o.eol->next;
        o.eol->next->prev = itr.curr->prev;
        itr.curr->prev = o.eol->prev;
        o.eol->prev->next = itr.curr;
        o.eol->prev = o.eol;
        o.eol->next = o.eol;
        o.len = 0;
    }

    void splice(const_iterator itr, list &o, const_iterator it)
    {
        --o.len; ++len;
        insert_p1_before_p2(it.curr, itr.curr);
    }

    void splice(const_iterator itr, list &&o, const_iterator it)
    {
        this->splice(itr, o, it);
    }

    void splice(const_iterator itr, list &o,
                const_iterator b, const_iterator e)
    {
        if (eol != o.eol) {
            const auto n = glglT::distance(b, e);
            len += n; o.len -= n;
        }
        b.curr->prev->next = e.curr;
        itr.curr->prev->next = b.curr;
        b.curr->prev = itr.curr->prev;
        itr.curr->prev = e.curr->prev;
        e.curr->prev->next = itr.curr;
        e.curr->prev = b.curr->prev;
    }

    void splice(const_iterator itr, list &&o,
                const_iterator b, const_iterator e)
    {
        this->splice(itr, o, b, e);
    }

    void remove(const T &t)
    {
        for (rebind_ptr p = eol->next, tmp; p != eol; p = tmp) {
            tmp = p->next;
            if (p->t == t)
                this->delete_p(p, tmp);
        }
    }

    template <typename Pred>
    void remove_if(Pred pd)
    {
        for (rebind_ptr p = eol->next, tmp; p != eol; p = tmp) {
            tmp = p->next;
            if (pd(p->t))
                this->delete_p(p, tmp);
        }
    }

    void reverse() noexcept
    {
        for (rebind_ptr p = eol->next, tmp; p != eol; p = tmp) {
            tmp = p->next;
            glglT::swap(p->prev, p->next);
        }   glglT::swap(eol->prev, eol->next);
    }

    void unique()
    {
        for (rebind_ptr p = eol->next, n; ; p = n) {
            n = p->next;
            if (n == eol)
                return;
            if (p->t == n->t)
                this->delete_p(p, n);
        }
    }

    template <typename Pred>
    void unique(Pred pd)
    {
        for (rebind_ptr p = eol->next, n; ; p = n) {
            n = p->next;
            if (n == eol)
                return;
            if (pd(p->t, n->t))
                this->delete_p(p, n);
        }
    }

    template <typename Comp>
    void sort(Comp c) { sort_aux(eol->next, eol, len / 2, len, c); }
    void sort() { sort_aux(eol->next, eol, len / 2, len); }
private:
    void free() noexcept
    {
        for (auto i = eol->next; i != eol;) {
            const auto tmp = i->next;
            uninitialized_with_alloc<rebind_alloc, rebind_traits>::
                destroy(a, i, 1);
            rebind_traits::deallocate(a, i, 1);
            i = tmp;
        }
    }

    template <typename... Args>
    rebind_ptr make_new_node(Args&&... args)
    {
        const auto new_node = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, new_node, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }
        return new_node;
    }

    void delete_p(rebind_ptr p, rebind_ptr n)
    {
        p->prev->next = n;
        n->prev = p->prev;
        rebind_traits::destroy(a, p);
        rebind_traits::deallocate(a, p, 1);
        --len;
    }

    iterator insert_move(const_iterator itr, iterator b, iterator e)
    {
        const rebind_ptr bef = itr.curr->prev;
        for (rebind_ptr new_node; b != e; ++b, ++len) {
            try {
                new_node = this->make_new_node(glglT::move(*b));
            } catch(...) {
                for (rebind_ptr tmpc = bef->next, tmpn; tmpc != itr.curr; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpc);
                    rebind_traits::deallocate(a, tmpc, 1);
                    tmpc = tmpn;
                }
                bef->next = itr.curr;
                itr.curr->prev = bef;
                throw;
            }
            new_node->prev = itr.curr->prev;
            new_node->next = itr.curr;
            itr.curr->prev->next = new_node;
            itr.curr->prev = new_node;
        }
    }

    static rebind_ptr sort_aux(rebind_ptr b, rebind_ptr e, size_t pd, size_t td)
    {
        if (pd == 0)
            return b;
        if (td < SL_LIM)
            return insertion_sort_aux(b, e);
        rebind_ptr m = b;
        for (auto n = pd; n-- != 0;)
            m = m->next;
        const auto ed = td - pd;
        b = sort_aux(b, m, pd / 2, pd);
        m = sort_aux(m, e, ed / 2, ed);
        return merge_self(b, m, e);
    }

    template <typename Comp>
    static rebind_ptr sort_aux(rebind_ptr b, rebind_ptr e, size_t pd, size_t td, Comp c)
    {
        if (pd == 0)
            return b;
        if (td < SL_LIM)
            return insertion_sort_aux(b, e, c);
        rebind_ptr m = b;
        for (auto n = pd; n-- != 0;)
            m = m->next;
        const auto ed = td - pd;
        b = sort_aux(b, m, pd / 2, pd, c);
        m = sort_aux(m, e, ed / 2, ed, c);
        return merge_self(b, m, e, c);
    }

    static rebind_ptr merge_self(rebind_ptr b, rebind_ptr m, rebind_ptr e)
    {
        if (b == m || m == e)
            return b;
        bool f = m->t < b->t;
        const rebind_ptr r = f ? m : b;
        for (rebind_ptr mtmp; ;) {
            if (f) {
                mtmp = m->next;
                insert_p1_before_p2(m, b);
                m = mtmp;
                if (m == e) break;
            } else {
                b = b->next;
                if (b == m) break;
            }   f = m->t < b->t;
        }   return r;
    }

    template <typename Comp>
    static rebind_ptr merge_self(rebind_ptr b, rebind_ptr m, rebind_ptr e, Comp c)
    {
        if (b == m || m == e)
            return b;
        bool f = c(m->t, b->t);
        const rebind_ptr r = f ? m : b;
        for (rebind_ptr mtmp; ;) {
            if (f) {
                mtmp = m->next;
                insert_p1_before_p2(m, b);
                m = mtmp;
                if (m == e) break;
            } else {
                b = b->next;
                if (b == m) break;
            }   f = c(m->t, b->t);
        }   return r;
    }

    static rebind_ptr insertion_sort_aux(rebind_ptr b, rebind_ptr e)
    {
        rebind_ptr new_b = b;
        for (rebind_ptr c = b->next, tmp; c != e; c = tmp) {
            tmp = c->next;
            for (rebind_ptr p = c->prev; ; p = p->prev) {
                if (c->t < p->t) {
                    if (p != new_b) continue;
                    new_b = c;
                } else {
                    p = p->next;
                    if (p == c) break;
                }   insert_p1_before_p2(c, p);
                break;
            }
        }   return new_b;
    }

    template <typename Comp>
    static rebind_ptr insertion_sort_aux(rebind_ptr b, rebind_ptr e, Comp cp)
    {
        rebind_ptr new_b = b;
        for (rebind_ptr c = b->next, tmp; c != e; c = tmp) {
            tmp = c->next;
            for (rebind_ptr p = c->prev; ; p = p->prev) {
                if (c->t < p->t) {
                    if (p != new_b) continue;
                    new_b = c;
                } else {
                    p = p->next;
                    if (p == c) break;
                }   insert_p1_before_p2(c, p);
                break;
            }
        }   return new_b;
    }

    static void insert_p1_before_p2(rebind_ptr p1, rebind_ptr p2)
    {
        p1->prev->next = p1->next;
        p1->next->prev = p1->prev;
        p2->prev->next = p1;
        p1->prev = p2->prev;
        p2->prev = p1;
        p1->next = p2;
    }
};

template <typename T, typename A> inline
void swap(list<T, A> &l1, list<T, A> &l2)
{
    l1.swap(l2);
}

template <typename T, typename A>
bool operator==(const list<T, A> &l1, const list<T, A> &l2)
{
    if (l1.size() != l2.size())
        return false;
    const auto e = l1.end();
    for (auto it1 = l1.begin(), it2 = l2.begin(); it1 != e; ++it1, ++it2)
        if (*it1 != *it2)
            return false;
    return true;
}

template <typename T, typename A> inline
bool operator!=(const list<T, A> &l1, const list<T, A> &l2)
{
    return !(l1 == l2);
}

template <typename T, typename A>
bool operator<(const list<T, A> &l1, const list<T, A> &l2)
{
    auto it1 = l1.begin(), it2 = l2.begin();
    const auto e1 = l1.end(), e2 = l2.end();
    while (true) {
        if (it1 == e1)
            return it2 != e2;
        if (it2 == e2)
            return false;
        if (*it1 < *it2)
            return true;
        if (*it2 < *it1)
            return false;
        ++it1, ++it2;
    }
}

template <typename T, typename A> inline
bool operator>=(const list<T, A> &l1, const list<T, A> &l2)
{
    return !(l1 < l2);
}

template <typename T, typename A> inline
bool operator>(const list<T, A> &l1, const list<T, A> &l2)
{
    return l2 < l1;
}

template <typename T, typename A> inline
bool operator<=(const list<T, A> &l1, const list<T, A> &l2)
{
    return !(l2 < l1);
}

} // namespace glglT

#endif // GLGL_LIST_H
