#ifndef GLGL_ANY_H
#define GLGL_ANY_H

#include <typeinfo>
#include <exception>
#include "sub/glgl_function.h"

namespace glglT {

struct any_object_base {
    virtual any_object_base *copy_helper(void *) const = 0;
    virtual void move_helper(void *) = 0;
    virtual const std::type_info &type_helper() const noexcept = 0;
    virtual void *cast_helper() noexcept = 0;
    virtual ~any_object_base() = default;
};

template <template <typename> class Any>
struct function_union<Any, any_object_base *> {
    typename aligned_storage<
        sizeof(Any<any_object_base *>), alignof(Any<any_object_base *>)>::type s;
};

class any;

class any_base {
    template <typename T> friend const T &any_cast(const any &a);
    template <typename T> friend T &&any_cast(any &&);
    template <typename T> friend T &any_cast(any &);
    template <typename T> friend const T *any_cast(const any *) noexcept;
    template <typename T> friend T *any_cast(any *) noexcept;

    template <typename T>
    struct any_object_template: any_object_base {
        T t;

        any_object_template(const T &obj): t(obj) {}
        any_object_template(T &&obj): t(glglT::move(obj)) {}
        template <typename... Args>
        any_object_template(Args&&... args): t(glglT::forward<Args>(args)...) {}

        any_object_base *copy_helper(void *p) const
        {
            void *o = p == nullptr ? ::operator new(sizeof(any_object_template)) : p;
            try {
                ::new (o) any_object_template(t);
            } catch (...) {
                if (p == nullptr)
                    ::operator delete(o);
                throw;
            }
            return static_cast<any_object_base *>(o);
        }

        void move_helper(void *p) { ::new (p) any_object_template(glglT::move(*this)); }
        const std::type_info &type_helper() const noexcept { return typeid(t); }
        void *cast_helper() noexcept { return &t; }
    };

    template <typename T> using any_t = any_object_template<typename decay<T>::type>;
    template <typename T> using store_in_heap_t = integral_constant<bool,
        sizeof(function_union<any_t, any_object_base *>) < sizeof(any_t<T>) ||
        alignof(function_union<any_t, any_object_base *>) < alignof(any_t<T>) ||
        alignof(function_union<any_t, any_object_base *>) % alignof(any_t<T>) != 0>;
    function_union<any_t, any_object_base *> s;
    any_object_base *p;
protected:
    constexpr any_base() noexcept: s(), p(nullptr) {}
    any_base(const any_base &o) { this->copy(o); }
    any_base(any_base &&o) { this->move(glglT::move(o)); }
    template <typename T,
              typename = typename enable_if<!store_in_heap_t<T>::value>::type>
    any_base(T &&t, int) { this->no_need_to_alloc<T>(glglT::forward<T>(t)); }
    template <typename T,
              typename = typename enable_if<store_in_heap_t<T>::value>::type>
    any_base(T &&t, ...) { this->need_to_alloc<T>(glglT::forward<T>(t)); }

    ~any_base() { delete p; }

    any_base &operator=(const any_base &o)
    {
        this->free();
        this->copy(o);
        return *this;
    }

    any_base &operator=(any_base &&o) noexcept
    {
        this->free();
        this->move(glglT::move(o));
        return *this;
    }

    template <typename T,
              typename = typename enable_if<!store_in_heap_t<T>::value>::type>
    void assign(T &&t, int)
    {
        this->free();
        this->no_need_to_alloc<T>(glglT::forward<T>(t));
    }

    template <typename T,
              typename = typename enable_if<store_in_heap_t<T>::value>::type>
    void assign(T &&t, ...)
    {
        this->free();
        this->need_to_alloc<T>(glglT::forward<T>(t));
    }
public:
    bool empty() const noexcept { return p == nullptr; }
    const std::type_info &type() const noexcept
    {
        return p ? p->type_helper() : typeid(void);
    }

    template <typename T, typename... Args>
    void emplace(Args&&... args)
    {
        this->free();
        this->emplace_aux<T>(typename store_in_heap_t<T>::type{},
                             glglT::forward<Args>(args)...);
    }

    void reset() noexcept
    {
        this->free();
        p = nullptr;
    }
private:
    void free() noexcept
    {
        if (p) {
            p->~any_object_base();
            if (p != static_cast<void *>(&s))
                ::operator delete(p);
        }
    }

    void copy(const any_base &o)
    {
        p = nullptr;
        if (o.p != nullptr)
            p = o.p->copy_helper(o.p == static_cast<const void *>(&(o.s))
                                 ? static_cast<void *>(&s) : p);
    }

    void move(any_base &&o)
    {
        p = nullptr;
        if (o.p == static_cast<void *>(&(o.s))) {
            o.p->move_helper(&s);
            o.p->~any_object_base();
            p = reinterpret_cast<any_object_base *>(&s);
        } else
            p = o.p;
        o.p = nullptr;
    }

    template <typename T, typename... Args>
    void no_need_to_alloc(Args&&... args)
    {
        p = nullptr;
        ::new (&s) any_t<T>(glglT::forward<Args>(args)...);
        p = reinterpret_cast<any_object_base *>(&s);
    }

    template <typename T, typename... Args>
    void need_to_alloc(Args&&... args)
    {
        p = nullptr;
        p = ::new any_t<T>(glglT::forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    void emplace_aux(false_type, Args&&... args)
    {
        this->no_need_to_alloc<T>(glglT::forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    void emplace_aux(true_type, Args&&... args)
    {
        this->need_to_alloc<T>(glglT::forward<Args>(args)...);
    }
};

struct any: any_base {
    template <typename T>
    any(T t): any_base(glglT::move(t), 0) {}
    constexpr any() noexcept: any_base() {}
    any(const any &o): any_base(static_cast<const any_base &>(o)) {}
    any(any &&o): any_base(static_cast<any_base &&>(glglT::move(o))) {}

    any &operator=(const any &o)
    {
        any_base::operator=(o);
        return *this;
    }

    any &operator=(any &&o)
    {
        any_base::operator=(glglT::move(o));
        return *this;
    }

    template <typename T>
    any &operator=(T &&t)
    {
        any_base::assign(glglT::forward<T>(t), 0);
        return *this;
    }

    void swap(any &o)
    {
        glglT::swap(*this, o);
    }
};

template <typename T, typename... Args> inline
any make_any(Args&&... args)
{
    any a;
    a.template emplace<T>(glglT::forward<Args>(args)...);
    return a;
}

class bad_any_cast: public std::exception {
    constexpr static const char *s = "bad any cast";
public:
    bad_any_cast() = default;
    virtual const char *what() const noexcept { return s; }
};

template <typename T> inline
const T &any_cast(const any &a)
{
    typedef typename decay<T>::type D;
    if (typeid(T) != a.type())
        throw bad_any_cast();
    return *reinterpret_cast<const D *>(a.p->cast_helper());
}

template <typename T> inline
T &&any_cast(any &&a)
{
    typedef typename decay<T>::type D;
    if (typeid(T) != a.type())
        throw bad_any_cast();
    return glglT::move(*reinterpret_cast<D *>(a.p->cast_helper()));
}

template <typename T> inline
T &any_cast(any &a)
{
    typedef typename decay<T>::type D;
    if (typeid(T) != a.type())
        throw bad_any_cast();
    return *reinterpret_cast<D *>(a.p->cast_helper());
}

template <typename T> inline
const T *any_cast(const any *a) noexcept
{
    typedef typename decay<T>::type D;
    if (typeid(T) != a->type())
        return nullptr;
    return reinterpret_cast<const D *>(a->p->cast_helper());
}

template <typename T> inline
T *any_cast(any *a) noexcept
{
    typedef typename decay<T>::type D;
    if (typeid(T) != a->type())
        return nullptr;
    return reinterpret_cast<D *>(a->p->cast_helper());
}

} // namespace glglT

#endif // GLGL_ANY_H
