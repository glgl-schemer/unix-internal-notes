#ifndef GLGL_DEQUE_H
#define GLGL_DEQUE_H

#include <stdexcept>
#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_allocator_traits.h"
#include "sub/glgl_base_algo.h"
#include "sub/glgl_other_iterator.h"

namespace glglT {

template <typename, typename> class deque;

template <typename It, typename T, typename Ptr>
class deque_use_iterator_base {
    template <typename, typename> friend class deque;
    template <typename, typename, typename> friend class deque_use_iterator_base;
    constexpr static int SQ_LIM = 16;
protected:
    Ptr *p_deque;
    size_t pcurr;
    size_t curr;

    constexpr deque_use_iterator_base() noexcept = default;
    deque_use_iterator_base(Ptr *d, size_t pc, size_t c) noexcept:
        p_deque(d), pcurr(pc), curr(c) {}

    T &operator*() const noexcept { return p_deque[pcurr][curr]; }
    T *operator->() const noexcept { return glglT::addressof(operator*()); }
    T &operator[](ptrdiff_t n) const noexcept { return *(*this + n); }
public:
    It &operator++() noexcept
    {
        if (++curr == SQ_LIM)
            ++pcurr, curr = 0;
        return static_cast<It &>(*this);
    }

    It &operator--() noexcept
    {
        if (curr == 0)
            --pcurr, curr = SQ_LIM - 1;
        else
            --curr;
        return static_cast<It &>(*this);
    }

    It operator++(int) noexcept
    {
        It tmp(p_deque, pcurr, curr);
        this->operator++();
        return tmp;
    }

    It operator--(int) noexcept
    {
        It tmp(p_deque, pcurr, curr);
        this->operator--();
        return tmp;
    }

    It operator+(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator-(-n);
        size_t t_curr = curr + n % SQ_LIM;
        size_t t_pcurr = pcurr + n / SQ_LIM;
        if (t_curr >= SQ_LIM)
            ++t_pcurr, t_curr -= SQ_LIM;
        return It(p_deque, t_pcurr, t_curr);
    }

    It operator-(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator+(-n);
        size_t t_curr = curr - n % SQ_LIM;
        size_t t_pcurr = pcurr - n / SQ_LIM;
        if (t_curr > SQ_LIM)
            --t_pcurr, t_curr += SQ_LIM;
        return It(p_deque, t_pcurr, t_curr);
    }

    It &operator+=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator-=(-n);
        curr += n % SQ_LIM;
        pcurr += n / SQ_LIM;
        if (curr >= SQ_LIM)
            ++pcurr, curr -= SQ_LIM;
        return static_cast<It &>(*this);
    }

    It &operator-=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator+=(-n);
        curr -= n % SQ_LIM;
        pcurr -= n / SQ_LIM;
        if (curr > SQ_LIM)
            --pcurr, curr += SQ_LIM;
        return static_cast<It &>(*this);
    }

    template <typename OIt>
    ptrdiff_t operator-(const deque_use_iterator_base<OIt, T, Ptr> &p) const noexcept
    {
        return (pcurr - p.pcurr) * SQ_LIM + curr - p.curr;
    }

    template <typename OIt>
    bool operator<(const deque_use_iterator_base<OIt, T, Ptr> &p) const noexcept
    {
        return pcurr < p.pcurr || (pcurr == p.pcurr && curr < p.curr);
    }

    template <typename OIt>
    bool operator==(const deque_use_iterator_base<OIt, T, Ptr> &p) const noexcept
    {
        return pcurr == p.pcurr && curr == p.curr;
    }
};

template <typename It1, typename It2, typename T, typename P> inline
bool operator>(const deque_use_iterator_base<It1, T, P> &p1,
               const deque_use_iterator_base<It2, T, P> &p2) noexcept
{
    return p2 < p1;
}

template <typename It1, typename It2, typename T, typename P> inline
bool operator<=(const deque_use_iterator_base<It1, T, P> &p1,
                const deque_use_iterator_base<It2, T, P> &p2) noexcept
{
    return !(p1 > p2);
}

template <typename It1, typename It2, typename T, typename P> inline
bool operator>=(const deque_use_iterator_base<It1, T, P> &p1,
                const deque_use_iterator_base<It2, T, P> &p2) noexcept
{
    return !(p1 < p2);
}

template <typename It1, typename It2, typename T, typename P> inline
bool operator!=(const deque_use_iterator_base<It1, T, P> &p1,
                const deque_use_iterator_base<It2, T, P> &p2) noexcept
{
    return !(p1 == p2);
}

template <typename, typename> class deque_use_iterator;

template <typename T, typename Ptr>
struct deque_use_const_iterator:
    deque_use_iterator_base<deque_use_const_iterator<T, Ptr>, T, Ptr>,
    iterator<random_access_iterator_tag, const T>  {
    template <typename, typename> friend class deque;

    constexpr deque_use_const_iterator() noexcept = default;
    deque_use_const_iterator(Ptr *d, size_t pc, size_t c) noexcept:
        deque_use_iterator_base<deque_use_const_iterator, T, Ptr>(d, pc, c) {}

    explicit operator deque_use_iterator<T, Ptr> () const noexcept;

    const T &operator*() const noexcept
    {
        return deque_use_iterator_base<deque_use_const_iterator, T, Ptr>::
            operator*();
    }

    const T *operator->() const noexcept
    {
        return deque_use_iterator_base<deque_use_const_iterator, T, Ptr>::
            operator->();
    }

    const T &operator[](ptrdiff_t n) const noexcept
    {
        return deque_use_iterator_base<deque_use_const_iterator, T, Ptr>::
            operator[](n);
    }
};

template <typename T, typename P> inline deque_use_const_iterator<T, P>
operator+(ptrdiff_t n, const deque_use_const_iterator<T, P> &p) noexcept
{
    return p + n;
}

template <typename T, typename Ptr>
struct deque_use_iterator:
    deque_use_iterator_base<deque_use_iterator<T, Ptr>, T, Ptr>,
    iterator<random_access_iterator_tag, const T> {
    template <typename, typename> friend class deque;

    constexpr deque_use_iterator() noexcept = default;
    deque_use_iterator(Ptr *d, size_t pc, size_t c) noexcept:
        deque_use_iterator_base<deque_use_iterator, T, Ptr>(d, pc, c) {}

    operator deque_use_const_iterator<T, Ptr> () const noexcept
    {
        return deque_use_const_iterator<T, Ptr>(this->p_deque, this->pcurr, this->curr);
    }

    T &operator*() const noexcept
    {
        return deque_use_iterator_base<deque_use_iterator, T, Ptr>::
            operator*();
    }

    T *operator->() const noexcept
    {
        return deque_use_iterator_base<deque_use_iterator, T, Ptr>::
            operator->();
    }

    T &operator[](ptrdiff_t n) const noexcept
    {
        return deque_use_iterator_base<deque_use_iterator, T, Ptr>::
            operator[](n);
    }
};

template <typename T, typename P> inline deque_use_iterator<T, P>
operator+(ptrdiff_t n, const deque_use_iterator<T, P> &p) noexcept
{
    return p + n;
}

template <typename T, typename P> inline
deque_use_const_iterator<T, P>::operator deque_use_iterator<T, P> () const noexcept
{
    return deque_use_iterator<T, P>(this->p_deque, this->pcurr, this->curr);
}

template <typename T, typename Alloc = allocator<T>>
class deque {
    struct copy_tag {};
    struct fill_tag {};
    struct move_tag {};
    constexpr static int SQ_LIM = 16;
public:
    typedef T value_type;
    typedef Alloc allocator_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef deque_use_iterator<T, pointer> iterator;
    typedef deque_use_const_iterator<T, pointer> const_iterator;
    typedef glglT::reverse_iterator<iterator> reverse_iterator;
    typedef glglT::reverse_iterator<const_iterator> const_reverse_iterator;
private:
    Alloc a;
    struct {
        size_t cap, beg, end, head, tail;
        pointer *data;
    } ptr_deque;
    size_t first;
    size_t last;
public:
    explicit deque(const Alloc &ac = Alloc()): a(ac),
        ptr_deque({0, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, nullptr}),
        first(), last() {}

    deque(size_type n, const T &t, const Alloc &ac = Alloc()): a(ac)
        { this->init(n, fill_tag(), t); }

    explicit deque(size_type n): a() { this->init(n, fill_tag()); }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    deque(It b, It e, const Alloc &ac = Alloc()):
        deque(b, e, ac, typename iterator_traits<It>::iterator_category()) {}

    deque(const deque &o):
        a(allocator_traits<Alloc>::
          select_on_container_copy_construction(o.a))
    {
        auto it = o.begin();
        this->init(o.size(), copy_tag(), it);
    }

    deque(const deque &o, const Alloc &ac): a(ac)
    {
        auto it = o.begin();
        this->init(o.size(), copy_tag(), it);
    }

    deque(deque &&o): a(glglT::move(o.a)), ptr_deque(o.ptr_deque),
        first(o.first), last(o.last) { o.ptr_deque.data = nullptr; }
    deque(deque &&o, const Alloc &ac): a(ac), ptr_deque(o.ptr_deque),
        first(o.first), last(o.last)
    {
        if (a != o.a) {
            auto it = o.begin();
            this->init(o.size(), move_tag(), it);
        } else
            o.ptr_deque.data = nullptr;
    }

    deque(std::initializer_list<T> il, const Alloc &ac = Alloc()): a(ac)
    {
        auto it = il.begin();
        this->init(il.size(), copy_tag(), it);
    }

    ~deque() { this->free(); }

    deque &operator=(const deque &o)
    {
        if (this == &o)
            return *this;
        this->free();
        glglT::alloc_copy(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_copy_assignment());
        auto it = o.begin();
        this->init(o.size(), copy_tag(), it);
        return *this;
    }

    deque &operator=(deque &&o)
    {
        if (this == &o)
            return *this;
        this->free();
        glglT::alloc_copy(a, glglT::move(o.a), typename allocator_traits<Alloc>::
                          propagate_on_container_move_assignment());
        if (a == o.a) {
            ptr_deque = o.ptr_deque;
            first = o.first;
            last = o.last;
            o.ptr_deque.data = nullptr;
        } else {
            auto it = o.begin();
            this->init(o.size(), move_tag(), it);
        }
        return *this;
    }

    deque &operator=(std::initializer_list<T> il)
    {
        this->assign(il);
        return *this;
    }

    void assign(size_type n, const T &t)
    {
        this->free();
        this->init(n, fill_tag(), t);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    void assign(It b, It e)
    {
        this->assign(b, e, typename iterator_traits<It>::iterator_category());
    }

    void assign(iterator b, iterator e)
    {
        this->assign(const_iterator(b), const_iterator(e));
    }

    void assign(const_iterator b, const_iterator e)
    {
        const auto f = this->begin(), l = this->end();
        if (f <= b && b <= l) {
            this->erase(e, l);
            this->erase(f, b);
        } else
            this->assign(b, e, forward_iterator_tag());
    }

    void assign(std::initializer_list<T> il)
    {
        this->free();
        auto it = il.begin();
        this->init(il.size(), copy_tag(), it);
    }

    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    reference at(size_type n)
    {
        if (n >= this->size())
            throw std::out_of_range("deque out of range");
        return this->operator[](n);
    }

    const_reference at(size_type n) const
    {
        if (n >= this->size())
            throw std::out_of_range("deque out of range");
        return this->operator[](n);
    }

    reference operator[](size_type n) noexcept
    {
        const auto head = SQ_LIM - first;
        if (n >= head) {
            n -= head;
            return ptr_deque.data[ptr_deque.beg + n / SQ_LIM + 1][n % SQ_LIM];
        } else
            return ptr_deque.data[ptr_deque.beg][n + first];
    }

    const_reference operator[](size_type n) const noexcept
    {
        const auto head = SQ_LIM - first;
        if (n >= head) {
            n -= head;
            return ptr_deque.data[ptr_deque.beg + n / SQ_LIM + 1][n % SQ_LIM];
        } else
            return ptr_deque.data[ptr_deque.beg][n + first];
    }

    reference front() noexcept { return ptr_deque.data[ptr_deque.beg][first]; }
    const_reference front() const noexcept
    {
        return ptr_deque.data[ptr_deque.beg][first];
    }

    reference back() noexcept
    {
        return ptr_deque.data[ptr_deque.end - 1][last ? last - 1 : SQ_LIM - 1];
    }

    const_reference back() const noexcept
    {
        return ptr_deque.data[ptr_deque.end - 1][last ? last - 1 : SQ_LIM - 1];
    }

    iterator begin() noexcept
    {
        return iterator(ptr_deque.data, ptr_deque.beg, first);
    }

    const_iterator begin() const noexcept { return this->cbegin(); }
    const_iterator cbegin() const noexcept
    {
        return const_iterator(ptr_deque.data, ptr_deque.beg, first);
    }

    iterator end() noexcept
    {
        const auto e = last ? ptr_deque.end - 1 : ptr_deque.end;
        return iterator(ptr_deque.data, e, last);
    }

    const_iterator end() const noexcept { return this->cend(); }
    const_iterator cend() const noexcept
    {
        const auto e = last ? ptr_deque.end - 1 : ptr_deque.end;
        return const_iterator(ptr_deque.data, e, last);
    }

    reverse_iterator rbegin() noexcept { return reverse_iterator(this->end()); }
    const_reverse_iterator rbegin() const noexcept { return this->crbegin(); }
    const_reverse_iterator crbegin() const noexcept
    {
        return const_reverse_iterator(this->end());
    }

    reverse_iterator rend() noexcept { return reverse_iterator(this->begin()); }
    const_reverse_iterator rend() const noexcept { return this->crend(); }
    const_reverse_iterator crend() const noexcept
    {
        return const_reverse_iterator(this->begin());
    }

    bool empty() const noexcept { return ptr_deque.beg == ptr_deque.end; }
    size_type size() const noexcept
    {
        const auto diff = ptr_deque.end - ptr_deque.beg;
        return (diff - bool(last)) * SQ_LIM - first + last;
    }

    size_type max_size() const noexcept
    {
        return allocator_traits<Alloc>::max_size(a);
    }

    void shrink_to_fit() noexcept
    {
        while (ptr_deque.head != ptr_deque.beg)
            allocator_traits<Alloc>::
                deallocate(a, ptr_deque.data[ptr_deque.head++], SQ_LIM);
        while (ptr_deque.tail != ptr_deque.end)
            allocator_traits<Alloc>::
                deallocate(a, ptr_deque.data[--ptr_deque.tail], SQ_LIM);
    }

    void clear() noexcept
    {
        const auto diff = ptr_deque.end - ptr_deque.beg;
        if (diff == 0)
            return;
        if (diff == 1) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, ptr_deque.data[ptr_deque.beg] + first,
                        last ? last - first : SQ_LIM);
        } else {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, ptr_deque.data[ptr_deque.beg] + first,
                        SQ_LIM - first);
            if (last)
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    destroy(a, ptr_deque.data[ptr_deque.end - 1], last);
            destroy_whole_block(ptr_deque.beg + 1, ptr_deque.end - 1);
        }
        ptr_deque.beg = ptr_deque.end =
            (ptr_deque.tail - ptr_deque.head) / 2 + ptr_deque.head;
        first = last = 0;
    }

    iterator insert(const_iterator itr, const T &t)
    {
        const auto pos = itr - this->begin();
        if (this->size() / 2 < pos) {
            this->push_back_aux(t);
            const auto pos_1 = this->begin() + pos - 1;
            for (auto i = this->end() - 1; --i != pos_1;)
                glglT::iter_swap(i, i + 1);
        } else {
            this->push_front_aux(t);
            const auto pos_1 = this->begin() + pos + 1;
            for (auto i = this->begin(); ++i != pos_1;)
                glglT::iter_swap(i, i - 1);
        }
        return this->begin() + pos;
    }

    iterator insert(const_iterator itr, T &&t)
    {
        const auto pos = itr - this->begin();
        if (this->size() / 2 < pos) {
            this->push_back_aux(glglT::move(t));
            const auto pos_1 = this->begin() + pos - 1;
            for (auto i = this->end() - 1; --i != pos_1;)
                glglT::iter_swap(i, i + 1);
        } else {
            this->push_front_aux(glglT::move(t));
            const auto pos_1 = this->begin() + pos + 1;
            for (auto i = this->begin(); ++i != pos_1;)
                glglT::iter_swap(i, i - 1);
        }
        return this->begin() + pos;
    }

    iterator insert(const_iterator itr, size_type n, const T &t)
    {
        return this->insr(itr, n, fill_tag(), t);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    iterator insert(const_iterator itr, It b, It e)
    {
        return this->insert(itr, b, e, typename iterator_traits<It>::
                            iterator_category());
    }

    iterator insert(const_iterator itr, std::initializer_list<T> il)
    {
        return this->insr(itr, il.size(), copy_tag(), il.begin());
    }

    template <typename... Args>
    iterator emplace(const_iterator itr, Args&&... args)
    {
        const auto pos = itr - this->begin();
        if (this->size() / 2 < pos) {
            this->push_back_aux(glglT::forward<Args>(args)...);
            const auto pos_1 = this->begin() + pos - 1;
            for (auto i = this->end() - 1; --i != pos_1;)
                glglT::iter_swap(i, i + 1);
        } else {
            push_front_aux(glglT::forward<Args>(args)...);
            const auto pos_1 = this->begin() + pos + 1;
            for (auto i = this->begin(); ++i != pos_1;)
                glglT::iter_swap(i, i - 1);
        }
        return this->begin() + pos;
    }

    iterator erase(const_iterator itr)
    {
        const auto b = this->begin(), e = this->end();
        if (itr == e)
            return e;
        const auto pos = itr - b;
        const auto res = b + pos;
        if (this->size() / 2 < pos) {
            glglT::move_backward(b, res, res + 1);
            this->pop_front();
        } else {
            glglT::move(res + 1, e, res);
            this->pop_back();
        }
        return res;
    }

    iterator erase(const_iterator b, const_iterator e)
    {
        const auto f = this->begin(), l = this->end();
        const auto pos = b - f;
        const auto res = f + pos;
        auto n = e - b;
        if (this->size() / 2 < pos) {
            glglT::move_backward(f, res, res + n);
            while (n-- != 0)
                this->pop_front();
        } else {
            glglT::move(res + n, l, res);
            while (n-- != 0)
                this->pop_back();
        }
        return res;
    }

    void push_back(const T &t) { this->push_back_aux(t); }
    void push_back(T &&t) { this->push_back_aux(glglT::move(t)); }
    template <typename... Args>
    void emplace_back(Args&&... args)
    {
        this->push_back_aux(glglT::forward<Args>(args)...);
    }

    void pop_back() noexcept
    {
        if (last == 0)
            last = SQ_LIM;
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, ptr_deque.data[ptr_deque.end - 1] + (--last), 1);
        if (last == 0)
            --ptr_deque.end;
    }

    void push_front(const T &t) { push_front_aux(t); }
    void push_front(T &&t) { push_front_aux(glglT::move(t)); }
    template <typename... Args>
    void emplace_front(Args&&... args)
    {
        this->push_front_aux(glglT::forward<Args>(args)...);
    }

    void pop_front() noexcept
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, ptr_deque.data[ptr_deque.beg] + (first++), 1);
        if (first == SQ_LIM) {
            first == 0;
            ++ptr_deque.beg;
        }
    }

    void resize(size_type n)
    {
        const auto sz = this->size();
        if (n < sz)
            this->erase(this->begin() + n, this->end());
        else
            while (n-- != 0)
                this->emplace_back();
    }

    void resize(size_type n, const T &t)
    {
        const auto sz = this->size();
        if (n < sz)
            this->erase(this->begin() + n, this->end());
        else
            this->insert(this->end(), n - sz, t);
    }

    void swap(deque &o)
    {
        glglT::alloc_swap(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_swap());
        glglT::swap(ptr_deque, o.ptr_deque);
        glglT::swap(first, o.first);
        glglT::swap(last, o.last);
    }
private:
    void free() noexcept
    {
        if (ptr_deque.data) {
            this->clear();
            this->shrink_to_fit();
            delete[] ptr_deque.data;
        }
    }

    template <typename It>
    deque(It b, It e, const Alloc &ac, input_iterator_tag): a(ac),
        ptr_deque({0, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, nullptr}),
        first(), last()
    {
        while (b != e) {
            this->push_back(*b);
            ++b;
        }
    }

    template <typename It>
    deque(It b, It e, const Alloc &ac, forward_iterator_tag): a(ac)
        { this->init(glglT::distance(b, e), copy_tag(), b); }

    template <typename It>
    void assign(It b, It e, input_iterator_tag)
    {
        this->free();
        ptr_deque = {0, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, SQ_LIM / 2, nullptr};
        first = last = 0;
        while (b != e) {
            this->push_back(*b);
            ++b;
        }
    }

    template <typename It>
    void assign(It b, It e, forward_iterator_tag)
    {
        this->free();
        this->init(glglT::distance(b, e), copy_tag(), b);
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, input_iterator_tag)
    {
        const auto pos = itr - begin();
        const auto sz = size();
        size_t n = 0;
        while (b != e) {
            this->push_back(*b);
            ++b, ++n;
        }
        const auto f = this->begin();
        const auto res = f + pos;
        glglT::rotate(res, f + sz, this->end());
        return res;
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, forward_iterator_tag)
    {
        return this->insr(itr, glglT::distance(b, e), copy_tag(), b);
    }

    void destruct_whole_block(size_t b, size_t e)
    {
        for (size_t i = b; i != e; ++i) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, ptr_deque.data[i], SQ_LIM);
            allocator_traits<Alloc>::deallocate(a, ptr_deque.data[i], SQ_LIM);
        }
    }

    void destroy_whole_block(size_t b, size_t e)
    {
        for (size_t i = b; i != e; ++i)
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, ptr_deque.data[i], SQ_LIM);
    }

    void init_in_exception(size_t b, size_t e, size_t p_cap)
    {
        this->destruct_whole_block(b, e);
        first = last = 0;
        ptr_deque.beg = ptr_deque.end = ptr_deque.head = ptr_deque.tail = p_cap / 2;
    }

    template <typename... Args>
    void new_block_construct(size_t i, size_type n, size_type pos,
                             fill_tag, Args&&... args)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_fill_n(a, ptr_deque.data[i] + pos, n,
                                 glglT::forward<Args>(args)...);
    }

    template <typename It>
    void new_block_construct(size_t i, size_type n, size_type pos, copy_tag, It &b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, b, n, ptr_deque.data[i] + pos);
        glglT::advance(b, n);
    }

    void new_block_construct(size_t i, size_type n, size_type pos, move_tag, iterator &b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_move_n(a, b, n, ptr_deque.data[i] + pos);
        glglT::advance(b, n);
    }

    template <typename... Args>
    void init(size_type n, Args&&... args)
    {
        const auto ndiff = n % SQ_LIM;
        const auto p_n = n / SQ_LIM + bool(ndiff);
        const auto p_cap = p_n < SQ_LIM ? SQ_LIM : p_n;
        const auto beg = p_cap / 2 - p_n / 2;
        const auto end = p_cap / 2 + p_n / 2 + p_n % 2;
        try {
            const auto data = new pointer[p_cap];
            ptr_deque = {p_cap, beg, end, beg, end, data};
        } catch (...) {
            ptr_deque = {};
            first = last = 0;
            throw;
        }
        const auto whole_end = end - bool(ndiff);
        for (size_t i = beg; i != whole_end; ++i) {
            try {
                ptr_deque.data[i] = allocator_traits<Alloc>::allocate(a, SQ_LIM);
            } catch(...) {
                init_in_exception(beg, i, p_cap);
                throw;
            }
            try {
                this->new_block_construct(i, SQ_LIM, 0, glglT::forward<Args>(args)...);
            } catch(...) {
                allocator_traits<Alloc>::deallocate(a, ptr_deque.data[i], SQ_LIM);
                this->init_in_exception(beg, i, p_cap);
                throw;
            }
        }
        if (ndiff) {
            try {
                ptr_deque.data[whole_end] =
                    allocator_traits<Alloc>::allocate(a, SQ_LIM);
            } catch(...) {
                this->init_in_exception(beg, whole_end, p_cap);
                throw;
            }
            try {
                this->new_block_construct(whole_end, ndiff, 0,
                                    glglT::forward<Args>(args)...);
            } catch(...) {
                allocator_traits<Alloc>::
                    deallocate(a, ptr_deque.data[whole_end], SQ_LIM);
                this->init_in_exception(beg, whole_end, p_cap);
                throw;
            }
        }
        first = 0;
        last = ndiff;
    }

    template <typename Arg>
    void inner_insr(iterator it, size_type n, fill_tag, Arg&& arg)
    {
        glglT::fill_n(it, n, glglT::forward<Arg>(arg));
    }

    template <typename It>
    void inner_insr(iterator it, size_type n, copy_tag, It b)
    {
        glglT::copy_n(b, n, it);
    }

    template <typename... Args>
    iterator insr(const_iterator itr, size_type n, Args&&... args)
    {
        const auto pos = itr - this->begin();
        const auto sz = this->size();
        const auto res_cap = SQ_LIM * ptr_deque.cap - sz;
        const bool choose_head = !(sz / 2 < pos);
        const auto one_side_cap = choose_head
            ? SQ_LIM * ptr_deque.beg + first
            : SQ_LIM * (ptr_deque.cap - ptr_deque.end + bool(last)) - last;
        if (res_cap < n) {
            const auto n_need = n / SQ_LIM + bool(n % SQ_LIM);
            const auto new_p_cap = ptr_deque.cap
                ? (n_need < ptr_deque.cap
                   ? ptr_deque.cap * 2 : ptr_deque.cap + n_need)
                : SQ_LIM < n_need ? n_need : SQ_LIM;
            const auto new_p_deque = new pointer[new_p_cap];
            if (choose_head) {
                const auto old_b = new_p_cap - ptr_deque.tail;
                glglT::copy(ptr_deque.data + ptr_deque.head,
                            ptr_deque.data + ptr_deque.tail,
                            new_p_deque + old_b);
                const auto move_n = old_b - ptr_deque.head;
                ptr_deque.head = old_b;
                ptr_deque.beg += move_n;
                ptr_deque.end += move_n;
                ptr_deque.tail += move_n;
            } else
                glglT::copy(ptr_deque.data + ptr_deque.head,
                            ptr_deque.data + ptr_deque.tail,
                            new_p_deque + ptr_deque.head);
            const auto b = ptr_deque.cap == 0;
            ptr_deque.cap = new_p_cap;
            delete[] ptr_deque.data;
            ptr_deque.data = new_p_deque;
        } else if (one_side_cap < n) {
            const auto new_res_cap = (res_cap - n) / 2 / SQ_LIM;
            const auto new_one_side = choose_head
                ? ptr_deque.cap - new_res_cap : new_res_cap;
            difference_type diff;
            if (choose_head) {
                if (ptr_deque.tail < new_one_side) {
                    const auto h = glglT::rotate(ptr_deque.data + ptr_deque.head,
                                                 ptr_deque.data + ptr_deque.tail,
                                                 ptr_deque.data + new_one_side);
                    diff = h - ptr_deque.data - ptr_deque.head;
                    ptr_deque.head = h - ptr_deque.data;
                    ptr_deque.tail = new_one_side;
                } else
                    diff = 0;
                ptr_deque.beg = glglT::rotate(ptr_deque.data + ptr_deque.beg + diff,
                                              ptr_deque.data + ptr_deque.end + diff,
                                              ptr_deque.data + new_one_side)
                                - ptr_deque.data;
                ptr_deque.end = new_one_side;
            } else {
                if (new_one_side < ptr_deque.head) {
                    const auto t = glglT::rotate(ptr_deque.data + new_one_side,
                                                 ptr_deque.data + ptr_deque.head,
                                                 ptr_deque.data + ptr_deque.tail);
                    diff = ptr_deque.data + ptr_deque.tail - t;
                    ptr_deque.tail = t - ptr_deque.data;
                    ptr_deque.head = new_one_side;
                } else
                    diff = 0;
                ptr_deque.end = glglT::rotate(ptr_deque.data + new_one_side,
                                              ptr_deque.data + ptr_deque.beg - diff,
                                              ptr_deque.data + ptr_deque.end - diff)
                                - ptr_deque.data;
                ptr_deque.beg = new_one_side;
            }
        }
        if (choose_head) {
            const auto old_beg = this->begin(), new_beg = old_beg - n;
            if (new_beg.pcurr < ptr_deque.head) {
                for (auto ii = new_beg.pcurr; ii != ptr_deque.head; ++ii)
                    try {
                        ptr_deque.data[ii] = allocator_traits<Alloc>::
                            allocate(a, SQ_LIM);
                    } catch(...) {
                        while (ii-- != new_beg.pcurr)
                            allocator_traits<Alloc>::
                                deallocate(a, ptr_deque.data[ii], SQ_LIM);
                        throw;
                    }
                ptr_deque.head = new_beg.pcurr;
            }
            const auto res = new_beg + pos;
            if (n < pos) {///
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    uninitialized_move_n(a, old_beg, n, new_beg);
                ptr_deque.beg = new_beg.pcurr;
                first = new_beg.curr;
                glglT::move(old_beg + n, old_beg + pos, old_beg);
            } else {
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    uninitialized_move_n(a, old_beg, pos, new_beg);
                try {
                    uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                        uninitialized_fill_n(a, res, n - pos);
                } catch(...) {
                    uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                        destroy_move(a, new_beg, old_beg, pos);
                    throw;
                }
                ptr_deque.beg = new_beg.pcurr;
                first = new_beg.curr;
            }
            this->inner_insr(res, n, glglT::forward<Args>(args)...);
            return res;
        } else {
            const auto old_end = this->end(), new_end = old_end + n;
            const auto new_pend = new_end.pcurr + bool(new_end.curr);
            if (ptr_deque.tail < new_pend) {
                for (auto ib = ptr_deque.tail; ib != new_pend; ++ib)
                    try {
                        ptr_deque.data[ib] = allocator_traits<Alloc>::
                            allocate(a, SQ_LIM);
                    } catch(...) {
                        while (ib-- != ptr_deque.tail)
                            allocator_traits<Alloc>::
                                deallocate(a, ptr_deque.data[ib], SQ_LIM);
                        throw;
                    }
                ptr_deque.tail = new_pend;
            }
            const auto bef_end = old_end - n;
            const auto res = this->begin() + pos;
            if (pos + n < sz) {///
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    uninitialized_move_n(a, bef_end, n, old_end);
                ptr_deque.end = new_pend;
                last = new_end.curr;
                glglT::move_backward(res, bef_end, old_end);
            } else {
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    uninitialized_move_n(a, res, sz - pos, res + n);
                try {
                    uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                        uninitialized_fill_n(a, old_end, n + pos - sz);
                } catch(...) {
                    uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                        destroy_move(a, res + n, res, sz - pos);
                    throw;
                }
                ptr_deque.end = new_pend;
                last = new_end.curr;
            }
            this->inner_insr(res, n, glglT::forward<Args>(args)...);
            return res;
        }
    }

    template <typename... Args>
    void push_back_aux(Args&&... args)
    {
        if (last == 0) {
            const bool nonzero_cap = ptr_deque.cap;
            if (ptr_deque.end == ptr_deque.cap || !nonzero_cap) {
                const auto new_p_cap = nonzero_cap ? ptr_deque.cap * 2 : SQ_LIM;
                const auto new_p_deque = new pointer[new_p_cap];
                glglT::copy(ptr_deque.data + ptr_deque.head,
                            ptr_deque.data + ptr_deque.tail,
                            new_p_deque + ptr_deque.head);
                ptr_deque.cap = new_p_cap;
                delete[] ptr_deque.data;
                ptr_deque.data = new_p_deque;
            }
            if (ptr_deque.end == ptr_deque.tail) {
                ptr_deque.data[ptr_deque.tail++] =
                    allocator_traits<Alloc>::allocate(a, SQ_LIM);
            }
            ++ptr_deque.end;
        }
        try {
            allocator_traits<Alloc>::
                construct(a, ptr_deque.data[ptr_deque.end - 1]
                          + last, glglT::forward<Args>(args)...);
        } catch (...) {
            if (last == 0)
                --ptr_deque.end;
            throw;
        }
        if (++last == SQ_LIM)
            last = 0;
    }

    template <typename... Args>
    void push_front_aux(Args&&... args)
    {
        if (first-- == 0) {
            const bool nonzero_cap = ptr_deque.cap;
            if (ptr_deque.beg == 0 || !nonzero_cap) {
                const auto new_p_cap = nonzero_cap ? ptr_deque.cap * 2 : SQ_LIM;
                const auto new_p_deque = new pointer[new_p_cap];
                const auto half_new_cap = new_p_cap / 2;
                glglT::copy(ptr_deque.data + ptr_deque.head,
                            ptr_deque.data + ptr_deque.tail,
                            new_p_deque + half_new_cap);
                ptr_deque.beg += half_new_cap;
                ptr_deque.end += half_new_cap;
                ptr_deque.head += half_new_cap;
                ptr_deque.tail += half_new_cap;
                ptr_deque.cap = new_p_cap;
                delete[] ptr_deque.data;
                ptr_deque.data = new_p_deque;
            }
            if (ptr_deque.beg == ptr_deque.head) {
                ptr_deque.data[--ptr_deque.head] =
                    allocator_traits<Alloc>::allocate(a, SQ_LIM);
            }
            --ptr_deque.beg;
            first = SQ_LIM - 1;
        }
        try {
            allocator_traits<Alloc>::
                construct(a, ptr_deque.data[ptr_deque.beg]
                          + first, glglT::forward<Args>(args)...);
        } catch (...) {
            ++first;
            if (first == SQ_LIM) {
                ++ptr_deque.beg;
                first = 0;
            }
            throw;
        }
    }
};

template <typename T, typename A> inline
void swap(deque<T, A> &d1, deque<T, A> &d2)
{
    d1.swap(d2);
}

template <typename T, typename A>
bool operator==(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    for (auto it1 = d1.begin(), it2 = d2.begin(); ; ++it1, ++it2) {
        if (it1 == d1.end())
            return it2 == d2.end();
        if (it2 == d2.end())
            return false;
        if (*it1 != *it2)
            return false;
    }   return true;
}

template <typename T, typename A> inline
bool operator!=(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    return !(d1 == d2);
}

template <typename T, typename A>
bool operator<(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    for (auto it1 = d1.begin(), it2 = d2.begin(); ; ++it1, ++it2) {
        if (it1 == d1.end())
            return true;
        if (it2 == d2.end())
            return false;
        if (*it1 < *it2)
            return true;
        if (*it2 < *it1)
            return false;
    }
}

template <typename T, typename A> inline
bool operator>(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    return (d2 < d1);
}

template <typename T, typename A> inline
bool operator>=(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    return !(d1 < d2);
}

template <typename T, typename A> inline
bool operator<=(const deque<T, A> &d1, const deque<T, A> &d2) noexcept
{
    return !(d2 < d1);
}

} // namespace glglT

#endif // GLGL_DEQUE_H
