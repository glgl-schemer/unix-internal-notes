#ifndef GLGL_OPTIONAL_H
#define GLGL_OPTIONAL_H

#include <exception>
#include "sub/glgl_type_tool.h"

namespace glglT {

struct nullopt_t {
    constexpr explicit nullopt_t(int) {}
};

constexpr nullopt_t nullopt = nullopt_t(0);

class bad_optional_access: public std::exception {
    constexpr static const char *s = "bad optional access";
public:
    bad_optional_access() = default;
    virtual const char *what() noexcept { return s; }
};

template <typename T>
class optional {
    bool have;
    typename aligned_storage<sizeof(T), alignof(T)>::type data;
public:
    constexpr optional() noexcept: have(false) {}
    constexpr explicit optional(nullopt_t) noexcept: have(false) {}
    optional(const optional &o) noexcept(noexcept(T(glglT::declval<const T &>()))):
        have(o.have)
    {
        if (have)
            ::new (&data) T (*reinterpret_cast<const T *>(&(o.data)));
    }

    optional(optional &&o) noexcept(noexcept(T(glglT::declval<T>()))): have(o.have)
    {
        if (have)
            ::new (&data) T (glglT::move(*reinterpret_cast<const T *>(&(o.data))));
    }

    optional(const T &t) noexcept(noexcept(T(glglT::declval<const T &>()))):
        have(true) { ::new (&data) T (t); }
    optional(T &&t) noexcept(noexcept(T(glglT::declval<T>()))):
        have(true) { ::new (&data) T (glglT::move(t)); }

    ~optional() { if (have) reinterpret_cast<T *>(&data)->~T(); }

    optional &operator=(nullopt_t) noexcept
    {
        if (have)
            reinterpret_cast<T *>(&data)->~T();
        have = false;
        return *this;
    }

    optional &operator=(const optional &o) noexcept(noexcept(T(glglT::declval<const T &>())))
    {
        if (have)
            reinterpret_cast<T *>(&data)->~T();
        have = o.have;
        if (o.have)
            ::new (&data) T (*reinterpret_cast<const T *>(&(o.data)));
        return *this;
    }

    optional &operator=(optional &&o) noexcept(noexcept(T(glglT::declval<T>())))
    {
        if (have)
            reinterpret_cast<T *>(data.data)->~T();
        have = o.have;
        if (o.have)
            ::new (&data) T (glglT::move(*reinterpret_cast<const T *>(&(o.data))));
        return *this;
    }

    template <typename O>
    optional &operator=(O &&o)
        noexcept(noexcept(T(glglT::declval<O>())) &&
                 noexcept(glglT::declval<T &>() = glglT::declval<O>()))
    {
        const bool ori_have = have;
        have = true;
        if (ori_have)
            *reinterpret_cast<T *>(&data) = glglT::forward<O>(o);
        else
            ::new (&data) T (glglT::forward<O>(o));
        return *this;
    }

    T *operator->() noexcept { return reinterpret_cast<T *>(&data); }
    const T *operator->() const noexcept
    {
        return reinterpret_cast<const T *>(&data);
    }

    T &operator*() & noexcept { return *reinterpret_cast<T *>(&data); }
    const T &operator*() const & noexcept
    {
        return *reinterpret_cast<const T *>(&data);
    }

    T &operator*() && noexcept { return glglT::move(*reinterpret_cast<T *>(&data)); }
    const T &&operator*() const && noexcept
    {
        return glglT::move(*reinterpret_cast<const T *>(&data));
    }

    explicit operator bool() const noexcept { return have; }
    bool empty() const noexcept { return !have; }

    T &value() &
    {
        if (!have)
            throw bad_optional_access();
        return **this;
    }

    const T &value() const &
    {
        if (!have)
            throw bad_optional_access();
        return **this;
    }

    T &&value() &&
    {
        if (!have)
            throw bad_optional_access();
        return glglT::move(**this);
    }

    const T &&value() const &&
    {
        if (!have)
            throw bad_optional_access();
        return glglT::move(**this);
    }

    template <typename O>
    T value_or(O &&o) const &
    {
        return have ? **this : T(glglT::forward<O>(o));
    }

    template <typename O>
    T value_or(O &&o) &&
    {
        return have ? glglT::move(**this) : T(glglT::forward<O>(o));
    }

    void reset() noexcept
    {
        if (have)
            reinterpret_cast<T *>(&data)->~T();
        have = false;
    }

    template <typename... Args>
    void emplace(Args&&... args)
    {
        if (have)
            reinterpret_cast<T *>(&data)->~T();
        have = true;
        ::new (&data) T (glglT::forward<Args>(args)...);
    }

    void swap(optional &o)
    {
        using glglT::swap;
        if (!have) {
            *this = glglT::move(o);
            o.reset();
        } else if (!o.have) {
            o = glglT::move(*this);
            this->reset();
        } else
            swap(*reinterpret_cast<T *>(data.data),
                 *reinterpret_cast<T *>(o.data.data));
    }
};

template <typename T> inline
void swap(optional<T> &o1, optional<T> &o2)
{
    o1.swap(o2);
}

template <typename T> inline
bool operator==(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return o1.empty() ? o2.empty() : !o2.empty() && *o1 == *o2;
}

template <typename T> inline
bool operator!=(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return !(o1 == o2);
}

template <typename T> inline
bool operator<(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return o1.empty() ? !o2.empty() : !o2.empty() && *o1 < *o2;
}

template <typename T> inline
bool operator>=(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return !(o1 < o2);
}

template <typename T> inline
bool operator>(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return o2 < o1;
}

template <typename T> inline
bool operator<=(const optional<T> &o1, const optional<T> &o2) noexcept
{
    return !(o1 > o2);
}

template <typename T> inline
optional<typename decay<T>::type> make_optional(T &&t)
    noexcept(noexcept(typename decay<T>::type(glglT::declval<T>())))
{
    return optional<typename decay<T>::type>(glglT::forward<T>(t));
}

template <typename T, typename... Args> inline
optional<T> make_optional(Args&&... args)
{
    optional<T> o;
    o.emplace(glglT::forward<Args>(args)...);
    return o;
}

} // namespace glglT

#endif // GLGL_OPTIONAL_H
