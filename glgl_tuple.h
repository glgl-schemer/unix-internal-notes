#ifndef GLGL_TUPLE_H
#define GLGL_TUPLE_H

#include "sub/glgl_pair.h"
#include "sub/glgl_tuple_im.h"

namespace glglT {

template <typename T1, typename T2>
template <typename Other1, typename Other2> inline
tuple_helper<T1, T2>::tuple_helper(const pair<Other1, Other2> &p):
    t1(p.first), t2(p.second) {}

template <typename T1, typename T2>
template <typename Other1, typename Other2> inline
tuple_helper<T1, T2>::tuple_helper(pair<Other1, Other2> &&p):
    t1(glglT::forward<Other1>(p.first)), t2(glglT::forward<Other2>(p.second)) {}

template <typename T1, typename T2>
template <typename Other1, typename Other2> inline tuple_helper<T1, T2> &
tuple_helper<T1, T2>::operator=(const pair<Other1, Other2> &p)
{
    t1 = p.first;
    t2 = p.second;
    return *this;
}

template <typename T1, typename T2>
template <typename Other1, typename Other2> inline tuple_helper<T1, T2> &
tuple_helper<T1, T2>::operator=(pair<Other1, Other2> &&p)
{
    t1 = glglT::forward<Other1>(p.first);
    t2 = glglT::forward<Other2>(p.second);
    return *this;
}

template <typename T1, typename T2>
template <typename... Args1, typename... Args2,
          size_t... I1, size_t... I2> inline
pair<T1, T2>::pair(tuple<Args1...> &&t1, tuple<Args2...> &&t2,
                   index_holder<I1...>, index_holder<I2...>):
    first(glglT::get<I1>(glglT::move(t1))...), second(glglT::get<I2>(glglT::move(t2))...) {}

template <typename T1, typename T2>
template <typename... Args1, typename... Args2> inline
pair<T1, T2>::pair(piecewise_construct_t,
                   tuple<Args1...> t1, tuple<Args2...> t2):
    pair(glglT::move(t1), move(t2),
         decltype(make_single_index(t1))(),
         decltype(make_single_index(t2))()) {}

template <typename T1, typename T2>
template <typename... Args1, size_t... I1> inline
pair<T1, T2>::pair(tuple<Args1...> &&t1,index_holder<I1...>, int):
    first(glglT::get<I1>(glglT::move(t1))...), second() {}

template <typename T1, typename T2>
template <typename... Args2, size_t... I2> inline
pair<T1, T2>::pair(tuple<Args2...> &&t2,index_holder<I2...>, unsigned):
    first(), second(glglT::get<I2>(glglT::move(t2))...) {}

template <typename T1, typename T2>
template <typename... Args1> inline
pair<T1, T2>::pair(piecewise_construct_t, tuple<Args1...> t1, tuple<>):
    pair(glglT::move(t1), decltype(make_single_index(t1))(), 0) {}

template <typename T1, typename T2>
template <typename... Args2> inline
pair<T1, T2>::pair(piecewise_construct_t, tuple<>, tuple<Args2...> t2):
    pair(glglT::move(t2), decltype(make_single_index(t2))(), 0u) {}

template <typename T1, typename T2> inline
pair<T1, T2>::pair(piecewise_construct_t, tuple<>, tuple<>):
    first(), second() {}

} // namespace glglT


#endif // GLGL_TUPLE_H
