#ifndef GLGL_SSTREAM_H
#define GLGL_SSTREAM_H

#include "glgl_string.h"

namespace glglT {

template <typename C, typename Traits = std::char_traits<C>,
          typename Alloc = allocator<C>>
class basic_istringstream: public std::basic_istream<C, Traits> {
    basic_stringbuf<C, Traits, Alloc> buf;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef typename Traits::int_type int_type;
    typedef typename Traits::pos_type pos_type;
    typedef typename Traits::off_type off_type;
    typedef Alloc allocator_type;

    explicit basic_istringstream(std::ios_base::openmode w = std::ios_base::in):
        std::basic_istream<C, Traits>(&buf), buf(w | std::ios_base::in) {}
    explicit basic_istringstream(const glglT::basic_string<C, Traits, Alloc> &s,
                                 std::ios_base::openmode w = std::ios_base::in):
        std::basic_istream<C, Traits>(&buf), buf(s, w | std::ios_base::in) {}
    basic_istringstream(basic_istringstream &&o) noexcept:
        std::basic_istream<C, Traits>(glglT::move(o)), buf(glglT::move(o.buf)) {}

    basic_istringstream &operator=(basic_istringstream &&o) noexcept
    {
        std::basic_istream<C, Traits>::operator=(glglT::move(o));
        buf = glglT::move(o.buf);
        return *this;
    }

    void swap(basic_istringstream &o)
    {
        std::basic_istream<C, Traits>::swap(o);
        buf.swap(o.buf);
    }

    basic_stringbuf<C, Traits, Alloc> *rdbuf() const noexcept
    {
        return const_cast<basic_stringbuf<C, Traits, Alloc> *>(&buf);
    }

    basic_string<C, Traits, Alloc> str() const { return buf.str(); }
    void str(const basic_string<C, Traits, Alloc> &s) { buf.str(s); }
};

template <typename C, typename T, typename A> inline
void swap(basic_istringstream<C, T, A> &s1, basic_istringstream<C, T, A> &s2)
{
    s1.swap(s2);
}

typedef basic_istringstream<char> istringstream;
typedef basic_istringstream<wchar_t> wistringstream;

template <typename C, typename Traits = std::char_traits<C>,
          typename Alloc = allocator<C>>
class basic_ostringstream: public std::basic_ostream<C, Traits> {
    basic_stringbuf<C, Traits, Alloc> buf;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef typename Traits::int_type int_type;
    typedef typename Traits::pos_type pos_type;
    typedef typename Traits::off_type off_type;
    typedef Alloc allocator_type;

    explicit basic_ostringstream(std::ios_base::openmode w = std::ios_base::out):
        std::basic_ostream<C, Traits>(&buf), buf(w | std::ios_base::out) {}
    explicit basic_ostringstream(const glglT::basic_string<C, Traits, Alloc> &s,
                                 std::ios_base::openmode w = std::ios_base::out):
        std::basic_ostream<C, Traits>(&buf), buf(s, w | std::ios_base::out) {}
    basic_ostringstream(basic_ostringstream &&o) noexcept:
        std::basic_ostream<C, Traits>(glglT::move(o)), buf(glglT::move(o.buf)) {}

    basic_ostringstream &operator=(basic_ostringstream &&o) noexcept
    {
        std::basic_ostream<C, Traits>::operator=(glglT::move(o));
        buf = glglT::move(o.buf);
        return *this;
    }

    void swap(basic_ostringstream &o)
    {
        std::basic_ostream<C, Traits>::swap(o);
        buf.swap(o.buf);
    }

    basic_stringbuf<C, Traits, Alloc> *rdbuf() const noexcept
    {
        return const_cast<basic_stringbuf<C, Traits, Alloc> *>(&buf);
    }

    basic_string<C, Traits, Alloc> str() const { return buf.str(); }
    void str(const basic_string<C, Traits, Alloc> &s) { buf.str(s); }
};

template <typename C, typename T, typename A> inline
void swap(basic_ostringstream<C, T, A> &s1, basic_ostringstream<C, T, A> &s2)
{
    s1.swap(s2);
}

typedef basic_ostringstream<char> ostringstream;
typedef basic_ostringstream<wchar_t> wostringstream;

template <typename C, typename Traits = std::char_traits<C>,
          typename Alloc = allocator<C>>
class basic_stringstream: public std::basic_iostream<C, Traits> {
    basic_stringbuf<C, Traits, Alloc> buf;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef typename Traits::int_type int_type;
    typedef typename Traits::pos_type pos_type;
    typedef typename Traits::off_type off_type;
    typedef Alloc allocator_type;

    explicit basic_stringstream(std::ios_base::openmode w =
                                std::ios_base::in | std::ios_base::out):
        std::basic_iostream<C, Traits>(&buf), buf(w | std::ios_base::in | std::ios_base::out) {}
    explicit basic_stringstream(const glglT::basic_string<C, Traits, Alloc> &s,
                                std::ios_base::openmode w =
                                std::ios_base::in | std::ios_base::out):
        std::basic_iostream<C, Traits>(&buf), buf(s, w | std::ios_base::in | std::ios_base::out) {}
    basic_stringstream(basic_stringstream &&o) noexcept:
        std::basic_iostream<C, Traits>(glglT::move(o)), buf(glglT::move(o.buf)) {}

    basic_stringstream &operator=(basic_stringstream &&o) noexcept
    {
        std::basic_iostream<C, Traits>::operator=(glglT::move(o));
        buf = glglT::move(o.buf);
        return *this;
    }

    void swap(basic_stringstream &o)
    {
        std::basic_iostream<C, Traits>::swap(o);
        buf.swap(o.buf);
    }

    basic_stringbuf<C, Traits, Alloc> *rdbuf() const noexcept
    {
        return const_cast<basic_stringbuf<C, Traits, Alloc> *>(&buf);
    }

    basic_string<C, Traits, Alloc> str() const { return buf.str(); }
    void str(const basic_string<C, Traits, Alloc> &s) { buf.str(s); }
};

template <typename C, typename T, typename A> inline
void swap(basic_stringstream<C, T, A> &s1, basic_stringstream<C, T, A> &s2)
{
    s1.swap(s2);
}

typedef basic_stringstream<char> stringstream;
typedef basic_stringstream<wchar_t> wstringstream;

} // namespace glglT

#endif // GLGL_SSTREAM_H
