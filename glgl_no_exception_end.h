#ifndef GLGL_NO_EXCEPTION_END_H
#define GLGL_NO_EXCEPTION_END_H

#include "glgl_no_exception_begin.h"

#ifndef __cpp_exceptions

#undef try
#undef catch
#undef throw

#endif // __cpp_exceptions

#endif // GLGL_NO_EXCEPTION_END_H
