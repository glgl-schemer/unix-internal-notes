#ifndef GLGL_FIBONACCI_HEAP_H
#define GLGL_FIBONACCI_HEAP_H

#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_memory_tool.h"
#include "sub/glgl_function_object.h"
#include "sub/glgl_mutable_heap_base.h"

namespace glglT {

template <typename T, typename Alloc>
struct fib_node_t {
    fib_node_t() = default;
    template <typename... Args>
    explicit fib_node_t(Args&&... args): t(glglT::forward<Args>(args)...) {}
    T t;
    bool mark;
    size_t degree;
    typename allocator_traits<Alloc>::
        template rebind_traits<fib_node_t>::pointer p;
    typename allocator_traits<Alloc>::
        template rebind_traits<fib_node_t>::pointer c;
    typename allocator_traits<Alloc>::
        template rebind_traits<fib_node_t>::pointer l;
    typename allocator_traits<Alloc>::
        template rebind_traits<fib_node_t>::pointer r;
};

template <typename T, typename RPtr>
class fib_const_iterator: public iterator<bidirectional_iterator_tag, const T> {
    RPtr curr;
    RPtr eoh;
public:
    constexpr fib_const_iterator() noexcept {}
    fib_const_iterator(RPtr c, RPtr h): curr(c), eoh(h) {}

    const T &operator*() const noexcept { return curr->t; }
    const T *operator->() const noexcept { return &(curr->t); }
    const RPtr &base() const noexcept { return curr; }
    fib_const_iterator &operator++() noexcept
    {
        if (!curr->degree) {
            while (curr->r == eoh && curr->p)
                curr = curr->p;
            curr = curr->r;
        } else
            curr = curr->c;
        return *this;
    }

    fib_const_iterator &operator--() noexcept
    {
        if (curr->l->r != eoh) {
            curr = curr->l;
            while (curr->degree)
                curr = curr->c->l;
        } else if (curr->p)
            curr = curr->p;
        else
            curr = curr->l;
        return *this;
    }

    fib_const_iterator operator++(int) noexcept
    {
        fib_const_iterator r = *this;
        this->operator++();
        return r;
    }

    fib_const_iterator operator--(int) noexcept
    {
        fib_const_iterator r = *this;
        this->operator--();
        return r;
    }

    bool operator==(const fib_const_iterator &r) const noexcept
    {
        return curr == r.curr;
    }

    bool operator!=(const fib_const_iterator &r) const noexcept
    {
        return curr != r.curr;
    }
};

template <typename T, typename Comp = less<T>, typename Alloc = allocator<T>>
class fibonacci_heap:
    public mutable_heap_base<T, Comp, Alloc, fibonacci_heap<T, Comp, Alloc>> {
    friend class mutable_heap_base<T, Comp, Alloc, fibonacci_heap>;
    typedef typename heap_iter_adaptor<fibonacci_heap>::rebind_alloc rebind_alloc;
    typedef typename heap_iter_adaptor<fibonacci_heap>::rebind_traits rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    struct move_tag {};

    Comp comp;
    rebind_alloc a;
    rebind_ptr eoh;
    struct { rebind_ptr *p; size_t l; } tmp;
    size_t len;
public:
    typedef fib_const_iterator<T, rebind_ptr> iterator;
    typedef iterator const_iterator;

    explicit fibonacci_heap(const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(rebind_traits::allocate(a, 1)), len() { this->init_heap(); }

    explicit fibonacci_heap(const Alloc &ac): comp(), a(ac),
        eoh(rebind_traits::allocate(a, 1)), len() { this->init_heap(); }

    template <typename It>
    fibonacci_heap(It b, It e, const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        this->push(b, e);
    }

    template <typename It, typename OutputIter>
    fibonacci_heap(It b, It e, OutputIter oiter,
                   const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        this->push(b, e, oiter);
    }

    fibonacci_heap(const fibonacci_heap &o): comp(o.comp),
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), glglT::ignore, o.top());
    }

    template <typename OutputIter>
    fibonacci_heap(const fibonacci_heap &o, OutputIter oiter): comp(o.comp),
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), oiter, o.top());
    }

    template <typename OutputIter = tuple<>>
    fibonacci_heap(const fibonacci_heap &o, const Alloc &ac, OutputIter may = glglT::ignore):
        comp(o.comp), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top());
    }

    fibonacci_heap(fibonacci_heap &&o): comp(glglT::move(o.comp)),
        a(glglT::move(o.a)), eoh(rebind_traits::allocate(a, 1)), tmp(o.tmp), len(o.len)
    {
        eoh->l = eoh->r = eoh;
        eoh->p = nullptr;
        eoh->degree = 0;
        o.len = o.tmp.l = 0;
        glglT::swap(eoh, o.eoh);
    }

    template <typename OutputIter = tuple<>>
    fibonacci_heap(fibonacci_heap &&o, const Alloc &ac, OutputIter may = glglT::ignore):
        comp(glglT::move(o.comp)), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        if (a == o.a) {
            glglT::swap(len, o.len);
            glglT::swap(tmp, o.tmp);
            glglT::swap(eoh, o.eoh);
        } else if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top(), move_tag());
    }

    fibonacci_heap(std::initializer_list<T> il,
                   const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        this->push(il.begin(), il.end());
    }

    template <typename OutputIter>
    fibonacci_heap(std::initializer_list<T> il, OutputIter oiter,
                   const Comp &c = Comp(), const Alloc &ac = Alloc()):
        comp(c), a(ac), eoh(rebind_traits::allocate(a, 1)), len()
    {
        this->init_heap();
        this->push(il.begin(), il.end(), oiter);
    }

    ~fibonacci_heap()
    {
        if (len) this->free(eoh->r);
        if (tmp.l) {
            glglT::uninitialized_destroy(tmp.p, tmp.l);
            ::operator delete(tmp.p);
        }   rebind_traits::deallocate(a, eoh, 1);
    }

    fibonacci_heap &operator=(const fibonacci_heap &o)
    {
        this->assign(o);
        return *this;
    }

    fibonacci_heap &operator=(fibonacci_heap &&o)
    {
        this->assign(glglT::move(o));
        return *this;
    }

    fibonacci_heap &operator=(std::initializer_list<T> il)
    {
        this->assign(il);
        return *this;
    }

    template <typename OutputIter = tuple<>>
    void assign(std::initializer_list<T> il, OutputIter may = glglT::ignore)
    {
        this->assign(il.begin(), il.end(), may);
    }

    template <typename It, typename OutputIter = tuple<>>
    void assign(It b, It e, OutputIter may = glglT::ignore)
    {
        this->clear();
        this->push(b, e, may);
    }

    template <typename OutputIter = tuple<>>
    void assign(const fibonacci_heap &o, OutputIter may = glglT::ignore)
    {
        if (eoh == o.eoh)
            return;
        this->clear();
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        comp = o.comp;
        if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top());
    }

    template <typename OutputIter = tuple<>>
    void assign(fibonacci_heap &&o, OutputIter may = glglT::ignore)
    {
        if (eoh == o.eoh)
            return;
        this->clear();
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        comp = o.comp;
        if (a != o.a) {
            glglT::swap(len, o.len);
            glglT::swap(tmp, o.tmp);
            glglT::swap(eoh, o.eoh);
        } else if (o.size())
            this->p_insert(o.begin(), o.end(), may, o.top(), move_tag());
    }

    const_iterator begin() const noexcept { return const_iterator(eoh->r, eoh); }
    const_iterator end() const noexcept { return const_iterator(eoh, eoh); }

    bool empty() const noexcept { return len == 0; }
    size_t size() const noexcept { return len; }
    const T &top() const noexcept { return eoh->c->t; }

    template <typename It, typename OutputIter = tuple<>>
    void push(It b, It e, OutputIter may = glglT::ignore)
    {
        if (b == e)
            return;
        const auto res = this->emplace(*b);
        while (++b != e) {
            const auto node = this->make_new_node(*b);
            this->insert_in_non_empty_heap(node);
            *may++ = iterator(node, eoh);
        }   *may++ = res;
    }

    template <typename... Args>
    iterator emplace(Args&&... args)
    {
        const auto node = this->make_new_node(glglT::forward<Args>(args)...);
        if (!len) {
            eoh->r = eoh->l = node;
            node->l = node->r = eoh;
            eoh->c = node;
            node->p = nullptr;
            node->degree = 0;
            node->mark = false;
            len = 1;
        } else
            this->insert_in_non_empty_heap(node);
        return iterator(node, eoh);
    }

    iterator erase(const_iterator itr)
    {
        this->decrease_key(itr.base(), T(this->top()));
        this->pop();
        return this->end();
    }

    void pop()
    {
        const auto m = this->pop_without_dealloc();
        rebind_traits::destroy(a, m);
        rebind_traits::deallocate(a, m, 1);
    }

    void clear() noexcept
    {
        if (len) {
            this->free(eoh->r);
            eoh->l = eoh->r = eoh;
        }   len = 0;
    }

    template <typename... Args>
    void update(const_iterator itr, Args&&... args)
    {
        T t(glglT::forward<Args>(args)...);
        if (this->comp(t, *itr))
            this->decrease_key(itr.base(), glglT::move(t));
        else if (this->comp(*itr, t))
            this->increase_key(itr.base(), glglT::move(t));
    }

    template <typename... Args>
    void increase(const_iterator itr, Args&&... args)
    {
        this->increase_key(itr.base(), T(glglT::forward<Args>(args)...));
    }

    template <typename... Args>
    void decrease(const_iterator itr, Args&&... args)
    {
        this->decrease_key(itr.base(), T(glglT::forward<Args>(args)...));
    }

    void merge(fibonacci_heap &&o) { this->merge(o); }
    void merge(fibonacci_heap &o)
    {
        if (o.len == 0)
            return;
        if (len == 0)
            *this = glglT::move(o);
        else if (a == o.a) {
            const auto o_smaller = this->comp(o.eoh->c->t, eoh->c->t);
            const auto last = eoh->l;
            eoh->l = o.eoh->l;
            eoh->l->r = eoh;
            o.eoh->r->l = last;
            last->r = o.eoh->r;
            if (o_smaller)
                eoh->c = o.eoh->c;
            len += o.len;
            o.len = 0;
            o.eoh->l = o.eoh->r = o.eoh;
        } else
            this->p_insert(o.begin(), o.end(), glglT::ignore, o.top(), move_tag());
    }

    void swap(fibonacci_heap &o)
    {
        using glglT::swap;
        glglT::alloc_swap(a, o.a,
                          typename rebind_traits::propagate_on_container_swap());
        swap(comp, o.comp);
        glglT::swap(eoh, o.eoh);
        glglT::swap(tmp, o.tmp);
        glglT::swap(len, o.len);
    }
private:
    void init_heap() noexcept
    {
        eoh->l = eoh->r = eoh;
        eoh->p = nullptr;
        eoh->degree = 0;
        tmp.l = 0;
    }

    template <typename It, typename OutputIter, typename... MOVE_TAG>
    void p_insert(It b, It e, OutputIter oiter, const T &tmin, MOVE_TAG... movetag)
    {
        if (!len) {
            const auto node = this->make_new_node_from_iter(*b, movetag...);
            eoh->r = eoh->l = node;
            node->l = node->r = eoh;
            eoh->c = node;
            node->p = nullptr;
            node->degree = 0;
            node->mark = false;
            len = 1;
            *oiter++ = iterator(node, eoh);
        }
        const auto tmin_smaller = this->comp(tmin, eoh->c->t);
        if (tmin_smaller) {
            while (++b != e) {
                const auto node = this->make_new_node_from_iter(*b, movetag...);
                this->insert_in_non_empty_heap(node);
                *oiter++ = iterator(node, eoh);
            }
        } else {
            for (; ++b != e; ++len) {
                const auto node = this->make_new_node_from_iter(*b, movetag...);
                this->insert_into_root_list(node);
                *oiter++ = iterator(node, eoh);
            }
        }
    }

    void insert_in_non_empty_heap(rebind_ptr node)
    {
        try {
            if (this->comp(node->t, eoh->c->t))
                eoh->c = node;
        } catch(...) {
            rebind_traits::destroy(a, node);
            rebind_traits::deallocate(a, node, 1);
            throw;
        }
        this->insert_into_root_list(node);
        ++len;
    }

    void insert_into_root_list(rebind_ptr node) noexcept
    {
        node->l = eoh->l;
        eoh->l->r = node;
        eoh->l = node;
        node->r = eoh;
        node->p = nullptr;
        node->degree = 0;
        node->mark = false;
    }

    template <typename O>
    rebind_ptr make_new_node_from_iter(O &&o, move_tag)
    {
        return this->make_new_node(glglT::move(o));
    }

    template <typename O>
    rebind_ptr make_new_node_from_iter(O &&o)
    {
        return this->make_new_node(glglT::forward<O>(o));
    }

    template <typename... Args>
    rebind_ptr make_new_node(Args&&... args)
    {
        const auto p = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, p, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, p, 1);
            throw;
        }
        return p;
    }

    rebind_ptr pop_without_dealloc()
    {
        const rebind_ptr m = eoh->c;
        if (m->degree) {
            m->c->l->r = m->r;
            m->r->l = m->c->l;
            m->c->l = m->l;
            m->l->r = m->c;
            for (auto p = m->c; p != m->r; p = p->r)
                p->p = nullptr;
        } else {
            m->l->r = m->r;
            m->r->l = m->l;
        }
        try {
            if (len != 1)
                this->consolidate();
        } catch(...) {
            this->insert_into_root_list(m);
            eoh->c = m;
            throw;
        }   --len;
        return m;
    }

    void consolidate() ///eoh->c when exception
    {
        glglT::pointer_emplace_n(tmp.p, tmp.l);
        size_t max_loop = 0;
        for (rebind_ptr node = eoh->r, next; ;node = next) {
            for (next = node->r; ; tmp.p[node->degree++] = nullptr) {
                if (node->degree >= tmp.l) {
                    if (tmp.l) {
                        try {
                            this->extend_tmp(2 * node->degree);
                        } catch(...) {
                            node->l = eoh;
                            eoh->r = node;
                            this->exception_in_consolidate(max_loop + 1);
                            throw;
                        }
                    } else
                        this->init_tmp();
                }
                if (tmp.p[node->degree] == nullptr)
                    break;
                try {
                    if (this->comp(tmp.p[node->degree]->t, node->t))
                        glglT::swap(node, tmp.p[node->degree]);
                } catch(...) {
                    node->l = eoh;
                    eoh->r = node;
                    this->exception_in_consolidate(max_loop + 1);
                    throw;
                }
                const auto new_c = tmp.p[node->degree];
                new_c->r = eoh;
                new_c->p = node;
                if (node->degree) {
                    new_c->l = node->c->l;
                    node->c->l->r = new_c;
                    node->c->l = new_c;
                } else {
                    new_c->l = new_c;
                    node->c = new_c;
                }
            }
            if (node->degree > max_loop)
                max_loop = node->degree;
            tmp.p[node->degree] = node;
            if (next == eoh)
                break;
        }
        eoh->c = eoh->l = eoh->r = tmp.p[max_loop];
        tmp.p[max_loop]->r = tmp.p[max_loop]->l = eoh;
        while (max_loop-- != 0) {
            if (tmp.p[max_loop] == nullptr)
                continue;
            try {
                if (this->comp(tmp.p[max_loop]->t, eoh->c->t))
                    eoh->c = tmp.p[max_loop];
            } catch(...) {
                this->exception_in_consolidate(max_loop + 1);
                throw;
            }
            tmp.p[max_loop]->l = eoh->l;
            eoh->l->r = tmp.p[max_loop];
            eoh->l = tmp.p[max_loop];
            tmp.p[max_loop]->r = eoh;
        }
    }

    void extend_tmp(size_t new_l)
    {
        const auto new_p = static_cast<rebind_ptr *>(
            ::operator new(sizeof(rebind_ptr) * new_l));
        try {
            glglT::uninitialized_emplace_n(
                glglT::uninitialized_copy_n(tmp.p, tmp.l, new_p), new_l - tmp.l);
        } catch(...) {
            ::operator delete(new_p);
            throw;
        }
        glglT::uninitialized_destroy(tmp.p, tmp.l);
        ::operator delete(tmp.p);
        tmp.p = new_p;
        tmp.l = new_l;
    }

    void init_tmp()
    {
        tmp.p = static_cast<rebind_ptr *>(::operator new(sizeof(rebind_ptr) * 16));
        glglT::uninitialized_emplace_n(tmp.p, 16);
        tmp.l = 16;
    }

    void exception_in_consolidate(size_t max_loop) noexcept
    {
        while (max_loop-- != 0) {
            if (tmp.p[max_loop] == nullptr)
                continue;
            tmp.p[max_loop]->l = eoh->l;
            eoh->l->r = tmp.p[max_loop];
            eoh->l = tmp.p[max_loop];
            tmp.p[max_loop]->r = eoh;
        }
    }

    void decrease_key(rebind_ptr node, T &&t)
    {
        const auto ori_node = node;
        if (node->p && this->comp(t, node->p->t)) {
            for (auto p = node->p; ; node = p, p = p->p) {
                --(p->degree);
                if (node == p->c) {
                    if (p->degree) {
                        p->c = node->r;
                        node->r->l = node->l;
                    }
                } else {
                    if (p->c->l != node)
                        node->r->l = node->l;
                    else
                        p->c->l = node->l;
                    node->l->r = node->r;
                }
                node->l = eoh->l;
                eoh->l->r = node;
                node->r = eoh;
                eoh->l = node;
                node->p = nullptr;
                node->mark = false;
                if (p->p == nullptr)
                    break;
                if (p->mark == false) {
                    p->mark = true;
                    break;
                }
            }
        }
        if (this->comp(t, eoh->c->t))
            eoh->c = ori_node;
        ori_node->t = glglT::move(t);
    }

    void increase_key(rebind_ptr node, T &&t)
    {
        auto old_v = glglT::move(node->t);
        node->t = glglT::move(t);
        if (node->degree) {
            rebind_ptr last = node->c->l;
            node->c->l = eoh->l;
            eoh->l->r = node->c;
            eoh->l = last;
            for (last = node->c; last != eoh; last = last->r)
                last->mark = last->p = nullptr;
        }   node->degree = 0;
        if (node == eoh->c)
            try {
                this->consolidate();
            } catch(...) {
                node->t = glglT::move(old_v);
                eoh->c = node;
                throw;
            }
    }

    void free(rebind_ptr bol) noexcept
    {
        for (rebind_ptr p = bol->p, tmp; ; bol = p, p = p->p) {
            do {
                while (bol->degree) {
                    bol->degree = 0;
                    p = bol;
                    bol = bol->c;
                }
                tmp = bol;
                bol = bol->r;
                rebind_traits::destroy(a, tmp);
                rebind_traits::deallocate(a, tmp, 1);
            } while (bol != eoh);
            if (p == nullptr) break;
        }
    }
};

template <typename T, typename C, typename A> inline
void swap(fibonacci_heap<T, C, A> &h1, fibonacci_heap<T, C, A> &h2)
{
    h1.swap(h2);
}

} // namespace glglT

#endif // GLGL_FIBONACCI_HEAP_H
