#ifndef GLGL_QUEUE_H
#define GLGL_QUEUE_H

#include "sub/glgl_sort_algo.h"
#include "sub/glgl_function_object.h"
#include "sub/glgl_container_adaptor.h"

namespace glglT {

template <typename, typename> class deque;
template <typename, typename> class queue;

template <typename T, typename C>
bool operator==(const queue<T, C> &, const queue<T, C> &);

template <typename T, typename C>
bool operator<(const queue<T, C> &, const queue<T, C> &);

template <typename T, typename C, typename A>
struct uses_allocator<queue<T, C>, A>: uses_allocator<C, A>::type {};

template <typename T, typename C = deque<T, allocator<T>>>
struct queue: container_adaptor_base<C> {
    friend bool operator==<>(const queue &, const queue &);
    friend bool operator< <>(const queue &, const queue &);
protected:
    C c;
public:
    explicit queue(const C &cn): c(cn) {}
    explicit queue(C &&cn = C()): c(glglT::move(cn)) {}
    queue(const queue &) = default;
    queue(queue &&) = default;
    queue &operator=(const queue &) = default;
    queue &operator=(queue &&) = default;

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    explicit queue(const Alloc &a): c(a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    queue(const C &cn, const Alloc &a): c(cn, a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    queue(C &&cn, const Alloc &a): c(glglT::move(cn), a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    queue(const queue &o, const Alloc &a):
        c(o.c, a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    queue(queue &&o, const Alloc &a):
        c(glglT::move(o.c), a) {}

    bool empty() const noexcept { return c.empty(); }
    decltype(c.size()) size() const noexcept { return c.size(); }

    decltype(declval<C &>().front()) front() noexcept(noexcept(declval<C &>().front()))
    {
        return c.front();
    }

    decltype(declval<const C &>().front()) front() const noexcept(noexcept(declval<const C &>().front()))
    {
        return c.front();
    }

    decltype(declval<C &>().back()) back() noexcept(noexcept(declval<C &>().back()))
    {
        return c.back();
    }

    decltype(declval<const C &>().back()) back() const noexcept(noexcept(declval<const C &>().back()))
    {
        return c.back();
    }

    void push(const typename C::value_type &v) { c.push_back(v); }
    void push(typename C::value_type &&v) { c.push_back(glglT::move(v)); }
    void pop() { c.pop_front(); }

    template <typename... Args> void emplace(Args&&... args)
    {
        c.emplace_back(glglT::forward<Args>(args)...);
    }

    void swap(queue &o)
    {
        using glglT::swap;
        swap(c, o.c);
    }
};

template <typename T, typename C> inline
bool operator==(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return s1.c == s2.c;
}

template <typename T, typename C> inline
bool operator<(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return s1.c < s2.c;
}

template <typename T, typename C> inline
bool operator!=(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return !(s1 == s2);
}

template <typename T, typename C> inline
bool operator>=(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return !(s1 < s2);
}

template <typename T, typename C> inline
bool operator>(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return s2 < s1;
}

template <typename T, typename C> inline
bool operator<=(const queue<T, C> &s1, const queue<T, C> &s2)
{
    return !(s1 > s2);
}

template <typename T, typename C> inline
void swap(queue<T, C> &s1, queue<T, C> &s2)
{
    s1.swap(s2);
}

template <typename, typename> class vector;
template <typename, typename, typename> class priority_queue;

template <typename T, typename C, typename Comp>
bool operator==(const priority_queue<T, C, Comp> &, const priority_queue<T, C, Comp> &);

template <typename T, typename C, typename Comp>
bool operator<(const priority_queue<T, C, Comp> &, const priority_queue<T, C, Comp> &);

template <typename T, typename C, typename Comp, typename A>
struct uses_allocator<priority_queue<T, C, Comp>, A>: uses_allocator<C, A>::type {};

template <typename T, typename C = vector<T, allocator<T>>,
          typename Comp = less<typename C::value_type>>
struct priority_queue: container_adaptor_base<C> {
    friend bool operator==<>(const priority_queue &, const priority_queue &);
    friend bool operator< <>(const priority_queue &, const priority_queue &);
protected:
    Comp comp;
    C c;
public:
    explicit priority_queue(const Comp &cp, const C &cn): comp(cp), c(cn) {}
    explicit priority_queue(const Comp &cp = Comp(), C &&cn = C()):
        comp(cp), c(glglT::move(cn)) {}
    priority_queue(const priority_queue &) = default;
    priority_queue(priority_queue &&) = default;
    priority_queue &operator=(const priority_queue &) = default;
    priority_queue &operator=(priority_queue &&) = default;

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    explicit priority_queue(const Alloc &a): c(a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    explicit priority_queue(const Comp &cp, const Alloc &a): comp(cp), c(a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    priority_queue(const Comp &cp, const C &cn, const Alloc &a):
        comp(cp), c(cn, a)
    {
        glglT::make_heap(c.begin(), c.end(), comp);
    }

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    priority_queue(const Comp &cp, C &&cn, const Alloc &a):
        comp(cp), c(glglT::move(cn), a)
    {
        glglT::make_heap(c.begin(), c.end(), comp);
    }

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    priority_queue(const priority_queue &o, const Alloc &a):
        c(o.c, a) {}

    template <typename Alloc,
              typename = typename enable_if<uses_allocator<C, Alloc>::value, Alloc>::type>
    priority_queue(priority_queue &&o, const Alloc &a):
        c(glglT::move(o.c), a) {}

    template <typename It>
    priority_queue(It b, It e, const Comp &cp, const C &cn): comp(cp), c(cn)
    {
        c.insert(c.end(), b, e);
        glglT::make_heap(c.begin(), c.end(), comp);
    }

    template <typename It>
    priority_queue(It b, It e, const Comp &cp = Comp(), C &&cn = C()):
        comp(cp), c(glglT::move(cn))
    {
        c.insert(c.end(), b, e);
        glglT::make_heap(c.begin(), c.end(), comp);
    }

    bool empty() const noexcept { return c.empty(); }
    decltype(c.size()) size() const noexcept { return c.size(); }

    decltype(declval<const C &>().front()) top() const noexcept(noexcept(declval<const C &>().front()))
    {
        return c.front();
    }

    void push(const typename C::value_type &v) { this->emplace(v); }
    void push(typename C::value_type &&v) { this->emplace(glglT::move(v)); }

    template <typename... Args>
    void emplace(Args&&... args)
    {
        c.emplace_back(glglT::forward<Args>(args)...);
        glglT::push_heap(c.begin(), c.end(), comp);
    }

    void pop()
    {
        glglT::pop_heap(c.begin(), c.end(), comp);
        c.pop_back();
    }

    void swap(priority_queue &o)
    {
        using glglT::swap;
        swap(c, o.c);
        swap(comp, o.comp);
    }
};

template <typename T, typename C, typename Comp> inline
void swap(priority_queue<T, C, Comp> &s1, priority_queue<T, C, Comp> &s2)
{
    s1.swap(s2);
}

} // namespace glglT

#endif // GLGL_QUEUE_H
