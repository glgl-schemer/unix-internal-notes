#ifndef GLGL_SCOPED_ALLOCATOR_H
#define GLGL_SCOPED_ALLOCATOR_H

#include "glgl_tuple.h"
#include "sub/glgl_allocator_traits.h"

namespace glglT {

template <typename O, typename... I>
struct test_alloc_copy {
    typedef typename conditional<
        test_alloc_copy<I...>::type::value ||
        allocator_traits<O>::propagate_on_container_copy_assignment::value,
    true_type, false_type>::type type;
};

template <typename O>
struct test_alloc_copy<O> {
    typedef typename allocator_traits<O>::
        propagate_on_container_copy_assignment
    type;
};

template <typename O, typename... I>
struct test_alloc_move {
    typedef typename conditional<
        test_alloc_copy<I...>::type::value ||
        allocator_traits<O>::propagate_on_container_move_assignment::value,
    true_type, false_type>::type type;
};

template <typename O>
struct test_alloc_move<O> {
    typedef typename allocator_traits<O>::
        propagate_on_container_move_assignment
    type;
};

template <typename O, typename... I>
struct test_alloc_swap {
    typedef typename conditional<
        test_alloc_copy<I...>::type::value ||
        allocator_traits<O>::propagate_on_container_swap::value,
    true_type, false_type>::type type;
};

template <typename O>
struct test_alloc_swap<O> {
    typedef typename allocator_traits<O>::propagate_on_container_swap type;
};

template <bool, bool, typename, typename>
struct construct0_helper;

template <bool B, typename O, typename I>
struct construct0_helper<false, B, O, I> {
    template <typename T, typename... Args> static
    void call(O &o, I &i, T *p, Args&&... args)
    {
        allocator_traits<O>::construct(o, p, glglT::forward<Args>(args)...);
    }
};

template <typename O, typename I>
struct construct0_helper<true, true, O, I> {
    template <typename T, typename... Args> static
    void call(O &o, I &i, T *p, Args&&... args)
    {
        allocator_traits<O>::construct(o, p, allocator_arg, i,
                                       glglT::forward<Args>(args)...);
    }
};

template <typename O, typename I>
struct construct0_helper<true, false, O, I> {
    template <typename T, typename... Args> static
    void call(O &o, I &i, T *p, Args&&... args)
    {
        allocator_traits<O>::construct(o, p, glglT::forward<Args>(args)..., i);
    }
};

template <bool, bool, typename>
struct construct1_helper;

template <bool B, typename Alloc>
struct construct1_helper<false, B, Alloc> {
    template <typename ...Args> static
    tuple<Args...> make_prime(Alloc &, tuple<Args...> &&t)
    {
        return glglT::move(t);
    }
};

template <typename Alloc>
struct construct1_helper<true, true, Alloc> {
    template <typename... Args> static
    auto make_prime(Alloc &a, tuple<Args...> &&t)
        -> tuple<allocator_arg_t, Alloc &, Args...>
    {
        return glglT::tuple_cat(allocator_arg, a, glglT::move(t));
    }
};

template <typename Alloc>
struct construct1_helper<true, false, Alloc> {
    template <typename... Args> static
    tuple<Args..., Alloc &> make_prime(Alloc &a, tuple<Args...> &&t)
    {
        return glglT::tuple_cat(glglT::move(t), a);
    }
};

template <typename O, typename... I>
class scoped_allocator_adaptor: public O {
    scoped_allocator_adaptor<I...> inners;
public:
    typedef O outer_allocator_type;
    typedef scoped_allocator_adaptor<I...> inner_allocator_type;
    typedef typename allocator_traits<O>::value_type value_type;
    typedef typename allocator_traits<O>::size_type size_type;
    typedef typename allocator_traits<O>::difference_type difference_type;
    typedef typename allocator_traits<O>::pointer pointer;
    typedef typename allocator_traits<O>::const_pointer const_pointer;
    typedef typename allocator_traits<O>::void_pointer void_pointer;
    typedef typename allocator_traits<O>::const_void_pointer const_void_pointer;
    typedef typename
    test_alloc_copy<O, I...>::type propagate_on_container_copy_assigment;
    typedef typename
    test_alloc_move<O, I...>::type propagate_on_container_move_assigment;
    typedef typename test_alloc_swap<O, I...>::type propagate_on_container_swap;

    template <typename T>
    struct rebind {
        typedef scoped_allocator_adaptor<
            typename allocator_traits<O>::template rebind_alloc<T>, I...> other;
    };
private:
    scoped_allocator_adaptor(const O &o, inner_allocator_type &i):
        inners(i),
        O(allocator_traits<O>::select_on_container_copy_construction(o)) {}
public:
    scoped_allocator_adaptor() = default;

    template <typename O2>
    scoped_allocator_adaptor(O2 &&o2, I... is)
        noexcept(noexcept(inner_allocator_type(glglT::forward<I>(is)...)) &&
                 noexcept(O(glglT::forward<O2>(o2)))):
        inners(glglT::forward<I>(is)...), O(glglT::forward<O2>(o2)) {}

    scoped_allocator_adaptor(const scoped_allocator_adaptor &o)
        noexcept(noexcept(inner_allocator_type(o.inners)) && noexcept(O(o))):
        inners(o.inners), O(o) {}

    scoped_allocator_adaptor(scoped_allocator_adaptor &&o)
        noexcept(noexcept(inner_allocator_type(glglT::move(o.inners))) &&
                 noexcept(O(glglT::move(o)))):
        inners(glglT::move(o.inners)), O(glglT::move(o)) {}

    template <typename O2>
    scoped_allocator_adaptor(const scoped_allocator_adaptor<O2, I...> &o)
        noexcept(noexcept(inner_allocator_type(o.inners)) && noexcept(O(o))):
        inners(o.inners), O(o) {}

    template <typename O2>
    scoped_allocator_adaptor(scoped_allocator_adaptor<O2, I...> &&o)
        noexcept(noexcept(inner_allocator_type(glglT::move(o.inners))) &&
                 noexcept(O(glglT::move(o)))):
        inners(glglT::move(o.inners)), O(glglT::move(o)) {}

    O &outer_allocator() noexcept { return *this; }
    const O &outer_allocator() const noexcept { return *this; }

    inner_allocator_type &inner_allocator() noexcept { return inners; }
    const inner_allocator_type &inner_allocator() const noexcept
    {
        return inners;
    }

    pointer allocate(size_type n)
    {
        return allocator_traits<O>::allocate(*this, n);
    }

    pointer allocate(size_type n, const_void_pointer h)
    {
        return allocator_traits<O>::allocate(*this, n, h);
    }

    void deallocate(pointer p, size_type n) noexcept
    {
        allocator_traits<O>::deallocate(*this, p, n);
    }

    constexpr size_type max_size() const noexcept
    {
        return allocator_traits<O>::max_size(*this);
    }

    template <typename T, typename... Args>
    void construct(T *p, Args&&... args)
    {
        construct0_helper<
            uses_allocator<T, inner_allocator_type>::value,
            is_constructible<T, allocator_arg_t,
                             inner_allocator_type, Args...>::value,
            O, inner_allocator_type>::
            call(*this, inners, p, glglT::forward<Args>(args)...);
    }

    template <typename T1, typename T2, typename... Args1, typename... Args2>
    void construct(pair<T1, T2> *p, piecewise_construct_t,
                   tuple<Args1...> t1, tuple<Args2...> t2)
    {
        auto prime1 = construct1_helper<
            uses_allocator<T1, inner_allocator_type>::value,
            is_constructible<T1, allocator_arg_t,
                             inner_allocator_type, Args1...>::value,
            inner_allocator_type>::make_prime(inners, glglT::move(t1));
        auto prime2 = construct1_helper<
            uses_allocator<T2, inner_allocator_type>::value,
            is_constructible<T2, allocator_arg_t,
                             inner_allocator_type, Args2...>::value,
            inner_allocator_type>::make_prime(inners, glglT::move(t2));
        allocator_traits<O>::construct(*this, p, piecewise_construct,
                                       glglT::move(prime1), glglT::move(prime2));
    }

    template <typename T1, typename T2>
    void construct(pair<T1, T2> *p)
    {
        construct(p, piecewise_construct, tuple<>(), tuple<>());
    }

    template <typename T1, typename T2, typename O1, typename O2>
    void construct(pair<T1, T2> *p, O1 &&o1, O2 &&o2)
    {
        construct(p, piecewise_construct,
                  forward_as_tuple(glglT::forward<O1>(o1)),
                  forward_as_tuple(glglT::forward<O2>(o2)));
    }

    template <typename T1, typename T2, typename O1, typename O2>
    void construct(pair<T1, T2> *p, pair<O1, O2> &&op)
    {
        construct(p, piecewise_construct,
                  forward_as_tuple(glglT::forward<O1>(op.first)),
                  forward_as_tuple(glglT::forward<O2>(op.second)));
    }

    template <typename T>
    void destroy(T *p)
    {
        allocator_traits<O>::destroy(*this, p);
    }

    scoped_allocator_adaptor select_on_container_copy_construction() const
    {
        return scoped_allocator_adaptor(*this, inners);
    }
};

template <typename O>
class scoped_allocator_adaptor<O>: public O {
    scoped_allocator_adaptor(const O &o):
        O(allocator_traits<O>::select_on_container_copy_construction(o)) {}
public:
    typedef O outer_allocator_type;
    typedef scoped_allocator_adaptor<O> inner_allocator_type;
    typedef typename allocator_traits<O>::value_type value_type;
    typedef typename allocator_traits<O>::size_type size_type;
    typedef typename allocator_traits<O>::difference_type difference_type;
    typedef typename allocator_traits<O>::pointer pointer;
    typedef typename allocator_traits<O>::const_pointer const_pointer;
    typedef typename allocator_traits<O>::void_pointer void_pointer;
    typedef typename allocator_traits<O>::const_void_pointer const_void_pointer;
    typedef
    decltype(test_alloc_copy<O>()) propagate_on_container_copy_assigment;
    typedef
    decltype(test_alloc_move<O>()) propagate_on_container_move_assigment;
    typedef decltype(test_alloc_move<O>()) propagate_on_container_swap;

    template <typename T>
    struct rebind {
        typedef scoped_allocator_adaptor<
            typename allocator_traits<O>::template rebind_alloc<T>> other;
    };

    scoped_allocator_adaptor() = default;

    template <typename O2>
    scoped_allocator_adaptor(O2 &&o2) noexcept(noexcept(O(glglT::forward<O2>(o2)))):
        O(glglT::forward<O2>(o2)) {}

    scoped_allocator_adaptor(const scoped_allocator_adaptor &o) noexcept(
        noexcept(O(o))): O(o) {}

    scoped_allocator_adaptor(scoped_allocator_adaptor &&o) noexcept(
        noexcept(O(glglT::move(o)))): O(glglT::move(o)) {}

    template <typename O2>
    scoped_allocator_adaptor(const scoped_allocator_adaptor<O2> &o)
        noexcept(noexcept(O(o))): O(o) {}

    template <typename O2>
    scoped_allocator_adaptor(scoped_allocator_adaptor<O2> &&o)
        noexcept(noexcept(O(glglT::move(o)))): O(glglT::move(o)) {}

    O &outer_allocator() noexcept { return *this; }
    const O &outer_allocator() const noexcept { return *this; }

    inner_allocator_type &inner_allocator() noexcept { return *this; }
    const inner_allocator_type &inner_allocator() const noexcept
    {
        return *this;
    }

    pointer allocate(size_type n)
    {
        return allocator_traits<O>::allocate(*this, n);
    }

    pointer allocate(size_type n, const_void_pointer h)
    {
        return allocator_traits<O>::allocate(*this, n, h);
    }

    void deallocate(pointer p, size_type n) noexcept
    {
        allocator_traits<O>::deallocate(*this, p, n);
    }

    constexpr size_type max_size() const noexcept
    {
        return allocator_traits<O>::max_size(*this);
    }

    template <typename T, typename... Args>
    void construct(T *p, Args&&... args)
    {
        construct0_helper<
            uses_allocator<T, inner_allocator_type>::value,
            is_constructible<T, allocator_arg_t,
                             inner_allocator_type, Args...>::value,
            O, inner_allocator_type>::
            call(*this, *this, p, glglT::forward<Args>(args)...);
    }

    template <typename T1, typename T2, typename... Args1, typename... Args2>
    void construct(pair<T1, T2> *p, piecewise_construct_t,
                   tuple<Args1...> t1, tuple<Args2...> t2)
    {
        auto prime1 = construct1_helper<
            uses_allocator<T1, inner_allocator_type>::value,
            is_constructible<T1, allocator_arg_t,
                             inner_allocator_type, Args1...>::value,
            inner_allocator_type>::make_prime(*this, glglT::move(t1));
        auto prime2 = construct1_helper<
            uses_allocator<T2, inner_allocator_type>::value,
            is_constructible<T2, allocator_arg_t,
                             inner_allocator_type, Args2...>::value,
            inner_allocator_type>::make_prime(*this, glglT::move(t2));
        allocator_traits<O>::construct(*this, p, piecewise_construct,
                                       glglT::move(prime1), glglT::move(prime2));
    }

    template <typename T1, typename T2>
    void construct(pair<T1, T2> *p)
    {
        construct(p, piecewise_construct, tuple<>(), tuple<>());
    }

    template <typename T1, typename T2, typename O1, typename O2>
    void construct(pair<T1, T2> *p, O1 &&o1, O2 &&o2)
    {
        construct(p, piecewise_construct,
                  forward_as_tuple(glglT::forward<O1>(o1)),
                  forward_as_tuple(glglT::forward<O2>(o2)));
    }

    template <typename T1, typename T2, typename O1, typename O2>
    void construct(pair<T1, T2> *p, pair<O1, O2> &&op)
    {
        construct(p, piecewise_construct,
                  forward_as_tuple(glglT::forward<O1>(op.first)),
                  forward_as_tuple(glglT::forward<O2>(op.second)));
    }

    template <typename T>
    void destroy(T *p)
    {
        allocator_traits<O>::destroy(*this, p);
    }

    scoped_allocator_adaptor select_on_container_copy_construction() const
    {
        return scoped_allocator_adaptor(*this);
    }
};

template <typename O1, typename O2> inline
bool operator==(const scoped_allocator_adaptor<O1> &a1,
                const scoped_allocator_adaptor<O2> &a2) noexcept
{
    return a1.outer_allocator() == a2.outer_allocator();
}

template <typename O1, typename O2> inline
bool operator!=(const scoped_allocator_adaptor<O1> &a1,
                const scoped_allocator_adaptor<O2> &a2) noexcept
{
    return !(a1 == a2);
}

template <typename O1, typename O2, typename I0, typename... I> inline
bool operator==(const scoped_allocator_adaptor<O1, I0, I...> &a1,
                const scoped_allocator_adaptor<O2, I0, I...> &a2) noexcept
{
    return a1.outer_allocator() == a2.outer_allocator() &&
        a1.inner_allocator() == a2.inner_allocator();
}

template <typename O1, typename O2, typename I0, typename... I> inline
bool operator!=(const scoped_allocator_adaptor<O1, I0, I...> &a1,
                const scoped_allocator_adaptor<O2, I0, I...> &a2) noexcept
{
    return !(a1 == a2);
}

} // namespace glglT

#endif // GLGL_SCOPED_ALLOCATOR_H
