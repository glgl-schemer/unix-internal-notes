#ifndef GLGL_FUNCTIONAL_H
#define GLGL_FUNCTIONAL_H

#include "sub/glgl_function.h"
#include "sub/glgl_function_object.h"
#include "sub/glgl_literal_template.h"
#include "sub/glgl_mem_fn.h"
#include "sub/glgl_bind.h"

#endif // GLGL_FUNCTIONAL_H
