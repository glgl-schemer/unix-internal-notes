#ifndef GLGL_STRING_H
#define GLGL_STRING_H

#include <ios>
#include <string>
#include <iomanip>
#include <stdexcept>
#include "sub/glgl_allocator.h"
#include "sub/glgl_base_algo.h"
#include "sub/glgl_hash.h"
#include "sub/glgl_other_iterator.h"
#include "sub/glgl_unique_ptr.h"
#include "sub/glgl_shared_ptr.h"

namespace glglT {

template <typename, typename, typename> class basic_string;

template <typename C, typename T, typename A, typename I>
basic_string<C, T, A> all_to_bs(int (*)(C *, size_t, const C *, ...), const C *, I);

template <typename C, typename Traits = std::char_traits<C>,
          typename Alloc = allocator<C>>
class basic_string {
    template <typename O, typename T, typename A, typename I> friend
    basic_string<O, T, A>
    all_to_bs(int (*)(O *, size_t, const O *, ...), const O *, I);

    struct move_tag {};

    constexpr static int SS_LIM = 16;
public:
    typedef Traits traits_type;
    typedef typename Traits::char_type value_type;
    typedef Alloc allocator_type;
    typedef typename allocator_traits<Alloc>::size_type size_type;
    typedef typename allocator_traits<Alloc>::difference_type difference_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef normal_iterator<pointer> iterator;
    typedef normal_iterator<const_pointer> const_iterator;
    typedef glglT::reverse_iterator<pointer> reverse_iterator;
    typedef glglT::reverse_iterator<const_pointer> const_reverse_iterator;
    static const size_type npos = -1;
private:
    Alloc a;
    pointer first;
    pointer last;
    pointer tail;
public:
    basic_string() noexcept(noexcept(Alloc())):
        first(nullptr), last(nullptr), tail(nullptr) {}

    explicit basic_string(const Alloc &ac) noexcept(noexcept(Alloc(ac))):
        a(ac), first(nullptr), last(nullptr), tail(nullptr) {}

    basic_string(size_type n, C c, const Alloc &ac = Alloc()): a(ac)
    {
        this->init(n, n < SS_LIM ? SS_LIM : n, c);
    }

    basic_string(const basic_string &o, size_type pos,
                 size_type n = npos, const Alloc &ac = Alloc()): a(ac)
    {
        const auto o_sz = o.size();
        if (o_sz >= pos) {
            if (o_sz - pos < n)
                n = o_sz - pos;
            const auto lp = o.first + pos;
            this->init(n, n < SS_LIM ? SS_LIM : n, lp);
        } else
            throw std::out_of_range("basic_string out of range");
    }

    basic_string(const C *s, size_type n, const Alloc &ac = Alloc()): a(ac)
    {
        this->init(n, n < SS_LIM ? SS_LIM : n, s);
    }

    basic_string(const C *s, const Alloc &ac = Alloc()): a(ac)
    {
        const auto n = Traits::length(s);
        this->init(n, n < SS_LIM ? SS_LIM : n, s);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    basic_string(It b, It e, const Alloc &ac = Alloc()):
        basic_string(b, e, ac, typename iterator_traits<It>::iterator_category()) {}

    basic_string(const basic_string &o)
    {
        const auto lp = o.first;
        const auto n = o.size();
        this->init(n, n < SS_LIM ? SS_LIM : n, lp);
    }

    basic_string(const basic_string &o, const Alloc &ac): a(ac)
    {
        const auto lp = o.first;
        const auto n = o.size();
        this->init(n, n < SS_LIM ? SS_LIM : n, lp);
    }

    basic_string(basic_string &&o) noexcept(noexcept(Alloc())):
        first(o.first), last(o.last), tail(o.tail)
    {
        o.first = o.last = o.tail = nullptr;
    }

    basic_string(basic_string &&o, const Alloc &ac): a(ac),
        first(o.first), last(o.last), tail(o.tail)
    {
        if (a == o.a)
            o.first = o.last = o.tail = nullptr;
        else {
            const auto lp = o.first;
            const auto n = o.size();
            this->init(n, n < SS_LIM ? SS_LIM : n, lp, move_tag());
        }
    }

    basic_string(std::initializer_list<C> il, const Alloc &ac = Alloc()):
        basic_string(il.begin(), il.end(), ac) {}

    ~basic_string() { this->free(); }

    basic_string &operator=(const basic_string &o)
    {
        if (first == o.first)
            return *this;
        this->free();
        glglT::alloc_copy(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_copy_assignment());
        auto lp = o.first;
        const auto n = o.size();
        this->init(n, n < SS_LIM ? SS_LIM : n, lp);
        return *this;
    }

    basic_string &operator=(basic_string &&o)
    {
        if (first == o.first)
            return *this;
        this->free();
        glglT::alloc_move(a, glglT::move(o.a), typename allocator_traits<Alloc>::
                          propagate_on_container_move_assignment());
        if (a == o.a) {
            first = o.first;
            last = o.last;
            tail = o.tail;
            o.first = o.last = o.tail = nullptr;
        } else {
            auto lp = o.first;
            const auto n = o.size();
            this->init(n, n < SS_LIM ? SS_LIM : n, lp, move_tag());
        }
        return *this;
    }

    basic_string &operator=(const C *s)
    {
        this->free();
        const auto n = Traits::length(s);
        this->init(n, n < SS_LIM ? SS_LIM : n, s);
        return *this;
    }

    basic_string &operator=(C c)
    {
        return this->assign(1, glglT::move(c));
    }

    basic_string &operator=(std::initializer_list<C> il)
    {
        return this->assign(il.begin(), il.end());
    }

    basic_string &assign(size_type n, C c)
    {
        this->free();
        this->init(n, n < SS_LIM ? SS_LIM : n, c);
        return *this;
    }

    basic_string &assign(const basic_string &o)
    {
        return this->operator=(o);
    }

    basic_string &assign(const basic_string &o, size_type pos, size_type n)
    {
        const auto o_sz = o.size();
        if (first == o.first) {
            if (o_sz >= pos) {
                this->erase(0, pos);
                const auto sz = this->size();
                if (n < sz)
                    this->erase(n, sz - n);
            } else
                throw std::out_of_range("basic_string out of range");
            return *this;
        }
        this->free();
        if (o_sz > pos) {
            if (o_sz - pos < n)
                n = o_sz - pos;
            auto lp = o.first + pos;
            this->init(n, n < SS_LIM ? SS_LIM : n, lp);
        } else
            throw std::out_of_range("basic_string out of range");
        return *this;
    }

    basic_string &assign(basic_string &&o) noexcept
    {
        return this->operator=(glglT::move(o));
    }

    basic_string &assign(const C *s, size_type n)
    {
        if (first <= s && s <= last) {
            this->erase(0, s - first);
            if (n < size())
                this->erase(const_iterator(first + n));
            return *this;
        }
        this->free();
        this->init(n, n < SS_LIM ? SS_LIM : n, s);
        return *this;
    }

    basic_string &assign(const C *s)
    {
        return this->operator=(s);
    }

    basic_string &assign(iterator b, iterator e)
    {
        return this->assign(const_iterator(b), const_iterator(e));
    }

    basic_string &assign(const_iterator b, const_iterator e)
    {
        if (first <= b.base() && b.base() <= first + size()) {
            this->erase(e, const_iterator(last));
            this->erase(const_iterator(first), b);
            return *this;
        }
        this->free();
        const auto n = glglT::distance(b, e);
        this->init(n, n < SS_LIM ? SS_LIM : n, b);
        return *this;
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    basic_string &assign(It b, It e)
    {
        return this->assign(b, e, typename iterator_traits<It>::iterator_category());
    }

    basic_string &assign(std::initializer_list<C> il)
    {
        return this->operator=(il);
    }

    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    reference at(size_type pos)
    {
        if (pos < this->size()) {
            return *(first + pos);
        } else
            throw std::out_of_range("basic_string out of range");
    }

    const_reference at(size_type pos) const
    {
        if (pos < this->size()) {
            return *(first + pos);
        } else
            throw std::out_of_range("basic_string out of range");
    }

    reference operator[](size_type pos) noexcept { return first[pos]; }

    const_reference operator[](size_type pos) const noexcept
    {
        return first[pos];
    }

    reference front() noexcept { return *first; }
    const_reference front() const noexcept { return *first; }

    reference back() noexcept { return *(last - 1); }
    const_reference back() const noexcept { return *(last - 1); }

    const C *data() const noexcept { return first; }
    const C *c_str() const noexcept { return first; }

    iterator begin() noexcept { return iterator(first); }
    const_iterator begin() const noexcept { return const_iterator(first); }
    const_iterator cbegin() const noexcept { return const_iterator(first); }

    iterator end() noexcept { return iterator(last); }
    const_iterator end() const noexcept { return const_iterator(last); }
    const_iterator cend() const noexcept { return const_iterator(last); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(last); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(last); }
    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(last); }

    reverse_iterator rend() noexcept { return reverse_iterator(first); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(first); }
    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(first); }

    bool empty() const noexcept { return first == last; }
    size_type size() const noexcept { return last - first; }
    size_type length() const noexcept { return last - first; }
    size_type capacity() const noexcept { return tail - first; }
    constexpr size_type max_size() const noexcept
    {
        return allocator_traits<Alloc>::max_size(a);
    }

    void shrink_to_fit() { this->move_area(this->size()); }
    void reserve(size_type new_cap = 0)
    {
        if (this->size() < new_cap && new_cap != this->capacity()) {
            if (this->max_size() < new_cap)
                throw std::length_error("vector too large");
            this->move_area(new_cap);
        }
    }

    void clear() noexcept
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, first, size());
        last = first;
    }

    basic_string &insert(size_type pos, size_type n, C c)
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (this->max_size() - n < sz)
            throw std::length_error("basic_string too large");
        if (this->capacity() - n < sz)
            this->insr(n, sz, pos, c);
        else
            this->ins2(n, sz, pos, c);
        return *this;
    }

    basic_string &insert(size_type pos, const C *s)
    {
        return this->insert(pos, s, Traits::length(s));
    }

    basic_string &insert(size_type pos, const C *s, size_type n)
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (this->max_size() - n < sz)
            throw std::length_error("basic_string too large");
        if (this->capacity() - n < sz)
            this->insr(n, sz, pos, s);
        else
            this->ins2(n, sz, pos, s);
        return *this;
    }

    basic_string &insert(size_type pos, const basic_string &o)
    {
        return this->insert(pos, o.c_str(), o.size());
    }

    basic_string &insert(size_type pos, const basic_string &o,
                         size_type opos, size_type n)
    {
        const auto osz = o.size();
        if (osz < opos)
            throw std::out_of_range("basic_string out of range");
        if (n > osz - opos)
            n = osz - opos;
        return this->insert(pos, o.c_str() + opos, n);
    }

    iterator insert(const_iterator itr, C c)
    {
        return this->insert(glglT::move(itr), 1, glglT::move(c));
    }

    iterator insert(const_iterator itr, size_type n, C c)
    {
        const auto pos = itr.base() - first;
        this->insert(pos, n, c);
        return this->begin() + pos;
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    iterator insert(const_iterator itr, It b, It e)
    {
        return this->insert(itr, b, e, typename iterator_traits<It>::
                            iterator_category());
    }

    iterator insert(const_iterator itr, std::initializer_list<C> il)
    {
        return this->insert(itr, il.begin, il.end);
    }

    basic_string &erase(size_type pos, size_type n)
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (sz - pos < n)
            n = sz - pos;
        const auto fpos = first + pos;
        glglT::move(fpos + n, last, fpos);
        last -= n;
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, last, n);
        return *this;
    }

    iterator erase(const_iterator itr)
    {
        const auto it = const_cast<pointer>(itr.base());
        glglT::move(it + 1, last, it);
        allocator_traits<Alloc>::destroy(a, --last);
        return iterator(it);
    }

    iterator erase(const_iterator b, const_iterator e)
    {
        const auto fpos = const_cast<pointer>(b.base());
        const auto n = glglT::distance(b, e);
        glglT::move(fpos + n, last, fpos);
        last -= n;
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, last, n);
        return iterator(fpos);
    }

    void push_back(C c)
    {
        this->append(1, glglT::move(c));
    }

    void pop_back() noexcept
    {
        allocator_traits<Alloc>::destroy(a, --last);
    }

    basic_string &append(size_type n, C c)
    {
        const auto sz = this->size();
        if (sz + n > this->max_size())
            throw std::length_error("basic_string too large");
        if (sz + n > this->capacity())
            this->appe(n, sz, c);
        else
            this->app2(n, sz, c);
        return *this;
    }

    basic_string &append(const basic_string &o)
    {
        return this->append(o, 0, o.size());
    }

    basic_string &append(const basic_string &o, size_type pos, size_type n)
    {
        const auto sz = o.size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (sz - pos < n)
            n = sz - pos;
        return this->append(o.c_str() + pos, n);
    }

    basic_string &append(const C *s, size_type n)
    {
        const auto sz = this->size();
        if (sz + n > this->max_size())
            throw std::length_error("basic_string too large");
        if (sz + n > this->capacity())
            this->appe(n, sz, s);
        else
            this->app2(n, sz, s);
        return *this;
    }

    basic_string &append(const C *s)
    {
        return this->append(s, Traits::length(s));
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    basic_string &append(It b, It e)
    {
        return this->append(b, e, typename iterator_traits<It>::iterator_category());
    }

    basic_string &append(std::initializer_list<C> il)
    {
        return this->append(il.begin(), il.end());
    }

    basic_string &operator+=(const basic_string &o) { return this->append(o); }
    basic_string &operator+=(const C *s) { return this->append(s); }
    basic_string &operator+=(std::initializer_list<C> il) { return this->append(il); }
    basic_string &operator+=(C c) { return this->append(1, glglT::move(c)); }

    int compare(const basic_string &o) const noexcept
    {
        return this->compare(0, this->size(), o);
    }

    int compare(size_type pos, size_type n, const basic_string &o) const
    {
        return this->compare(pos, n, o, 0, o.size());
    }

    int compare(size_type pos1, size_type n1, const basic_string &o,
                size_type pos2, size_type n2) const
    {
        const auto sz2 = o.size();
        if (sz2 < pos2)
            throw std::out_of_range("basic_string out of range");
        if (sz2 - pos2 < n2)
            n2 = sz2 - pos2;
        return this->compare(pos1, n1, o.c_str() + pos2, n2);
    }

    int compare(const C *s) const noexcept
    {
        return this->compare(0, this->size(), s);
    }

    int compare(size_type pos, size_type n, const C *s) const
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (sz - pos < n)
            n = sz - pos;
        return first ? Traits::compare(first + pos, s, n) : (s && *s) ? -1 : 0;
    }

    int compare(size_type pos, size_type n1, const C *s, size_type n2) const
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (sz - pos < n1)
            n1 = sz - pos;
        if (n1 == 0)
            return n2 ? -1 : 0;
        if (n2 == 0)
            return 1;
        const auto b = n1 < n2, e = n1 == n2;
        const auto n = b ? n1 : n2;
        const int i = Traits::compare(first + pos, s, n);
        return i | e ? i : b ? -1 : 1;
    }

    basic_string &replace(size_type pos, size_type n, const basic_string &o)
    {
        this->erase(pos, n);
        this->insert(pos, o);
        return *this;
    }

    basic_string &replace(const_iterator b, const_iterator e,
                          const basic_string &o)
    {
        this->erase(b, e);
        this->insert(b.base() - first, o);
        return *this;
    }

    basic_string &replace(size_type pos1, size_type n1, const basic_string &o,
                          size_type pos2, size_type n2)
    {
        this->erase(pos1, n1);
        this->insert(pos1, o, pos2, n2);
        return *this;
    }

    template <typename It>
    basic_string &replace(const_iterator b, const_iterator e, It f, It l)
    {
        this->erase(b, e);
        this->insert(b, f, l);
        return *this;
    }

    basic_string &replace(size_type pos, size_type n, const C *s, size_type sn)
    {
        this->erase(pos, n);
        this->insert(pos, s, sn);
        return *this;
    }

    basic_string &replace(const_iterator b, const_iterator e,
                          const C *s, size_type sn)
    {
        this->erase(b, e);
        this->insert(b.base() - first, s, sn);
        return *this;
    }

    basic_string &replace(size_type pos, size_type n, const C *s)
    {
        const auto sn = Traits::length(s);
        this->erase(pos, n);
        this->insert(pos, s, sn);
        return *this;
    }

    basic_string &replace(const_iterator b, const_iterator e, const C *s)
    {
        const auto sn = Traits::length(s);
        this->erase(b, e);
        this->insert(b.base() - first, s, sn);
        return *this;
    }

    basic_string &replace(size_type pos, size_type n, size_type cn, C c)
    {
        this->erase(pos, n);
        this->insert(pos, cn, c);
        return *this;
    }

    basic_string &replace(const_iterator b, const_iterator e, size_type cn, C c)
    {
        this->erase(b, e);
        this->insert(b, cn, c);
        return *this;
    }

    basic_string &replace(const_iterator b, const_iterator e,
                          std::initializer_list<C> il)
    {
        this->erase(b, e);
        this->insert(b, il);
        return *this;
    }

    basic_string substr(size_type pos = 0, size_type n = npos) const
    {
        return basic_string(*this, pos, n);
    }

    size_type copy(C *s, size_type n, size_type pos = 0) const
    {
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        if (sz - pos < n)
            n = sz - pos;
        glglT::copy_n(first, n, s);
        return n;
    }

    void resize(size_type n, C c = C())
    {
        if (n > max_size())
            throw std::length_error("basic_string too large");
        const auto sz = this->size();
        if (n < sz)
            this->erase(n, sz - n);
        else if (n != sz)
            this->append(n, c);
    }

    void swap(basic_string &o)
    {
        glglT::alloc_swap(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_swap());
        glglT::swap(first, o.first);
        glglT::swap(last, o.last);
        glglT::swap(tail, o.tail);
    }

    size_type find(const basic_string &o, size_type pos = 0) const noexcept
    {
        return this->find(o.c_str(), pos, o.size());
    }

    size_type find(const C *s, size_type pos, size_type n) const noexcept
    {
        if (this->size() < pos)
            return npos;
        auto p = glglT::search(first + pos, last, s, s + n,
                               [](const C &a, const C &b) {
                                 return Traits::eq(a, b);
                               });
        return p == last ? npos : p - first;
    }

    size_type find(const C *s, size_type pos = 0) const noexcept
    {
        return this->find(s, pos, Traits::length(s));
    }

    size_type find(const C &c, size_type pos = 0) const noexcept
    {
        if (this->size() < pos)
            return npos;
        auto p = glglT::find_if(first + pos, last,
                                [&c](const C &o){ return Traits::eq(c, o); });
        return p == last ? npos : p - first;
    }

    size_type rfind(const basic_string &o, size_type pos = npos) const noexcept
    {
        return this->rfind(o.c_str(), pos, o.size());
    }

    size_type rfind(const C *s, size_type pos, size_type n) const noexcept
    {
        if (this->size() < pos)
            return npos;
        auto p = glglT::find_end(first + pos, last, s, s + n,
                                 [](const C &a, const C &b) {
                                     return Traits::eq(a, b);
                                 });
        return p == last ? npos : p - first;
    }

    size_type rfind(const C *s, size_type pos = npos) const noexcept
    {
        return this->rfind(s, pos, Traits::length(s));
    }

    size_type rfind(const C &c, size_type pos = npos) const noexcept
    {
        const auto sz = this->size();
        if (sz < pos)
            pos = sz;
        for (auto l = last; l-- != first;)
            if (Traits::eq(*l, c))
                return l - first;
        return npos;
    }

    size_type find_first_of(const basic_string &o, size_type pos = 0)
        const noexcept
    {
        return this->find_first_of(o.c_str(), pos, o.size());
    }

    size_type find_first_of(const C *s, size_type pos, size_type n)
        const noexcept
    {
        const auto sz = this->size();
        if (sz <= pos)
            return npos;
        do {
            if (Traits::find(s, n, first[pos]))
                return pos;
        } while (++pos != sz);
        return npos;
    }

    size_type find_first_of(const C *s, size_type pos = 0) const noexcept
    {
        return this->find_first_of(s, pos, Traits::length(s));
    }

    size_type find_first_of(const C &c, size_type pos = 0) const noexcept
    {
        return this->find(c, pos);
    }

    size_type find_first_not_of(const basic_string &o, size_type pos = 0)
        const noexcept
    {
        return this->find_first_not_of(o.c_str(), pos, o.size());
    }

    size_type find_first_not_of(const C *s, size_type pos, size_type n)
        const noexcept
    {
        const auto sz = this->size();
        if (sz <= pos)
            return npos;
        do {
            if (!Traits::find(s, n, first[pos]))
                return pos;
        } while (++pos != sz);
        return npos;
    }

    size_type find_first_not_of(const C *s, size_type pos = 0) const noexcept
    {
        return this->find_first_not_of(s, pos, Traits::length(s));
    }

    size_type find_first_not_of(const C &c, size_type pos = 0) const noexcept
    {
        if (this->size() <= pos)
            return npos;
        auto p = glglT::find_if(first + pos, last,
                                [&c](const C &o){ return !Traits::eq(c, o); });
        return p == last ? npos : p - first;
    }

    size_type find_last_of(const basic_string &o, size_type pos = npos)
        const noexcept
    {
        return this->find_last_of(o.c_str(), pos, o.size());
    }

    size_type find_last_of(const C *s, size_type pos, size_type n)
        const noexcept
    {
        const auto sz = this->size();
        if (sz < pos)
            pos = sz;
        if (n >= pos)
            return npos;
        pos -= n;
        do {
            if (Traits::find(s, n, first[--pos]))
                return pos;
        } while (pos != 0);
        return npos;
    }

    size_type find_last_of(const C *s, size_type pos = npos)
        const noexcept
    {
        return this->find_last_of(s, pos, Traits::length(s));
    }

    size_type find_last_of(const C &c, size_type pos = 0) const noexcept
    {
        return this->rfind(c, pos);
    }

    size_type find_last_not_of(const basic_string &o, size_type pos = npos)
        const noexcept
    {
        return this->find_last_not_of(o.c_str(), pos, o.size());
    }

    size_type find_last_not_of(const C *s, size_type pos, size_type n)
        const noexcept
    {
        const auto sz = this->size();
        if (sz < pos)
            pos = sz;
        if (n >= pos)
            return npos;
        pos -= n;
        do {
            if (!Traits::find(s, n, first[--pos]))
                return pos;
        } while (pos != 0);
        return npos;
    }

    size_type find_last_not_of(const C *s, size_type pos = npos)
        const noexcept
    {
        return this->find_last_not_of(s, pos, Traits::length(s));
    }

    size_type find_last_not_of(const C &c, size_type pos = npos) const noexcept
    {
        const auto sz = this->size();
        if (sz < pos)
            pos = sz;
        if (pos == 0)
            return npos;
        do {
            if (!Traits::eq(c, first[--pos]))
                return pos;
        } while (pos != 0);
        return npos;
    }
private:
    basic_string(Alloc &&ac, pointer f, pointer l, pointer t): a(glglT::move(ac)),
        first(glglT::move(f)), last(glglT::move(l)), tail(glglT::move(t)) {}

    void free() noexcept
    {
        if (first != nullptr) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first, last - first);
            allocator_traits<Alloc>::deallocate
                (a, first, glglT::alloc_de_size(a, first, tail));
        }
    }

    void move_area(size_type new_cap)
    {
        const auto of = first;
        try {
            first = allocator_traits<Alloc>::allocate(a, new_cap);
        } catch(...) {
            first = of;
            throw;
        }
        const auto sz = this->size();
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of, sz, first);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, new_cap);
            first = of;
            throw;
        }
        allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
        last = first + sz;
        tail = first + new_cap;
    }

    template <typename It>
    basic_string(It b, It e, const Alloc &ac, input_iterator_tag): a(ac)
    {
        if (b != e) {
            this->init(1, SS_LIM, *b);
            for (++b; b != e; ++b)
                this->push_back(*b);
        } else
            first = last = tail = nullptr;
    }

    template <typename It>
    basic_string(It b, It e, const Alloc &ac, forward_iterator_tag): a(ac)
    {
        const auto n = glglT::distance(b, e);
        this->init(n, n < SS_LIM ? SS_LIM : n, b);
    }

    template <typename It>
    basic_string &assign(It b, It e, forward_iterator_tag)
    {
        this->free();
        const auto n = glglT::distance(b, e);
        this->init(n, n < SS_LIM ? SS_LIM : n, b);
        return *this;
    }

    template <typename It>
    basic_string &assign(It b, It e, input_iterator_tag)
    {
        this->free();
        if (b != e) {
            this->init(1, SS_LIM, *b);
            for (++b; b != e; ++b)
                this->push_back(*b);
        }
        return *this;
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, input_iterator_tag)
    {
        const auto pos = itr.base() - first;
        for (; b != e; ++b, ++itr)
            this->insert(itr, 1, *b);
        return iterator(first + pos);
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, forward_iterator_tag)
    {
        const auto pos = itr.base() - first;
        const auto sz = this->size();
        if (sz < pos)
            throw std::out_of_range("basic_string out of range");
        const auto n = glglT::distance(b, e);
        if (this->max_size() - n < sz)
            throw std::length_error("basic_string too large");
        if (this->capacity() - n < sz)
            this->insr(n, sz, pos, b);
        else
            this->ins2(n, sz, pos, b);
        return iterator(first + pos);
    }

    template <typename It>
    basic_string &append(It b, It e, input_iterator_tag)
    {
        for (; b != e; ++b)
            this->push_back(*b);
        return *this;
    }

    template <typename It>
    basic_string &append(It b, It e, forward_iterator_tag)
    {
        const auto sz = this->size();
        const auto n = glglT::distance(b, e);
        if (sz > this->max_size() - n)
            throw std::length_error("basic_string too large");
        if (sz > this->capacity() - n)
            this->appe(n, sz, b);
        else
            this->app2(n, sz, b);
        return *this;
    }

    template <typename MoveTAG>
    void inner_new_place_construct(size_type n, pointer where, pointer b, MoveTAG&&)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_move_n(a, b, n, where);
    }

    template <typename It>
    void inner_new_place_construct(size_type n, pointer where, It b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, b, n, where);
    }

    void inner_new_place_construct(size_type n, pointer where, C c)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_fill_n(a, where, n, c);
    }

    template <typename ItorC, typename... MoveTAG>
    void init(size_type n, size_type sz, ItorC b, MoveTAG&&... may_have)
    {
        try {
            first = allocator_traits<Alloc>::allocate(a, sz);
        } catch(...) {
            first = last = tail = nullptr;
            throw;
        }
        try {
            this->inner_new_place_construct(n, first, b,
                                            glglT::forward<MoveTAG>(may_have)...);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, sz);
            first = last = tail = nullptr;
            throw;
        }
        assert(n != 16);
        last = first + n;
        tail = first + sz;
    }

    template <typename ItorC>
    void insr(size_type n, size_type sz, size_type pos, ItorC b)
    {
        const auto cap = sz == 0 ? (SS_LIM > n ? SS_LIM : n * 2)
                                 :  sz < n ? sz + n : sz * 2;
        const auto of = first;
        try {
            first = allocator_traits<Alloc>::allocate(a, cap);
        } catch(...) {
            first = of;
            throw;
        }
        try {
            this->inner_new_place_construct(n, first + pos, b);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of + pos, sz - pos, first + n + pos);
        } catch(...) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first + pos, n);
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of, pos, first);
        } catch(...) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first + pos, n);
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy_move(a, first + n + pos, of + pos, sz - pos);
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = of;
            throw;
        }
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::destroy(a, of, sz);
        allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
        last = first + sz + n;
        tail = first + cap;
    }

    template <typename It>
    void inner_ins2(size_type n, pointer where, It b)
    {
        glglT::copy_n(b, n, where);
    }

    void inner_ins2(size_type n, pointer where, C c)
    {
        glglT::fill_n(where, n, c);
    }

    template <typename ItorC>
    void ins2(size_type n, size_type sz, size_type pos, ItorC b)
    {
        const auto beflast = last - n;
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, beflast, n, last);
        const auto fpos = first + pos;
        if (fpos < beflast)
            try {
                glglT::move_backward(fpos, beflast, last);
            } catch(...) {
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    destroy(a, last, n);
                throw;
            }
        try {
            this->inner_ins2(n, fpos, b);
        } catch(...) {
            glglT::move(fpos + n, last + n, fpos);
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, last, n);
            throw;
        }
        last += n;
    }

    template <typename ItorC>
    void appe(size_type n, size_type sz, ItorC b)
    {
        const auto cap = sz == 0 ? (SS_LIM > n ? SS_LIM : n * 2)
                                 :  sz < n ? sz + n : sz * 2;
        const auto of = first;
        try {
            first = allocator_traits<Alloc>::allocate(a, cap);
        } catch(...) {
            first = of;
            throw;
        }
        try {
            this->inner_new_place_construct(n, first + sz, b);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of, sz, first);
        } catch(...) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first + sz, n);
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = of;
            throw;
        }
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::destroy(a, of, sz);
        allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
        last = first + sz + n;
        tail = first + cap;
    }

    template <typename It>
    void app2(size_type n, size_type sz, It b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, b, n, first + sz);
        last += n;
    }

    void app2(size_type n, size_type sz, C c)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_fill_n(a, first + sz, n, c);
        last += n;
    }
};

using string = basic_string<char>;
using wstring = basic_string<wchar_t>;
using u16string = basic_string<char16_t>;
using u32string = basic_string<char32_t>;

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const basic_string<C, T, A> &s1,
                                const basic_string<C, T, A> &s2)
{
    basic_string<C, T, A> res(s1.size() + s2.size() + 1, C());
    res.append(s1.begin(), s1.end());
    res.append(s2.begin(), s2.end());
    return res;
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const C *s1, const basic_string<C, T, A> &s2)
{
    const auto n = T::length(s1);
    basic_string<C, T, A> res(n + s2.size() + 1, C());
    res.append(s1, n);
    res.append(s2.begin(), s2.end());
    return res;
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(C c, const basic_string<C, T, A> &s2)
{
    basic_string<C, T, A> res(s2.size() + 2, C());
    res.push_back(glglT::move(c));
    res.append(s2.begin(), s2.end());
    return res;
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const basic_string<C, T, A> &s1, const C *s2)
{
    const auto n = T::length(s2);
    basic_string<C, T, A> res(n + s1.size() + 1, C());
    res.append(s1.begin(), s1.end());
    res.append(s2, n);
    return res;
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const basic_string<C, T, A> &s1, C c)
{
    basic_string<C, T, A> res(s1.size() + 2, C());
    res.append(s1.begin(), s1.end());
    res.push_back(glglT::move(c));
    return res;
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(basic_string<C, T, A> &&s1,
                                basic_string<C, T, A> &&s2)
{
    const auto sz = s1.size() + s2.size();
    const bool choose_s1 = (sz <= s1.capacity() || sz > s2.capacity());
    return choose_s1 ?
        glglT::move(s1.append(s2)) : glglT::move(s2.insert(0, s1));
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const C *s1, basic_string<C, T, A> &&s2)
{
    return glglT::move(s2.insert(0, s1));
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(C c, basic_string<C, T, A> &&s2)
{
    return glglT::move(s2.insert(0, 1, c));
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(basic_string<C, T, A> &&s1, const C *s2)
{
    return glglT::move(s1.append(s2));
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(basic_string<C, T, A> &&s1, C c)
{
    return glglT::move(s1 += c);
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(const basic_string<C, T, A> &s1,
                                basic_string<C, T, A> &&s2)
{
    return glglT::move(s2.insert(0, s1));
}

template <typename C, typename T, typename A> inline
basic_string<C, T, A> operator+(basic_string<C, T, A> &&s1,
                                const basic_string<C, T, A> &s2)
{
    return glglT::move(s1.append(s2));
}

template <typename C, typename T, typename A> inline bool
operator==(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return s1.compare(s2) == 0;
}

template <typename C, typename T, typename A> inline bool
operator!=(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return !(s1 == s2);
}

template <typename C, typename T, typename A> inline bool
operator<(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return s1.compare(s2) < 0;
}

template <typename C, typename T, typename A> inline bool
operator>=(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return !(s1 < s2);
}

template <typename C, typename T, typename A> inline bool
operator>(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return s2 < s1;
}

template <typename C, typename T, typename A> inline bool
operator<=(const basic_string<C, T, A> &s1, const basic_string<C, T, A> &s2)
    noexcept
{
    return !(s2 < s1);
}

template <typename C, typename T, typename A> inline
bool operator==(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return s1.compare(s2) == 0;
}

template <typename C, typename T, typename A> inline
bool operator==(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return s2.compare(s1) == 0;
}

template <typename C, typename T, typename A> inline
bool operator!=(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return !(s1 == s2);
}

template <typename C, typename T, typename A> inline
bool operator!=(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return !(s1 == s2);
}

template <typename C, typename T, typename A> inline
bool operator<(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return s1.compare(s2) < 0;
}

template <typename C, typename T, typename A> inline
bool operator<(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return s2.compare(s1) > 0;
}

template <typename C, typename T, typename A> inline
bool operator>=(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return !(s1 < s2);
}

template <typename C, typename T, typename A> inline
bool operator>=(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return !(s1 < s2);
}

template <typename C, typename T, typename A> inline
bool operator>(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return s2 < s1;
}

template <typename C, typename T, typename A> inline
bool operator>(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return s2 < s1;
}

template <typename C, typename T, typename A> inline
bool operator<=(const basic_string<C, T, A> &s1, const C *s2) noexcept
{
    return !(s2 < s1);
}

template <typename C, typename T, typename A> inline
bool operator<=(const C *s1, const basic_string<C, T, A> &s2) noexcept
{
    return !(s2 < s1);
}

template <typename C, typename T, typename A> inline
void swap(basic_string<C, T, A> &s1, basic_string<C, T, A> &s2)
{
    s1.swap(s2);
}

template <typename C, typename T, typename A, typename I> inline
basic_string<C, T, A>
all_to_bs(int (* cov)(C *, size_t, const C *, ...), const C *s, I i)
{
    auto len = i != 0
        ? static_cast<typename make_signed<size_t>::type>
            (std::log10(i < 0 ? -i : i))
        : 0;
    if (len < 0)
        len = -len;
    A a;
    auto buf = allocator_traits<A>::allocate(a, len + 10);
    uninitialized_with_alloc<A, allocator_traits<A>>::
        uninitialized_fill_n(a, buf, len + 10, C());
    const auto &n = cov(buf, len + 10, s, i);
    uninitialized_with_alloc<A, allocator_traits<A>>::
        destroy(a, buf + n, len + 10 - n);
    return basic_string<C, T, A>(glglT::move(a), buf, buf + n, buf + len + 10);
}

inline string to_string(int value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%d", value);
}

inline string to_string(long value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%ld", value);
}

inline string to_string(long long value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%lld", value);
}

inline string to_string(unsigned value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%u", value);
}

inline string to_string(unsigned long value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%lu", value);
}

inline string to_string(unsigned long long value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%llu", value);
}

inline string to_string(float value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%f", value);
}

inline string to_string(double value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%f", value);
}

inline string to_string(long double value)
{
    typedef std::char_traits<char> T;
    return glglT::all_to_bs<char, T, allocator<char>>(std::snprintf, "%Lf", value);
}

inline wstring to_wstring(int value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%d", value);
}

inline wstring to_wstring(long value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%ld", value);
}

inline wstring to_wstring(long long value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%lld", value);
}

inline wstring to_wstring(unsigned value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%u", value);
}

inline wstring to_wstring(unsigned long value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%lu", value);
}

inline wstring to_wstring(unsigned long long value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%llu", value);
}

inline wstring to_wstring(float value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%f", value);
}

inline wstring to_wstring(double value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%f", value);
}

inline wstring to_wstring(long double value)
{
    typedef std::char_traits<wchar_t> T;
    typedef allocator<wchar_t> A;
    return glglT::all_to_bs<wchar_t, T, A>(std::swprintf, L"%Lf", value);
}

template <> struct hash<string>: function_type<size_t(string)>, slow_hash_base {
    size_t operator()(const string &s) const noexcept
    {
        return FNVhash<char>::_1a(s.c_str(), s.size());
    }
};

template <> struct hash<wstring>: function_type<size_t(wstring)>, slow_hash_base {
    size_t operator()(const wstring &s) const noexcept
    {
        return FNVhash<wchar_t>::_1a(s.c_str(), s.size());
    }
};

template <> struct hash<u16string>: function_type<size_t(u16string)>, slow_hash_base {
    size_t operator()(const u16string &s) const noexcept
    {
        return FNVhash<char16_t>::_1a(s.c_str(), s.size());
    }
};

template <> struct hash<u32string>: function_type<size_t(u32string)>, slow_hash_base {
    size_t operator()(const u32string &s) const noexcept
    {
        return FNVhash<char32_t>::_1a(s.c_str(), s.size());
    }
};

// io start

template <typename C, typename Traits = std::char_traits<C>,
          typename Alloc = allocator<C>>
class basic_stringbuf: public std::basic_streambuf<C, Traits> {
    typename std::ios_base::openmode mode;
    basic_string<C, Traits, Alloc> strbuf;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef typename Traits::int_type int_type;
    typedef typename Traits::pos_type pos_type;
    typedef typename Traits::off_type off_type;
    typedef Alloc allocator_type;

    explicit
    basic_stringbuf(typename std::ios_base::openmode w =
                    std::ios_base::in | std::ios_base::out):
        std::basic_streambuf<C, Traits>(), mode(w) {}
    explicit
    basic_stringbuf(const basic_string<C, Traits, Alloc> &str,
                    typename std::ios_base::openmode w =
                    std::ios_base::in | std::ios_base::out):
        basic_stringbuf(basic_string<C, Traits, Alloc>(str), w) {}
    explicit
    basic_stringbuf(basic_string<C, Traits, Alloc> &&str,
                    typename std::ios_base::openmode w =
                    std::ios_base::in | std::ios_base::out):
        std::basic_streambuf<C, Traits>(), mode(w), strbuf(glglT::move(str))
    {
        if (w & std::ios_base::in) {
            if (w & std::ios_base::ate)
                this->setg(strbuf.begin().base(),
                           strbuf.end().base(),
                           strbuf.end().base());
            else
                this->setg(strbuf.begin().base(),
                           strbuf.begin().base(),
                           strbuf.end().base());
        }
        if (w & std::ios_base::out) {
            this->setp(strbuf.begin().base(),
                       strbuf.begin().base() + strbuf.capacity());
            if ((w & std::ios_base::ate) || (w & std::ios_base::app))
                this->pbump(strbuf.size());
        }
    }

    basic_stringbuf(const basic_stringbuf &) = delete;
    basic_stringbuf(basic_stringbuf &&sbuf) noexcept:
        std::basic_streambuf<C, Traits>(glglT::move(sbuf)),
        strbuf(glglT::move(sbuf.strbuf)), mode(glglT::move(sbuf.mode)) {}

    basic_stringbuf &operator=(const basic_stringbuf &) = delete;
    basic_stringbuf &operator=(basic_stringbuf &&sbuf) noexcept
    {
        std::basic_streambuf<C, Traits>::operator=(glglT::move(sbuf));
        strbuf = glglT::move(sbuf.strbuf);
        mode = glglT::move(sbuf.mode);
        return *this;
    }

    void swap(basic_stringbuf &sbuf)
    {
        std::basic_streambuf<C, Traits>::swap(sbuf);
        strbuf.swap(sbuf.strbuf);
        std::swap(mode, sbuf.mode);
    }

    basic_string<C, Traits, Alloc> str() const
    {
        if (mode & std::ios_base::out)
            return basic_string<C, Traits, Alloc>(this->pbase(), this->pptr());
        return basic_string<C, Traits, Alloc>(this->eback(), this->egptr());
    }

    void str(const basic_string<C, Traits, Alloc> &str)
    {
        strbuf = str;
        if (mode & std::ios_base::in)
            this->setg(strbuf.begin().base(),
                       strbuf.begin().base(),
                       strbuf.end().base());
        if (mode & std::ios_base::out) {
            this->setp(strbuf.begin().base(),
                       strbuf.begin().base() + strbuf.capacity());
            if (mode & std::ios_base::ate)
                this->pbump(strbuf.size());
        }
    }
protected:
    virtual int_type underflow()
    {
        if (this->egptr() > this->gptr())
            return Traits::to_int_type(*(this->gptr()));
        if (this->pptr() > this->egptr()) {
            this->setg(this->eback(), this->gptr(), this->pptr());
            return Traits::to_int_type(*(this->gptr()));
        }
        return Traits::eof();
    }

    virtual int_type pbackfail(int_type c = Traits::eof())
    {
        if (this->eback() == this->gptr())
            return Traits::eof();
        if (Traits::eq_int_type(c, Traits::eof()) ||
            Traits::eq(Traits::to_char_type(c), this->gptr()[-1])) {
            this->gbump(-1);
            return Traits::not_eof(c);
        }
        if (!(mode & std::ios_base::out))
            return Traits::eof();
        this->gbump(-1);
        *(this->gptr()) = Traits::to_char_type(c);
        return c;
    }

    virtual int_type overflow(int_type c = Traits::eof())
    {
        if (Traits::eq_int_type(c, Traits::eof()))
            return Traits::not_eof(c);
        if (!(mode & std::ios_base::out))
            return Traits::eof();
        if (this->pptr() == this->epptr()) {
            if (strbuf.max_size() == strbuf.capacity())
                return Traits::eof();
            const bool also_in = mode & std::ios_base::in;
            decltype(this->gptr() - this->eback()) gd1;
            if (also_in)
                gd1 = this->gptr() - this->eback();
            const auto sz = strbuf.size();
            strbuf.push_back(Traits::to_char_type(c));
            this->setp(strbuf.begin().base(),
                       strbuf.begin().base() + strbuf.capacity());
            this->pbump(sz);
            if (also_in)
                this->setg(strbuf.begin().base(),
                           strbuf.begin().base() + gd1,
                           strbuf.end().base());
        } else
            strbuf.push_back(Traits::to_char_type(c));
        this->pbump(1);
        return c;
    }

    virtual std::basic_streambuf<C, Traits> *setbuf(C *, std::streamsize)
    {
        return this;
    }

    virtual pos_type seekoff(off_type off, std::ios_base::seekdir dir,
                             std::ios_base::openmode w =
                             std::ios_base::in | std::ios_base::out)
    {
        if (strbuf.size() == 0 && off != 0)
            return pos_type(off_type(-1));
        off_type newoffi, newoffo;
        const bool also_in = w & std::ios_base::in;
        const bool also_out = w & std::ios_base::out;
        if (dir == std::ios_base::beg)
            newoffi = newoffo = 0;
        else if (dir == std::ios_base::cur) {
            if (also_in) {
                if (also_out)
                    return pos_type(off_type(-1));
                else
                    newoffi = this->gptr() - this->eback();
            } else if (also_out)
                newoffo = this->pptr() - this->pbase();
        } else if (dir == std::ios_base::end)
            newoffo = newoffi = strbuf.size();
        if (also_in) {
            const auto newoff = newoffi + off;
            if (newoff < 0 || newoff > strbuf.size())
                return pos_type(off_type(-1));
            this->setg(this->eback(), this->eback() + newoff, this->egptr());
            if (!also_out)
                return pos_type(newoff);
        }
        if (also_out) {
            const auto newoff = newoffo + off;
            if (newoff < 0 || newoff > strbuf.size())
                return pos_type(off_type(-1));
            this->setp(this->pbase(), this->pbase() + strbuf.capacity());
            this->pbump(newoff);
            return pos_type(newoff);
        }
        return pos_type(off_type(-1));
    }

    virtual pos_type seekpos(pos_type sp, std::ios_base::openmode w =
                             std::ios_base::in | std::ios_base::out)
    {
        return this->seekoff(off_type(sp), std::ios_base::beg, w);
    }
};

template <typename C, typename T, typename A> inline
void swap(basic_stringbuf<C, T, A> &s1, basic_stringbuf<C, T, A> &s2)
{
    s1.swap(s2);
}

typedef basic_stringbuf<char> stringbuf;
typedef basic_stringbuf<wchar_t> wstringbuf;

template <typename C, typename T, typename A> std::basic_ostream<C, T> &
operator<<(std::basic_ostream<C, T> &os, const basic_string<C, T, A> &s)
{
    typedef typename basic_string<C, T, A>::size_type size_type;
    const size_type w = os.width(0);
    const size_type sz = s.size();
    if (sz < w) {
        unique_ptr<C[]> buf(new C[w]);
        const auto fill_left = (os.flags() & std::ios_base::adjustfield) == std::ios_base::left;
        const C fill_char = os.fill();
        if (!fill_left)
            glglT::fill_n(buf.get(), w - sz, fill_char);
        glglT::copy_n(s.c_str(), sz, buf.get() + (!fill_left ? w - sz : 0));
        if (fill_left)
            glglT::fill_n(buf.get() + sz, w - sz, fill_char);
        return os.write(buf.get(), w);
    } else
        return os.write(s.c_str(), s.size());
}

template <typename C, typename T, typename A> std::basic_istream<C, T> &
operator>>(std::basic_istream<C, T> &is, basic_string<C, T, A> &s)
{
    typedef typename basic_string<C, T, A>::size_type size_type;
    typename T::int_type c = typename T::int_type();
    const size_type w = is.width(0);
    size_type n = w == 0 ? s.max_size() : w;
    s.resize(0);
    try {
        if (!std::basic_istream<C, T>::sentry(is, true))
            return is;
    } catch (...) {
        return is;
    }
    try {
        for (c = is.rdbuf()->sgetc(); n--; c = is.rdbuf()->snextc()) {
            if (T::eq_int_type(c, T::eof())) {
                is.setstate(std::ios_base::eofbit);
                break;
            }
            if (std::isspace(T::to_char_type(c), is.getloc()))
                break;
            s.push_back(T::to_char_type(c));
        }
    } catch (...) {
        is.setstate(std::ios_base::badbit);
    }
    if (s.empty())
        is.setstate(std::ios_base::failbit);
    return is;
}

template <typename C, typename T, typename A> std::basic_istream<C, T> &
getline(std::basic_istream<C, T> &is, basic_string<C, T, A> &s, C d)
{
    basic_stringbuf<C, T, A> buf(std::ios_base::out);
    is.get(buf, d);
    s = buf.str();
    return is;
}

template <typename C, typename T, typename A> std::basic_istream<C, T> &
getline(std::basic_istream<C, T> &is, basic_string<C, T, A> &s)
{
    return glglT::getline(is, s, is.widen('\n'));
}

// io end

template <typename I, typename C, typename T, typename A, typename... B> inline
I bs_to_all(const basic_string<C, T, A> &s, size_t *pos, B... b)
{
    I i;
    auto buf = basic_stringbuf<C, T, A>(s, std::ios_base::in);
    std::basic_istream<C, T> is(&buf);
    struct setbase {
        std::basic_istream<C, T> &operator()(std::basic_istream<C, T> &is)
            const noexcept
        {
            return is;
        }

        std::basic_istream<C, T> &operator()(std::basic_istream<C, T> &is, int base)
            const noexcept(noexcept(is >> std::setbase(base)))
        {
            return is >> std::setbase(base);
        }
    };
    setbase()(is, b...) >> i;
    const auto g = is.gcount();
    if (g == 0)
        throw std::invalid_argument("no conversion in basic_string");
    if (is.rdstate() & std::ios_base::failbit)
        throw std::out_of_range("number too large");
    if (pos)
        *pos = g;
    return i;
}

inline int stoi(const string &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<int>(s, pos, base);
}

inline int stoi(const wstring &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<int>(s, pos, base);
}

inline long stol(const string &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<long>(s, pos, base);
}

inline long stol(const wstring &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<long>(s, pos, base);
}

inline long long stoll(const string &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<long long>(s, pos, base);
}

inline long long stoll(const wstring &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<long long>(s, pos, base);
}

inline unsigned long stoul(const string &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<unsigned long>(s, pos, base);
}

inline unsigned long stoul(const wstring &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<unsigned long>(s, pos, base);
}

inline
unsigned long long stoull(const string &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<unsigned long long>(s, pos, base);
}

inline
unsigned long long stoull(const wstring &s, size_t *pos = 0, int base = 10)
{
    return glglT::bs_to_all<unsigned long long>(s, pos, base);
}

inline float stof(const string &s, size_t *pos = 0)
{
    return glglT::bs_to_all<float>(s, pos);
}

inline float stof(const wstring &s, size_t *pos = 0)
{
    return glglT::bs_to_all<float>(s, pos);
}

inline double stod(const string &s, size_t *pos = 0)
{
    return glglT::bs_to_all<double>(s, pos);
}

inline double stod(const wstring &s, size_t *pos = 0)
{
    return glglT::bs_to_all<double>(s, pos);
}

inline long double stold(const string &s, size_t *pos = 0)
{
    return glglT::bs_to_all<long double>(s, pos);
}

inline long double stold(const wstring &s, size_t *pos = 0)
{
    return glglT::bs_to_all<long double>(s, pos);
}

namespace string_literal {

inline string operator""_s(const char *s, size_t n)
{
    return string(s, n);
}

inline wstring operator""_s(const wchar_t *s, size_t n)
{
    return wstring(s, n);
}

inline u16string operator""_s(const char16_t *s, size_t n)
{
    return u16string(s, n);
}

inline u32string operator""_s(const char32_t *s, size_t n)
{
    return u32string(s, n);
}

} // namespace string_literal

} // namespace glglT

#endif // GLGL_STRING_H
