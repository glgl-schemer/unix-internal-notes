#ifndef GLGL_NO_EXCEPTION_BEGIN_H
#define GLGL_NO_EXCEPTION_BEGIN_H

#include <exception>

#ifndef __cpp_exceptions

#define try if (true)
#define catch(E) if (false)
#define throw std::terminate();

#endif // __cpp_exceptions

#endif // GLGL_NO_EXCEPTION_BEGIN_H
