#ifndef GLGL_TYPEINDEX_H
#define GLGL_TYPEINDEX_H

#include <typeinfo>
#include "sub/glgl_hash.h"

namespace glglT {

class type_index {
    const std::type_info *info_wrapper;
public:
    type_index(const std::type_info &info) noexcept: info_wrapper(&info) {}

    bool operator==(const type_index &o) const noexcept
        { return *info_wrapper == *(o.info_wrapper); }
    bool operator!=(const type_index &o) const noexcept
        { return *info_wrapper != *(o.info_wrapper); }
    bool operator<(const type_index &o) const noexcept
        { return info_wrapper->before(*(o.info_wrapper)); }
    bool operator<=(const type_index &o) const noexcept
        { return !(*this > o); }
    bool operator>(const type_index &o) const noexcept
        { return o < *this; }
    bool operator>=(const type_index &o) const noexcept
        { return !(*this < o); }

    size_t hash_code() const noexcept(noexcept(info_wrapper->hash_code()))
        { return info_wrapper->hash_code(); }
    const char *name() const noexcept(noexcept(info_wrapper->name()))
        { return info_wrapper->name(); }
};

template <> struct hash<type_index>:
    function_type<size_t (type_index)>, fast_hash_base {
    size_t operator()(const type_index &ti) const
        noexcept(noexcept(ti.hash_code()))
    {
        return ti.hash_code();
    }
};

} // namespace glglT

#endif // GLGL_TYPEINDEX_H
