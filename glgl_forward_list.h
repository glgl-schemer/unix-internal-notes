#ifndef GLGL_FORWARD_LIST_H
#define GLGL_FORWARD_LIST_H

#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_allocator_traits.h"
#include "sub/glgl_base_algo.h"
#include "sub/glgl_other_iterator.h"

namespace glglT {

template <typename T, typename Alloc>
struct forward_use_node_t {
    forward_use_node_t() = default;
    template <typename ...Args>
    explicit forward_use_node_t(Args&&... args): t(glglT::forward<Args>(args)...) {}
    T t;
    typename allocator_traits<Alloc>::
        template rebind_traits<forward_use_node_t>::pointer next;
};

template <typename, typename> class forward_list;
template <typename, typename> class forward_use_iterator;

template <typename T, typename RPtr>
class forward_use_const_iterator: public iterator<forward_iterator_tag, const T> {
    template <typename, typename> friend class forward_list;
    friend class forward_use_iterator<T, RPtr>;

    RPtr curr;
public:
    constexpr forward_use_const_iterator() noexcept {}
    explicit forward_use_const_iterator(RPtr rp) noexcept: curr(rp) {}
    const T &operator*() const noexcept { return curr->t; }
    const T *operator->() const noexcept { return &(curr->t); }
    const RPtr &base() const noexcept { return curr; }
    explicit operator forward_use_iterator<T, RPtr> () const noexcept;
    forward_use_const_iterator &operator++() noexcept
    {
        curr = curr->next;
        return *this;
    }

    forward_use_const_iterator operator++(int) noexcept
    {
        forward_use_const_iterator tmp = *this;
        curr = curr->next;
        return tmp;
    }

    bool operator==(const forward_use_const_iterator &r) const noexcept
    {
        return curr == r.curr;
    }
};

template <typename T, typename P> inline
bool operator!=(const forward_use_const_iterator<T, P> &r1,
                const forward_use_const_iterator<T, P> &r2) noexcept
{
    return !(r1 == r2);
}

template <typename T, typename RPtr>
class forward_use_iterator: public iterator<forward_iterator_tag, T>  {
    template <typename, typename> friend class forward_list;
    friend class forward_use_const_iterator<T, RPtr>;

    RPtr curr;
public:
    constexpr forward_use_iterator() noexcept {}
    explicit forward_use_iterator(RPtr rp) noexcept: curr(rp) {}
    T &operator*() const noexcept { return curr->t; }
    T *operator->() const noexcept { return &(curr->t); }
    const RPtr &base() const noexcept { return curr; }
    operator forward_use_const_iterator<T, RPtr> () const noexcept
    {
        return forward_use_const_iterator<T, RPtr>(curr);
    }

    forward_use_iterator &operator++() noexcept
    {
        curr = curr->next;
        return *this;
    }

    forward_use_iterator operator++(int) noexcept
    {
        forward_use_iterator tmp = *this;
        curr = curr->next;
        return tmp;
    }

    bool operator==(const forward_use_const_iterator<T, RPtr> &r) const noexcept
    {
        return curr == r.curr;
    }
};

template <typename T, typename P> inline
bool operator!=(const forward_use_iterator<T, P> &r1,
                const forward_use_iterator<T, P> &r2) noexcept
{
    return !(r1 == r2);
}

template <typename T, typename P> inline
bool operator!=(const forward_use_const_iterator<T, P> &r1,
                const forward_use_iterator<T, P> &r2) noexcept
{
    return !(r1 == r2);
}

template <typename T, typename P> inline
bool operator!=(const forward_use_iterator<T, P> &r1,
                const forward_use_const_iterator<T, P> &r2) noexcept
{
    return !(r1 == r2);
}

template <typename T, typename P> inline
forward_use_const_iterator<T, P>::operator forward_use_iterator<T, P> () const noexcept
{
    return forward_use_iterator<T, P>(curr);
}

template <typename T, typename Alloc = allocator<T>>
class forward_list {
    typedef typename allocator_traits<Alloc>::
        template rebind_alloc<forward_use_node_t<T, Alloc>> rebind_alloc;
    typedef typename allocator_traits<Alloc>::
        template rebind_traits<forward_use_node_t<T, Alloc>> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    static constexpr size_t SF_LIM = 16;

    rebind_alloc a;
    rebind_ptr eol;
    size_t len;
public:
    typedef T value_type;
    typedef Alloc allocator_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef forward_use_iterator<T, rebind_ptr> iterator;
    typedef forward_use_const_iterator<T, rebind_ptr> const_iterator;

    explicit forward_list(const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len() { eol->next = eol; }
    forward_list(size_type n, const T &t, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        this->insert_after(const_iterator(eol), n, t);
    }

    explicit forward_list(size_type n): a(),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        while (n-- != 0)
            this->emplace_front();
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    forward_list(It b, It e, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        this->insert_after(const_iterator(eol), b, e);
    }

    forward_list(const forward_list &o):
        a(rebind_traits::select_on_container_copy_construction(o.a)),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        this->insert_after(const_iterator(eol), o.begin(), o.end());
    }

    forward_list(const forward_list &o, const Alloc &ac): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        this->insert_after(const_iterator(eol), o.begin(), o.end());
    }

    forward_list(forward_list &&o): a(glglT::move(o.a)),
        eol(rebind_traits::allocate(a, 1)), len(o.len)
    {
        eol->next = eol;
        glglT::swap(eol, o.eol);
    }

    forward_list(forward_list &&o, const Alloc &ac): a(ac),
        eol(rebind_traits::allocate(a, 1)), len(o.len)
    {
        eol->next = eol;
        if (a != o.a)
            this->insert_after_move(const_iterator(eol), o.begin(), o.end());
        else
            glglT::swap(eol, o.eol);
    }

    forward_list(std::initializer_list<T> il, const Alloc &ac = Alloc()): a(ac),
        eol(rebind_traits::allocate(a, 1)), len()
    {
        eol->next = eol;
        this->insert_after(const_iterator(eol), il.begin(), il.end());
    }

    ~forward_list() { this->free(); rebind_traits::deallocate(a, eol, 1); }

    forward_list &operator=(const forward_list &o)
    {
        if (eol == o.eol)
            return *this;
        this->clear();
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        this->insert_after(const_iterator(eol), o.begin(), o.end());
        return *this;
    }

    forward_list &operator=(forward_list &&o)
    {
        if (eol == o.eol)
            return *this;
        this->clear();
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        if (a == o.a) {
            glglT::swap(eol, o.eol);
            len = o.len;
        } else
            this->insert_after_move(const_iterator(eol), o.begin(), o.end());
    }

    forward_list &operator=(std::initializer_list<T> il)
    {
        this->assign(il);
        return *this;
    }

    void assign(size_type n, const T &t)
    {
        this->clear();
        this->insert_after(const_iterator(eol), n, t);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    void assign(It b, It e)
    {
        this->clear();
        this->insert_after(const_iterator(eol), b, e);
    }

    void assign(std::initializer_list<T> il)
    {
        this->clear();
        this->insert_after(const_iterator(eol), il.begin(), il.end());
    }

    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    reference front() noexcept { return eol->next->t; }
    const_reference front() const noexcept { return eol->next->t; }

    iterator before_begin() noexcept { return iterator(eol); }
    const_iterator before_begin() const noexcept { return const_iterator(eol); }
    const_iterator cbefore_begin() const noexcept { return const_iterator(eol); }

    iterator begin() noexcept { return iterator(eol->next); }
    const_iterator begin() const noexcept { return const_iterator(eol->next); }
    const_iterator cbegin() const noexcept { return const_iterator(eol->next); }

    iterator end() noexcept { return iterator(eol); }
    const_iterator end() const noexcept { return const_iterator(eol); }
    const_iterator cend() const noexcept { return const_iterator(eol); }

    bool empty() const noexcept { return len == 0; }
    size_type size() const noexcept { return len; }
    size_type max_size() const noexcept { return rebind_traits::max_size(a); }
    void clear() noexcept { this->free(); eol->next = eol; len = 0; }

    iterator insert_after(const_iterator itr, const T &t)
    {
        return this->emplace_after(itr, t);
    }

    iterator insert_after(const_iterator itr, T &&t)
    {
        return this->emplace_after(itr, glglT::move(t));
    }

    iterator insert_after(const_iterator itr, size_type n, const T &t)
    {
        rebind_ptr new_node = itr.curr;
        for (const rebind_ptr e = itr.curr->next; n-- != 0; ++len) {
            try {
                new_node = this->make_new_node(t);
            } catch(...) {
                for (rebind_ptr tmpc = itr.curr->next, tmpn; tmpc != e; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpn);
                    rebind_traits::deallocate(a, tmpn, 1);
                    tmpc = tmpn;
                }   itr.curr->next = e;
                throw;
            }
            new_node->next = itr.curr->next;
            itr.curr->next = new_node;
        }   return iterator(new_node);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    iterator insert_after(const_iterator itr, It b, It e)
    {
        rebind_ptr new_node = itr.curr;
        for (const rebind_ptr fe = itr.curr; b != e; ++b, ++len) {
            try {
                new_node = this->make_new_node(*b);
            } catch(...) {
                for (rebind_ptr tmpc = fe->next, tmpn; tmpc != itr.curr->next; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpn);
                    rebind_traits::deallocate(a, tmpn, 1);
                    tmpc = tmpn;
                }   fe->next = itr.curr->next;
                throw;
            }
            new_node->next = itr.curr->next;
            itr.curr->next = new_node;
            itr.curr = new_node;
        }   return iterator(new_node);
    }

    iterator insert_after(const_iterator itr, std::initializer_list<T> il)
    {
        return this->insert_after(itr, il.begin(), il.end());
    }

    template <typename... Args>
    iterator emplace_after(const_iterator itr, Args&&... args)
    {
        rebind_ptr new_node = this->make_new_node(glglT::forward<Args>(args)...);
        new_node->next = itr.curr->next;
        itr.curr->next = new_node;
        ++len;
        return iterator(new_node);
    }

    iterator erase_after(const_iterator itr)
    {
        const auto p = itr.curr->next;
        const auto n = p->next;
        itr.curr->next = n;
        rebind_traits::destroy(a, p);
        rebind_traits::deallocate(a, p, 1);
        --len;
        return iterator(n);
    }

    iterator erase_after(const_iterator bef, const_iterator e)
    {
        for (rebind_ptr p = bef.curr->next, n; p != e.curr; p = n, --len) {
            n = p->next;
            rebind_traits::destroy(a, p);
            rebind_traits::deallocate(a, p, 1);
        }   bef.curr->next = e.curr;
        return iterator(e.curr);
    }

    void push_front(const T &t) { this->emplace_front(t); }
    void push_front(T &&t) { this->emplace_front(glglT::move(t)); }
    template <typename... Args>
    void emplace_front(Args&&... args)
    {
        rebind_ptr new_node = this->make_new_node(glglT::forward<Args>(args)...);
        new_node->next = eol->next;
        eol->next = new_node;
        ++len;
    }

    void pop_front()
    {
        const auto p = eol->next;
        eol->next = p->next;
        rebind_traits::destroy(a, p);
        rebind_traits::deallocate(a, p, 1);
        --len;
    }

    void resize(size_type n)
    {
        len = n;
        rebind_ptr bef = eol;
        while (bef->next != eol) {
            if (n-- == 0) {
                this->erase_after(const_iterator(bef), const_iterator(eol));
                return;
            }   bef = bef->next;
        }
        while (n-- != 0)
            this->emplace_after(const_iterator(bef));
    }

    void resize(size_type n, const T &t)
    {
        len = n;
        rebind_ptr bef = eol;
        while (bef->next != eol) {
            if (n-- == 0) {
                this->erase_after(const_iterator(bef), const_iterator(eol));
                return;
            }   bef = bef->next;
        }
        this->insert_after(const_iterator(bef), n, t);
    }

    void swap(forward_list &o)
    {
        glglT::alloc_swap(a, o.a, typename rebind_traits::
                          propagate_on_container_swap());
        glglT::swap(eol, o.eol);
        glglT::swap(len, o.len);
    }

    void merge(forward_list &&o) { this->merge(o); }
    void merge(forward_list &o)
    {
        if (eol == o.eol)
            return;
        len += o.len;
        for (rebind_ptr bef = eol, n = bef->next, on = o.eol->next, otmp; ;) {
            if (n == eol) {
                if (on != o.eol) {
                    bef->next = on;
                    while (on->next != o.eol)
                        on = on->next;
                    on->next = eol;
                }   break;
            }
            if (on == o.eol)
                break;
            if (on->t < n->t) {
                otmp = on->next;
                bef->next = on;
                on->next = n;
                bef = on;
                on = otmp;
            } else {
                bef = n;
                n = n->next;
            }
        }
        o.len = 0;
        o.eol->next = o.eol;
    }

    template <typename Comp>
    void merge(forward_list &&o, Comp c) { this->merge(o, c); }
    template <typename Comp>
    void merge(forward_list &o, Comp c)
    {
        if (eol == o.eol)
            return;
        len += o.len;
        for (rebind_ptr bef = eol, n = bef->next, on = o.eol->next, otmp; ;) {
            if (n == eol) {
                if (on != o.eol) {
                    bef->next = on;
                    while (on->next != o.eol)
                        on = on->next;
                    on->next = eol;
                }   break;
            }
            if (on == o.eol)
                break;
            if (c(on->t, n->t)) {
                otmp = on->next;
                bef->next = on;
                on->next = n;
                bef = on;
                on = otmp;
            } else {
                bef = n;
                n = n->next;
            }
        }
        o.len = 0;
        o.eol->next = o.eol;
    }

    void splice_after(const_iterator itr, forward_list &o)
    {
        rebind_ptr ol = o.eol->next;
        while (ol->next != o.eol)
            ol = ol->next;
        ol->next = itr.curr->next;
        itr.curr->next = o.eol->next;
        len += o.len;
        o.len = 0;
        o.eol->next = o.eol;
    }

    void splice_after(const_iterator itr, forward_list &&o)
    {
        this->splice_after(itr, o);
    }

    void splice_after(const_iterator itr, forward_list &o, const_iterator it)
    {
        ++len, --o.len;
        const auto n = it.curr->next;
        if (n == itr.curr)
            return;
        it.curr->next = n->next;
        n->next = itr.curr->next;
        itr.curr->next = n;
    }

    void splice_after(const_iterator itr, forward_list &&o, const_iterator it)
    {
        this->splice_after(itr, o, it);
    }

    void splice_after(const_iterator itr, forward_list &o,
                      const_iterator b, const_iterator e)
    {
        if (eol != o.eol) {
            const auto n = glglT::distance(b, e);
            len += n, o.len -= n;
        }
        rebind_ptr ol = b.curr->next;
        while (ol->next != e.curr)
            ol = ol->next;
        ol->next = itr.curr->next;
        itr.curr->next = b.curr->next;
        b.curr->next = e.curr;
    }

    void splice_after(const_iterator itr, forward_list &&o,
                      const_iterator b, const_iterator e)
    {
        this->splice_after(itr, o, b, e);
    }

    void remove(const T &t)
    {
        for (rebind_ptr b = eol, p; b->next != eol; b = b->next) {
            p = b->next;
            if (p->t == t)
                this->delete_p(b, p);
        }
    }

    template <typename Pred>
    void remove_if(Pred pd)
    {
        for (rebind_ptr b = eol, p; b->next != eol; b = b->next) {
            p = b->next;
            if (pd(p->t))
                this->delete_p(b, p);
        }
    }

    void reverse()
    {
        for (rebind_ptr bef = eol, b = eol->next, p; ; bef = b, b = p) {
            p = b->next;
            b->next = bef;
            if (b == eol) break;
        }
    }

    void unique()
    {
        for (rebind_ptr b = eol->next, p; b != eol;) {
            p = b->next;
            if (b->t == p->t)
                this->delete_p(b, p);
            else b = b->next;
        }
    }

    template <typename Pred>
    void unique(Pred pd)
    {
        for (rebind_ptr b = eol->next, p; b != eol;) {
            p = b->next;
            if (pd(b->t, p->t))
                this->delete_p(b, p);
            else b = b->next;
        }
    }

    template <typename Comp>
    void sort(Comp c) { sort_aux(eol, eol, len, len / 2, c); }
    void sort() { sort_aux(eol, eol, len, len / 2); }
private:
    void free() noexcept
    {
        for (rebind_ptr p = eol->next, tmp; p != eol; p = tmp) {
            tmp = p->next;
            rebind_traits::destroy(a, p);
            rebind_traits::deallocate(a, p, 1);
        }
    }

    template <typename... Args>
    rebind_ptr make_new_node(Args&&... args)
    {
        rebind_ptr p = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, p, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, p, 1);
            throw;
        }
        return p;
    }

    void delete_p(rebind_ptr bef, rebind_ptr p) noexcept
    {
        --len;
        bef->next = p->next;
        rebind_traits::destroy(a, p);
        rebind_traits::deallocate(a, p, 1);
    }

    void insert_after_move(const_iterator itr, iterator b, iterator e)
    {
        rebind_ptr new_node = itr.curr;
        for (const rebind_ptr fe = itr.curr; b != e; ++b, ++len) {
            try {
                new_node = this->make_new_node(glglT::move(*b));
            } catch(...) {
                for (rebind_ptr tmpc = fe->next, tmpn; tmpc != itr.curr->next; --len) {
                    tmpn = tmpc->next;
                    rebind_traits::destroy(a, tmpn);
                    rebind_traits::deallocate(a, tmpn, 1);
                    tmpc = tmpn;
                }   fe->next = itr.curr->next;
                throw;
            }
            new_node->next = itr.curr->next;
            itr.curr->next = new_node;
            itr.curr = new_node;
        }
    }

    static rebind_ptr insertion_sort(rebind_ptr bef, rebind_ptr e)
    {
        rebind_ptr bp = bef->next;
        for (rebind_ptr p = bp->next; p != e; p = bp->next) {
            for (rebind_ptr prev = bef, b = bef->next; ; prev = b, b = b->next) {
                if (p->t < b->t) {
                    prev->next = p;
                    bp->next = p->next;
                    p->next = b;
                } else if (p == b->next) {
                    bp = p;
                } else continue;
                break;
            }
        }   return bp;
    }

    static rebind_ptr merge_self(rebind_ptr bef, rebind_ptr bef_m, rebind_ptr e)
    {
        rebind_ptr bm = bef_m;
        for (rebind_ptr b = bef->next, m = bef_m->next; ;) {
            if (m->t < b->t) {
                bm->next = m->next;
                bef->next = m;
                bef = m;
                m->next = b;
                m = bm->next;
                if (m == e) break;
            } else {
                if (b == bef_m) {
                    do {
                        bm = bm->next;
                    } while (bm->next != e);
                    break;
                }
                bef = b;
                b = b->next;
            }
        }   return bm;
    }

    static rebind_ptr sort_aux(rebind_ptr bef, rebind_ptr e, size_t l, size_t h)
    {
        if (h == 0)
            return bef->next;
        if (l < SF_LIM)
            return insertion_sort(bef, e);
        rebind_ptr bef_m = bef;
        for (size_t i = 0; i != h; ++i)
            bef_m = bef_m->next;
        const auto t = l - h;
        bef_m = sort_aux(bef, bef_m->next, h, h / 2);
        sort_aux(bef_m, e, t, t / 2);
        return merge_self(bef, bef_m, e);
    }

    template <typename Comp>
    static rebind_ptr insertion_sort(rebind_ptr bef, rebind_ptr e, Comp c)
    {
        rebind_ptr bp = bef->next;
        for (rebind_ptr p = bp->next; p != e; p = bp->next) {
            for (rebind_ptr prev = bef, b = bef->next; ; prev = b, b = b->next) {
                if (c(p->t, b->t)) {
                    prev->next = p;
                    bp->next = p->next;
                    p->next = b;
                } else if (p == b->next) {
                    bp = p;
                } else continue;
                break;
            }
        }   return bp;
    }

    template <typename Comp>
    static rebind_ptr merge_self(rebind_ptr bef, rebind_ptr bef_m, rebind_ptr e,
                                 Comp c)
    {
        rebind_ptr bm = bef_m;
        for (rebind_ptr b = bef->next, m = bef_m->next; ;) {
            if (c(m->t, b->t)) {
                bm->next = m->next;
                bef->next = m;
                bef = m;
                m->next = b;
                m = bm->next;
                if (m == e) break;
            } else {
                if (b == bef_m) {
                    do {
                        bm = bm->next;
                    } while (bm->next != e);
                    break;
                }
                bef = b;
                b = b->next;
            }
        }   return bm;
    }

    template <typename Comp>
    static rebind_ptr sort_aux(rebind_ptr bef, rebind_ptr e, size_t l, size_t h,
                               Comp c)
    {
        if (h == 0)
            return bef->next;
        if (l < SF_LIM)
            return insertion_sort(bef, e, c);
        rebind_ptr bef_m = bef;
        for (size_t i = 0; i != h; ++i)
            bef_m = bef_m->next;
        const auto t = l - h;
        bef_m = sort_aux(bef, bef_m->next, h, h / 2, c);
        sort_aux(bef_m, e, t, t / 2, c);
        return merge_self(bef, bef_m, e, c);
    }
};

template <typename T, typename A> inline
void swap(forward_list<T, A> &l1, forward_list<T, A> &l2)
{
    l1.swap(l2);
}

template <typename T, typename A>
bool operator==(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    if (l1.size() != l2.size())
        return false;
    const auto e = l1.end();
    for (auto it1 = l1.begin(), it2 = l2.begin(); it1 != e; ++it1, ++it2)
        if (*it1 != *it2)
            return false;
    return true;
}

template <typename T, typename A> inline
bool operator!=(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    return !(l1 == l2);
}

template <typename T, typename A>
bool operator<(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    auto it1 = l1.begin(), it2 = l2.begin();
    const auto e1 = l1.end(), e2 = l2.end();
    while (true) {
        if (it1 == e1)
            return it2 != e2;
        if (it2 == e2)
            return false;
        if (*it1 < *it2)
            return true;
        if (*it2 < *it1)
            return false;
        ++it1, ++it2;
    }
}

template <typename T, typename A> inline
bool operator>=(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    return !(l1 < l2);
}

template <typename T, typename A> inline
bool operator>(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    return l2 < l1;
}

template <typename T, typename A> inline
bool operator<=(const forward_list<T, A> &l1, const forward_list<T, A> &l2)
{
    return !(l2 < l1);
}

} // namespace glglT

#endif // GLGL_FORWARD_LIST_H
