#ifndef GLGL_LOCKFREE_H
#define GLGL_LOCKFREE_H

#include <atomic>
#include "sub/glgl_allocator.h"
#include "sub/glgl_shared_ptr.h"
#include "sub/glgl_unique_ptr.h"

namespace glglT {

template <typename Alloc>
class lockfree_alloc {
    shared_ptr<Alloc> a;
protected:
    explicit lockfree_alloc(const Alloc &ac): a(glglT::make_shared<Alloc>(ac)) {}
    Alloc &get() noexcept { return *a; }
};

template <typename T>
class lockfree_alloc<allocator<T>>: allocator<T> {
protected:
    explicit lockfree_alloc(const allocator<T> &) {}
    allocator<T> &get() noexcept { return *this; }
};

template <typename Alloc>
struct lockfree_deleter: lockfree_alloc<Alloc> {
    typedef typename allocator_traits<Alloc>::pointer pointer;
    explicit lockfree_deleter(const lockfree_alloc<Alloc> &a):
        lockfree_alloc<Alloc>(a) {}
    void operator()(const pointer p) noexcept
    {
        if (p == nullptr)
            return;
        allocator_traits<Alloc>::destroy(this->get(), p);
        allocator_traits<Alloc>::deallocate(this->get(), p, 1);
    }
};

template <typename> struct lockfree_base;
template <typename> struct hazard_pointer;

template <typename T, typename Alloc, typename Node,
          template <typename, typename, typename> class LockFree>
struct lockfree_base<LockFree<T, Alloc, Node>>: lockfree_alloc<Alloc> {
    typedef T value_type;
    typedef Alloc allocator_type;
protected:
    explicit lockfree_base(const Alloc &a): lockfree_alloc<Alloc>(a) {}

    template <typename... Args>
    Node *make_new_node(Args&&... args)
    {
        auto p = allocator_traits<Alloc>::allocate(this->get(), 1);
        try {
            allocator_traits<Alloc>::construct(this->get(), p, glglT::forward<Args>(args)...);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(this->get(), p, 1);
            throw;
        }
        try {
            return new Node(static_cast<LockFree<T, Alloc, Node> *>(this)->make_unique_ptr(p));
        } catch(...) {
            allocator_traits<Alloc>::destroy(this->get(), p);
            allocator_traits<Alloc>::deallocate(this->get(), p, 1);
            throw;
        }
    }
};

template <typename T, typename Alloc, typename Node,
          template <typename, typename, typename> class LockFree>
class hazard_pointer<LockFree<T, Alloc, Node>> {
    struct hazard_ptr {
        std::atomic<Node *> p;
        hazard_ptr *next;
        std::atomic_bool need_clean, used;
    };

    std::atomic<hazard_ptr *> head;
protected:
    constexpr hazard_pointer() noexcept: head(nullptr) {}
    ~hazard_pointer()
    {
        for (hazard_ptr *hp = head.load(std::memory_order_acquire), *n; hp != nullptr; hp = n) {
            n = hp->next;
            delete hp;
        }
    }

    hazard_ptr *hazard_ptr_alloc()
    {
        for (hazard_ptr *hp = head.load(std::memory_order_acquire); hp; hp = hp->next)
            if (hp->used.exchange(true, std::memory_order_acq_rel) == false)
                return hp;
        auto hp = new hazard_ptr;
        std::atomic_init(&(hp->p), (Node *)(nullptr));
        hp->next = nullptr;
        std::atomic_init(&(hp->need_clean), false);
        std::atomic_init(&(hp->used), true);
        this->hazard_ptr_push(hp);
        return hp;
    }

    void hazard_ptr_store(hazard_ptr *hp, Node *p) noexcept
    {
        if (p != nullptr) {
            hp->p.store(p, std::memory_order_release);
        } else
            hp->need_clean.store(true, std::memory_order_release);
    }

    bool hazard_ptr_check(Node *p) noexcept
    {
        for (auto hp = head.load(std::memory_order_acquire); hp; hp = hp->next) {
            if (hp->need_clean.load(std::memory_order_acquire)) {
                if (hp->p.load(std::memory_order_relaxed) != p)
                    continue;
                this->hazard_ptr_clear(hp);
            } else if (hp->p.load(std::memory_order_acquire) == p)
                return true;
        }
        return false;
    }

    void hazard_ptr_clear(hazard_ptr *hp) noexcept
    {
        hp->p.store(nullptr, std::memory_order_relaxed);
        hp->need_clean.store(false, std::memory_order_release);
        hp->used.store(false, std::memory_order_release);
    }

    void hazard_ptr_free(Node *p) noexcept
    {
        if (this->hazard_ptr_check(p))
            return static_cast<LockFree<T, Alloc, Node> *>(this)->garbage_push(p);
        delete p;
        static_cast<LockFree<T, Alloc, Node> *>(this)->garbage_claim();
    }
private:
    void hazard_ptr_push(hazard_ptr *hp) noexcept
    {
        hp->next = head.load(std::memory_order_acquire);
        while (!head.compare_exchange_weak(hp->next, hp, std::memory_order_acq_rel, std::memory_order_acquire));
    }
};

template <typename T, typename Alloc>
struct lockfree_stack_node {
    unique_ptr<T, lockfree_deleter<Alloc>> data;
    lockfree_stack_node *next;
    explicit lockfree_stack_node(unique_ptr<T, lockfree_deleter<Alloc>> &&p):
        data(glglT::move(p)) {}
};

template <typename T, typename Alloc = allocator<T>, typename Node = lockfree_stack_node<T, Alloc>>
class lockfree_stack:
    public lockfree_base<lockfree_stack<T, Alloc, Node>>,
    public hazard_pointer<lockfree_stack<T, Alloc, Node>> {
    std::atomic<Node *> head, g;
public:
    explicit lockfree_stack(const Alloc &a = Alloc()):
        lockfree_base<lockfree_stack>(a), hazard_pointer<lockfree_stack>(),
        head(nullptr), g(nullptr) {}
    lockfree_stack(const lockfree_stack &) = delete;
    lockfree_stack &operator=(const lockfree_stack &) = delete;
    ~lockfree_stack()
    {
        for (auto p = head.load(std::memory_order_acquire), old = p; p != nullptr; old = p) {
            p = p->next;
            delete old;
        }
    }

    void push(const T &t) { this->emplace(t); }
    void push(T &&t) { this->emplace(glglT::move(t)); }
    template <typename... Args>
    void emplace(Args&&... args)
    {
        auto p = this->make_new_node(glglT::forward<Args>(args)...);
        p->next = head.load(std::memory_order_acquire);
        while (!head.compare_exchange_weak(p->next, p, std::memory_order_acq_rel, std::memory_order_acquire));
    }

    unique_ptr<T, lockfree_deleter<Alloc>> pop()
    {
        auto hp = this->hazard_ptr_alloc();
        auto p = head.load(std::memory_order_acquire);
        do {
            if (p == nullptr) {
                this->hazard_ptr_clear(hp);
                return this->make_unique_ptr(nullptr);
            }
            this->hazard_ptr_store(hp, p);
        } while (!head.compare_exchange_strong(p, p->next, std::memory_order_acq_rel, std::memory_order_acquire));
        auto up = glglT::move(p->data);
        this->hazard_ptr_store(hp, nullptr);
        this->hazard_ptr_free(p);
        return up;
    }

    unique_ptr<T, lockfree_deleter<Alloc>> make_unique_ptr(typename allocator_traits<Alloc>::pointer p)
    {
        return unique_ptr<T, lockfree_deleter<Alloc>>(p, lockfree_deleter<Alloc>(*this));
    }

    void garbage_push(Node *p) noexcept
    {
        p->next = g.load(std::memory_order_acquire);
        while (!g.compare_exchange_weak(p->next, p, std::memory_order_acq_rel, std::memory_order_acquire));
    }

    void garbage_claim() noexcept
    {
        auto all_g = g.exchange(nullptr, std::memory_order_acq_rel);
        for (Node *next; all_g; all_g = next) {
            next = all_g->next;
            if (this->hazard_ptr_check(all_g))
                this->garbage_push(all_g);
            else
                delete all_g;
        }
    }
};

template <typename T, typename Alloc>
struct lockfree_queue_node {
    unique_ptr<T, lockfree_deleter<Alloc>> data;
    std::atomic<lockfree_queue_node *> next;
    explicit lockfree_queue_node(unique_ptr<T, lockfree_deleter<Alloc>> &&p):
        data(glglT::move(p)), next(nullptr) {}
};

template <typename T, typename Alloc = allocator<T>, typename Node = lockfree_queue_node<T, Alloc>>
class lockfree_queue:
    public lockfree_base<lockfree_queue<T, Alloc, Node>>,
    public hazard_pointer<lockfree_queue<T, Alloc, Node>> {
    std::atomic<Node *> head, tail, g;
public:
    explicit lockfree_queue(const Alloc &a = Alloc()):
        lockfree_base<lockfree_queue>(a), hazard_pointer<lockfree_queue>(), g(nullptr)
    {
        auto null_node = new Node(this->make_unique_ptr(nullptr));
        std::atomic_init(&head, null_node);
        std::atomic_init(&tail, null_node);
    }

    lockfree_queue(const lockfree_queue&) = delete;
    lockfree_queue &operator=(const lockfree_queue &) = delete;
    ~lockfree_queue()
    {
        for (auto p = head.load(std::memory_order_acquire), old = p; p != nullptr; old = p) {
            p = p->next.load(std::memory_order_acquire);
            delete old;
        }
    }

    void push(const T &t) { this->emplace(t); }
    void push(T &&t) { this->emplace(glglT::move(t)); }
    template <typename... Args>
    void emplace(Args&&... args)
    {
        decltype(this->hazard_ptr_alloc()) hp = this->hazard_ptr_alloc();
        Node *p;
        try {
            p = this->make_new_node(glglT::forward<Args>(args)...);
        } catch(...) {
            this->hazard_ptr_clear(hp);
            throw;
        }
        auto t = tail.load(std::memory_order_acquire);
        do {
            this->hazard_ptr_store(hp, t);
        } while (!tail.compare_exchange_strong(t, p, std::memory_order_acq_rel, std::memory_order_acquire));
        t->next.store(p, std::memory_order_release);
        this->hazard_ptr_clear(hp);
    }

    unique_ptr<T, lockfree_deleter<Alloc>> pop()
    {
        decltype(this->hazard_ptr_alloc()) hp = this->hazard_ptr_alloc(), n_hp;
        try {
            n_hp = this->hazard_ptr_alloc();
        } catch(...) {
            this->hazard_ptr_clear(hp);
            throw;
        }
        Node *p = head.load(std::memory_order_acquire), *h;
        do {
            this->hazard_ptr_store(hp, p);
            h = p->next.load(std::memory_order_acquire);
            if (h == nullptr) {
                this->hazard_ptr_clear(hp);
                this->hazard_ptr_clear(n_hp);
                return this->make_unique_ptr(nullptr);
            }
            this->hazard_ptr_store(n_hp, h);
        } while (!head.compare_exchange_strong(p, h, std::memory_order_acq_rel, std::memory_order_acquire));
        auto up = glglT::move(h->data);
        this->hazard_ptr_clear(n_hp);
        this->hazard_ptr_store(hp, nullptr);
        this->hazard_ptr_free(p);
        return up;
    }

    unique_ptr<T, lockfree_deleter<Alloc>> make_unique_ptr(typename allocator_traits<Alloc>::pointer p)
    {
        return unique_ptr<T, lockfree_deleter<Alloc>>(p, lockfree_deleter<Alloc>(*this));
    }

    void garbage_push(Node *p) noexcept
    {
        auto n = g.load(std::memory_order_acquire);
        do {
            p->next.store(n, std::memory_order_relaxed);
        } while (g.compare_exchange_strong(n, p, std::memory_order_acq_rel, std::memory_order_acquire));
    }

    void garbage_claim() noexcept
    {
        auto all_g = g.exchange(nullptr, std::memory_order_acq_rel);
        for (Node *next; all_g; all_g = next) {
            next = all_g->next.load(std::memory_order_relaxed);
            if (this->hazard_ptr_check(all_g))
                this->garbage_push(all_g);
            else
                delete all_g;
        }
    }
};

} // namespace glglT

#endif // GLGL_LOCKFREE_H
