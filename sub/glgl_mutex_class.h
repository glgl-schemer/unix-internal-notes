#ifndef GLGL_MUTEX_CLASS_H
#define GLGL_MUTEX_CLASS_H

#include <atomic>
#include <chrono>
#include <thread>
#include <system_error>
#include "glgl_type_tool.h"

namespace glglT {

template <typename Integer> class generic_mutex;

template <typename Integer, typename Pred> void mutex_wait_aux(generic_mutex<Integer> &, Pred);

template <typename Integer>
class generic_mutex {
    template <typename Int, typename Pred> friend void mutex_wait_aux(generic_mutex<Int> &, Pred);

    typedef typename std::chrono::high_resolution_clock::rep rep;
    std::atomic<Integer> f = ATOMIC_VAR_INIT(0);
    std::atomic<rep> sleep;
    std::atomic<rep> loop;
public:
    constexpr generic_mutex() noexcept: sleep(16), loop(16) {}
    generic_mutex(const generic_mutex &) = delete;

    void lock()
    {
        mutex_wait_aux(*this, [this](){ return try_lock(); });
    }

    bool try_lock()
    {
        return !(f.load(std::memory_order_acquire) ||
                 f.exchange(true, std::memory_order_acquire));
    }

    void unlock()
    {
        f.store(false, std::memory_order_release);
    }
};

template <typename Integer, typename Pred>
void mutex_wait_aux(generic_mutex<Integer> &m, Pred pred)
{
    typedef typename std::chrono::high_resolution_clock::duration duration;

    auto n = m.loop.load(std::memory_order_relaxed);
    auto t = m.sleep.load(std::memory_order_relaxed);
    const auto tmp = t, nmp = n;
    while (true) {
        auto i = n;
        while (!pred() && --i != 0);
        if (i != 0) {
            if (i == n)
                t -= t / 2;
            else if (n < t)
                n -= i / 2;
            else
                n += t;
            break;
        } else
            std::this_thread::sleep_for(duration(++t));
    }
    if (n != nmp)
        m.loop.store(n, std::memory_order_relaxed);
    if (t != tmp)
        m.sleep.store(t, std::memory_order_relaxed);
}

template <typename Mutex>
class generic_recursive_mutex {
    Mutex m;
    std::atomic<typename std::thread::id> pid;
    size_t i;
public:
    generic_recursive_mutex() noexcept: m(), pid(typename std::thread::id()), i(0) {}
    generic_recursive_mutex(const generic_recursive_mutex &) = delete;

    void lock()
    {
        typename std::thread::id curr = pid.load(std::memory_order_relaxed);
        if (curr != std::this_thread::get_id()) {
            m.lock();
            pid.store(std::this_thread::get_id(), std::memory_order_relaxed);
        } else
            this->check();
        ++i;
    }

    void unlock()
    {
        if (--i == 0) {
            pid.store(typename std::thread::id(), std::memory_order_relaxed);
            m.unlock();
        }
    }

    bool try_lock()
    {
        typename std::thread::id curr = pid.load(std::memory_order_relaxed);
        bool hold = curr == std::this_thread::get_id();
        if (!hold) {
            hold = m.try_lock();
            if (hold)
                pid.store(std::this_thread::get_id(), std::memory_order_relaxed);
        } else
            this->check();
        i += hold;
        return hold;
    }
private:
    void check()
    {
        if (i == SIZE_MAX)
            throw std::system_error(int(std::errc::result_out_of_range),
                                    std::generic_category(), "lock counter");
    }
};

template <typename Mutex>
class generic_timed_mutex {
    Mutex m;
public:
    generic_timed_mutex() noexcept: m() {}
    generic_timed_mutex(const generic_timed_mutex &) = delete;

    void lock() { m.lock(); }
    void unlock() { m.unlock(); }
    bool try_lock() { return m.try_lock(); }

    template <typename R, typename P>
    bool try_lock_for(const std::chrono::duration<R, P> &d)
    {
        if (m.try_lock())
            return true;
        std::this_thread::sleep_for(d);
        return false;
    }

    template <typename C, typename D>
    bool try_lock_until(const std::chrono::time_point<C, D> &p)
    {
        if (m.try_lock())
            return true;
        std::this_thread::sleep_until(p);
        return false;
    }
};

template <typename Integer>
class generic_shared_mutex {
    generic_mutex<Integer> m;
    generic_mutex<bool> exclude;
public:
    constexpr generic_shared_mutex() noexcept = default;
    generic_shared_mutex(const generic_shared_mutex &) = delete;

    void lock()
    {
        exclude.lock();
        std::atomic<Integer> *p = reinterpret_cast<std::atomic<Integer> *>(&m);
        mutex_wait_aux(m, [p](){ return p->load(std::memory_order_acquire) == 0; });
    }

    void unlock() { exclude.unlock(); }
    bool try_lock()
    {
        if (exclude.try_lock() == false)
            return false;
        std::atomic<Integer> *p = reinterpret_cast<std::atomic<Integer> *>(&m);
        if (p->load(std::memory_order_acquire) == 0)
            return true;
        exclude.unlock();
        return false;
    }

    void lock_shared()
    {
        exclude.lock();
        std::atomic<Integer> *p = reinterpret_cast<std::atomic<Integer> *>(&m);
        p->fetch_add(1, std::memory_order_relaxed);
        exclude.unlock();
    }

    bool try_lock_shared()
    {
        if (exclude.try_lock() == false)
            return false;
        std::atomic<Integer> *p = reinterpret_cast<std::atomic<Integer> *>(&m);
        p->fetch_add(1, std::memory_order_relaxed);
        exclude.unlock();
        return true;
    }

    void unlock_shared()
    {
        std::atomic<Integer> *p = reinterpret_cast<std::atomic<Integer> *>(&m);
        p->fetch_sub(1, std::memory_order_release);
    }
};

template <typename Mutex>
struct generic_shared_timed_mutex : generic_timed_mutex<Mutex> {
    generic_shared_timed_mutex() noexcept = default;
    generic_shared_timed_mutex(const generic_shared_timed_mutex &) = delete;

    void lock_shared() { reinterpret_cast<Mutex *>(this)->lock_shared(); }
    void unlock_shared() { reinterpret_cast<Mutex *>(this)->unlock_shared(); }
    bool try_lock_shared() { return reinterpret_cast<Mutex *>(this)->try_lock_shared(); }

    template <typename R, typename P>
    bool try_lock_shared_for(const std::chrono::duration<R, P> &d)
    {
        if (this->try_lock_shared())
            return true;
        std::this_thread::sleep_for(d);
        return false;
    }

    template <typename C, typename D>
    bool try_lock_shared_until(const std::chrono::time_point<C, D> &p)
    {
        if (this->try_lock_shared())
            return true;
        std::this_thread::sleep_until(p);
        return false;
    }
};

using mutex = generic_mutex<bool>;
using timed_mutex = generic_timed_mutex<mutex>;
using recursive_mutex = generic_recursive_mutex<mutex>;
using recursive_timed_mutex = generic_timed_mutex<recursive_mutex>;
using shared_mutex = generic_shared_mutex<size_t>;
using shared_time_mutex = generic_shared_timed_mutex<shared_mutex>;

class once_flag {
    std::atomic_bool f, g;
public:
    constexpr once_flag() noexcept: f(false), g(false) {}
    once_flag(const once_flag &) = delete;

    template <typename Callable, typename... Args>
    void call_once(Callable &&fun, Args&&... args)
    {
        while (true) {
            if (f.load(std::memory_order_acquire) == false &&
                f.exchange(true, std::memory_order_acq_rel) == false)
                break;
            if (g.load(std::memory_order_acquire))
                return;
            std::this_thread::yield();
        }
        try {
            glglT::invoke(glglT::forward<Callable>(fun), glglT::forward<Args>(args)...);
        } catch(...) {
            f.store(false, std::memory_order_release);
            throw;
        }
        g.store(true, std::memory_order_release);
    }
};

template <typename Callable, typename... Args> inline
void call_once(once_flag &f, Callable &&fun, Args&&... args)
{
    f.call_once(glglT::forward<Callable>(fun), glglT::forward<Args>(args)...);
}

template <typename Mutex0, typename Mutex1> inline
int try_lock(Mutex0 &m0, Mutex1 &m1)
{
    if (m0.try_lock()) {
        try {
            if (m1.try_lock())
                return -1;
        } catch(...) {
            m0.unlock();
            throw;
        }
        m0.unlock();
        return 1;
    }
    return 0;
}

template <typename Mutex0, typename Mutex1, typename... Mutexs> inline
int try_lock(Mutex0 &m0, Mutex1 &m1, Mutexs&... ms)
{
    if (m0.try_lock()) {
        try {
            const int i = glglT::try_lock(m1, ms...);
            if (i != -1)
                m0.unlock();
            return i == -1 ? -1 : i + 1;
        } catch(...) {
            m0.unlock();
            throw;
        }
    }
    return 0;
}

template <typename Mutex0, typename Mutex1, typename... Mutexs>
void lock(Mutex0 &m0, Mutex1 &m1, Mutexs&... ms)
{
    while (glglT::try_lock(m0, m1, ms...) != -1)
        std::this_thread::yield();
}

} // namespace glglT

#endif // GLGL_MUTEX_CLASS_H
