#ifndef GLGL_MUTABLE_HEAP_BASE_H
#define GLGL_MUTABLE_HEAP_BASE_H

#include "glgl_tuple_im.h"
#include "glgl_other_iterator.h"
#include "glgl_allocator_traits.h"

namespace glglT {

template <typename, typename> class fib_node_t;
template <typename> class _2_3_4_node_t;

template <typename, typename> class fib_const_iterator;
template <typename, typename> class _2_3_4_const_iterator;

template <typename, typename, typename> class fibonacci_heap;
template <typename, typename, typename> class _2_3_4_heap;

template <typename> struct heap_iter_adaptor;

template <typename T, typename C, typename A>
struct heap_iter_adaptor<fibonacci_heap<T, C, A>> {
    typedef typename allocator_traits<A>::
        template rebind_alloc<fib_node_t<T, A>> rebind_alloc;
    typedef typename allocator_traits<A>::
        template rebind_traits<fib_node_t<T, A>> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    typedef fib_const_iterator<T, rebind_ptr> iterator;
};

template <typename T, typename C, typename A>
struct heap_iter_adaptor<_2_3_4_heap<T, C, A>> {
    typedef A rebind_alloc;
    typedef allocator_traits<A> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    typedef _2_3_4_const_iterator<T, rebind_ptr> iterator;
};

template <typename T, typename Comp, typename Alloc, typename Heap>
class mutable_heap_base {
    typedef typename heap_iter_adaptor<Heap>::rebind_traits rebind_traits;
public:
    typedef T value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef Comp value_compare;
    typedef Alloc allocator_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef typename heap_iter_adaptor<Heap>::iterator iterator;
    typedef iterator const_iterator;
    typedef glglT::reverse_iterator<iterator> reverse_iterator;
    typedef glglT::reverse_iterator<const_iterator> const_reverse_iterator;

    Comp value_comp() const { return static_cast<Heap *>(this)->comp; }
    Alloc get_allocator() const { return static_cast<Heap *>(this)->a; }

    size_type max_size() const noexcept
    {
        return rebind_traits::max_size(static_cast<const Heap *>(this)->a);
    }

    const_iterator cend() const noexcept
    {
        return static_cast<const Heap *>(this)->end();
    }

    const_iterator cbegin() const noexcept
    {
        return static_cast<const Heap *>(this)->begin();
    }

    const_reverse_iterator rbegin() const noexcept { return this->crbegin(); }
    const_reverse_iterator crbegin() const noexcept
    {
        return const_reverse_iterator(this->cend());
    }

    const_reverse_iterator rend() const noexcept { return this->crend(); }
    const_reverse_iterator crend() const noexcept
    {
        return const_reverse_iterator(this->cbegin());
    }

    template <typename It>
    iterator insert(const_iterator, It b, It e)
    {
        return this->insert(b, e);
    }

    template <typename It>
    iterator insert(It b, It e)
    {
        iterator res;
        struct not_move_t {
            iterator *p;
            iterator &operator*() const noexcept { return *p; }
            not_move_t &operator++() noexcept { return *this; }
            not_move_t &operator++(int) noexcept { return *this; }
        };
        static_cast<Heap *>(this)->push(b, e, not_move_t{&res});
        return res;
    }

    iterator insert(std::initializer_list<T> il)
    {
         return this->insert(il.begin(), il.end());
    }

    iterator insert(const_iterator, std::initializer_list<T> il)
    {
        return this->insert(il.begin(), il.end());
    }

    template <typename OutputIter = tuple<>>
    void push(std::initializer_list<T> il, OutputIter may = glglT::ignore)
    {
        static_cast<Heap *>(this)->push(il.begin(), il.end(), may);
    }

    void push(const T &t) { this->insert(t); }
    void push(T &&t) { this->insert(glglT::move(t)); }
    iterator insert(const_iterator, const T &t) { return this->insert(t); }
    iterator insert(const_iterator, T &&t) { return this->insert(glglT::move(t)); }
    iterator insert(const T &t) { return static_cast<Heap *>(this)->emplace(t); }
    iterator insert(T &&t)
    {
        return static_cast<const Heap *>(this)->emplace(glglT::move(t));
    }
};

} // namespace glglT

#endif // GLGL_MUTABLE_HEAP_BASE_H
