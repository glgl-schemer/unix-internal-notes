#ifndef GLGL_CONTAINER_ADAPTOR_H
#define GLGL_CONTAINER_ADAPTOR_H

#include <initializer_list>
#include "glgl_memory_tool.h"

namespace glglT {

template <typename Container>
struct container_adaptor_base {
    typedef Container container_type;
    typedef typename Container::value_type value_type;
    typedef typename Container::size_type size_type;
    typedef typename Container::reference reference;
    typedef typename Container::const_reference const_reference;
};

} // namespace glglT

#endif // GLGL_CONTAINER_ADAPTOR_H
