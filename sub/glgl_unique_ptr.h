#ifndef GLGL_UNIQUE_PTR_H
#define GLGL_UNIQUE_PTR_H

#include "glgl_functional_base.h"

namespace glglT {

template <typename> class shared_ptr;

template <typename T>
struct default_delete {
    constexpr default_delete() = default;
    void operator()(T *p) const noexcept { delete p; }
};

template <typename T>
struct default_delete<T []> {
    constexpr default_delete() = default;
    void operator()(T *p) const noexcept { delete [] p; }
};

template <typename T, typename D, typename = type_helper<>>
struct pointer_type { typedef T * pointer; };

template <typename T, typename D>
struct pointer_type<T, D,
                    type_helper<typename remove_reference<D>::type::pointer>> {
    typedef typename remove_reference<D>::type::pointer pointer;
};

template <typename D, bool is_lref = is_lvalue_reference<D>::value>
struct judge_d_t {
    typedef typename add_const<D>::type & ltype;
    typedef D && rtype;
};

template <typename D>
struct judge_d_t<D, true> {
    typedef D ltype;
    typedef typename remove_reference<D>::type && rtype;
};

template <typename T, typename D = default_delete<T>>
class unique_ptr {
    template <typename> friend class shared_ptr;
    typename pointer_type<T, D>::pointer ptr = nullptr;
    D deleter;
public:
    typedef typename pointer_type<T, D>::pointer pointer;
    typedef T element_type;
    typedef D deleter_type;

    constexpr unique_ptr() noexcept = default;
    constexpr unique_ptr(decltype(nullptr)) noexcept: ptr(nullptr) {}
    explicit unique_ptr(pointer p) noexcept: ptr(glglT::move(p)) {}
    unique_ptr(pointer p, typename judge_d_t<D>::ltype d) noexcept:
        ptr(glglT::move(p)), deleter(d) {}
    unique_ptr(pointer p, typename judge_d_t<D>::rtype d) noexcept:
        ptr(glglT::move(p)), deleter(glglT::move(d)) {}
    unique_ptr(unique_ptr &&u) noexcept:
        ptr(u.ptr), deleter(u.deleter) { u.ptr = nullptr; }

    template <typename O, typename OD>
    unique_ptr(unique_ptr<O, OD> &&u) noexcept:
        ptr(u.ptr), deleter(u.deleter) { u.ptr = nullptr; }
    ~unique_ptr() { deleter(ptr); }

    unique_ptr &operator=(decltype(nullptr)) noexcept
    {
        deleter(ptr);
        ptr = nullptr;
        return *this;
    }

    unique_ptr &operator=(unique_ptr &&u) noexcept
    {
        deleter(ptr);
        ptr = u.ptr;
        u.ptr = nullptr;
        deleter = u.deleter;
        return *this;
    }

    template <typename O, typename OD>
    unique_ptr &operator=(unique_ptr<O, OD> &&u) noexcept
    {
        deleter(ptr);
        ptr = u.ptr;
        u.ptr = nullptr;
        deleter = u.deleter;
        return *this;
    }

    pointer release() noexcept
    {
        pointer p = glglT::move(ptr);
        ptr = nullptr;
        return p;
    }

    void reset(pointer p = pointer()) noexcept
    {
        deleter(ptr);
        ptr = glglT::move(p);
    }

    void swap(unique_ptr &u) noexcept
    {
        using glglT::swap;
        swap(ptr, u.ptr);
        swap(deleter, u.deleter);
    }

    pointer get() const noexcept { return ptr; }
    D &get_deleter() noexcept { return deleter; }
    const D &get_deleter() const noexcept { return deleter; }
    explicit operator bool() const noexcept { return ptr; }

    auto operator*() const noexcept(noexcept(*ptr))
        -> typename add_lvalue_reference<T>::type
    {
        return *ptr;
    }

    pointer operator->() const noexcept { return ptr; }
};

template <typename T, typename D>
class unique_ptr<T[], D> {
    template <typename> friend class shared_ptr;
    typename pointer_type<T, D>::pointer ptr = nullptr;
    D deleter;
public:
    typedef typename pointer_type<T, D>::pointer pointer;
    typedef T element_type;
    typedef D deleter_type;

    constexpr unique_ptr() noexcept = default;
    constexpr unique_ptr(decltype(nullptr)) noexcept: ptr() {}
    explicit unique_ptr(pointer p) noexcept: ptr(glglT::move(p)) {}
    unique_ptr(pointer p, typename judge_d_t<D>::ltype d) noexcept:
        ptr(glglT::move(p)), deleter(d) {}
    unique_ptr(pointer p, typename judge_d_t<D>::rtype d) noexcept:
        ptr(glglT::move(p)), deleter(glglT::move(d)) {}
    unique_ptr(unique_ptr &&u) noexcept:
        ptr(u.ptr), deleter(u.deleter) { u.ptr = nullptr; }
    ~unique_ptr() { deleter(ptr); }

    unique_ptr &operator=(decltype(nullptr)) noexcept
    {
        deleter(ptr);
        ptr = nullptr;
        return *this;
    }

    unique_ptr &operator=(unique_ptr &&u) noexcept
    {
        deleter(ptr);
        ptr = u.ptr;
        u.ptr = nullptr;
        deleter = u.deleter;
        return *this;
    }

    pointer release() noexcept
    {
        pointer p = glglT::move(ptr);
        ptr = nullptr;
        return p;
    }

    void reset(pointer p = pointer()) noexcept
    {
        deleter(ptr);
        ptr = glglT::move(p);
    }

    template <typename O> void reset(O) = delete;

    void reset(decltype(nullptr)) noexcept
    {
        deleter(ptr);
        ptr = nullptr;
    }

    void swap(unique_ptr &u) noexcept
    {
        using glglT::swap;
        swap(ptr, u.ptr);
        swap(deleter, u.deleter);
    }

    pointer get() const noexcept { return ptr; }
    D &get_deleter() noexcept { return deleter; }
    const D &get_deleter() const noexcept { return deleter; }
    explicit operator bool() const noexcept { return ptr; }

    T &operator[](size_t i) const { return ptr[i]; }
};

template <typename T, typename D> inline
void swap(unique_ptr<T, D>& u1, unique_ptr<T, D>& u2) noexcept
{
    u1.swap(u2);
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator==(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return u1.get() == u2.get();
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator!=(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return u1.get() != u2.get();
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator<(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return u1.get() < u2.get();
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator>(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return u2 < u1;
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator<=(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return !(u1 > u2);
}

template <typename T1, typename D1, typename T2, typename D2> inline
bool operator>=(const unique_ptr<T1, D1> &u1, const unique_ptr<T2, D2> &u2)
{
    return !(u1 < u2);
}

template <typename T, typename D> inline
bool operator==(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return !u;
}

template <typename T, typename D> inline
bool operator==(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return !u;
}

template <typename T, typename D> inline
bool operator!=(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return u;
}

template <typename T, typename D> inline
bool operator!=(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return u;
}

template <typename T, typename D> inline
bool operator<(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return u.get() < nullptr;
}

template <typename T, typename D> inline
bool operator<(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return nullptr < u.get();
}

template <typename T, typename D> inline
bool operator>(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return nullptr < u;
}

template <typename T, typename D> inline
bool operator>(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return u < nullptr;
}

template <typename T, typename D> inline
bool operator<=(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return !(u > nullptr);
}

template <typename T, typename D> inline
bool operator<=(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return !(nullptr > u);
}

template <typename T, typename D> inline
bool operator>=(const unique_ptr<T, D> &u, decltype(nullptr)) noexcept
{
    return !(u < nullptr);
}

template <typename T, typename D> inline
bool operator>=(decltype(nullptr), const unique_ptr<T, D> &u) noexcept
{
    return !(nullptr < u);
}

template <typename T> struct hash;

template <typename T, typename D>
struct hash<unique_ptr<T, D>>: function_type<size_t (unique_ptr<T, D>)>, fast_hash_base {
    size_t operator()(const unique_ptr<T, D> &u) const noexcept
    {
        return hash<typename unique_ptr<T, D>::pointer>()(u.get());
    }
};

} // namespace glglT


#endif // GLGL_UNIQUE_PTR_H
