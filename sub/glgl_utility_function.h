#ifndef GLGL_UTILITY_FUNCTION_H
#define GLGL_UTILITY_FUNCTION_H

#include <new>
#include "glgl_type_category.h"

namespace glglT {

template <typename T> inline
T* addressof(T &t) noexcept
{
    return reinterpret_cast<T *>(&const_cast<char &>
                                 (reinterpret_cast<const volatile char&>
                                  (t)));
}

template <typename T> constexpr
auto move(T &&t) noexcept -> typename remove_reference<T>::type &&
{
    return static_cast<typename remove_reference<T>::type &&>(t);
}

template <typename T> constexpr
T &&forward(typename remove_reference<T>::type &t) noexcept
{
    return static_cast<T &&>(t);
}

template <typename T> constexpr
T &&forward(typename remove_reference<T>::type &&t) noexcept
{
    return static_cast<T &&>(t);
}

template <typename T> inline
void swap(T &t1, T &t2)
    noexcept(noexcept(t1 = glglT::move(t2)) &&
             noexcept(new (&t1) T (glglT::move(t2))))
{
    T t3 = glglT::move(t1);
    t1 = glglT::move(t2);
    t2 = glglT::move(t3);
}

template <typename T, size_t N>
void swap(T (& a1)[N], T (& a2)[N]) noexcept(noexcept(swap(*a1, *a2)));

template <typename T, size_t N>
class array_swapper {
    friend void swap<T, N>(T (& a1)[N], T (& a2)[N])
        noexcept(noexcept(swap(*a1, *a2)));

    template <size_t I>
    static void swap(T *a1, T *a2, false_type) noexcept(noexcept(swap(*a1, *a2)))
    {
        using glglT::swap;
        swap(a1[I], a2[I]);
        array_swapper::swap<I + 1>(a1, a2, integral_constant<bool, I + 1 == N>{});
    }

    template <size_t> static void swap(T *, T *, true_type) noexcept {}
};

template <typename T, size_t N> inline
void swap(T (& a1)[N], T (& a2)[N]) noexcept(noexcept(swap(*a1, *a2)))
{
    array_swapper<T, N>::swap<0>(a1, a2, integral_constant<bool, 0 == N>{});
}

template <typename T>
typename add_rvalue_reference<T>::type declval() noexcept;

} // namespace glglT


#endif // GLGL_UTILITY_FUNCTION_H
