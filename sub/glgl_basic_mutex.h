#ifndef GLGL_BASIC_MUTEX_H
#define GLGL_BASIC_MUTEX_H

#include <chrono>
#include <system_error>
#include "glgl_utility_function.h"

namespace glglT {

struct defer_lock_t { };
struct try_to_lock_t { };
struct adopt_lock_t { };

constexpr defer_lock_t defer_lock{};
constexpr try_to_lock_t try_to_lock{};
constexpr adopt_lock_t adopt_lock{};

template <typename Mutex>
class lock_guard {
    Mutex *p;
public:
    typedef Mutex mutex_type;

    explicit lock_guard(Mutex &m): p(glglT::addressof(m)) { p->lock(); }
    lock_guard(Mutex &m, adopt_lock_t): p(glglT::addressof(m)) {}
    lock_guard(const lock_guard &) = delete;

    ~lock_guard() { p->unlock(); }
};

template <typename Mutex, typename Lock>
class lock_base {
protected:
    Mutex *p;
    bool own;
public:
    typedef Mutex mutex_type;

    constexpr lock_base() noexcept: p(nullptr), own(false) {}
    lock_base(lock_base &&o) noexcept: p(o.p), own(o.own)
    {
        o.p = nullptr;
        o.own = false;
    }

    explicit lock_base(Mutex &m): p(glglT::addressof(m))
    {
        reinterpret_cast<Lock *>(this)->lock_aux();
        own = true;
    }

    lock_base(Mutex &m, defer_lock_t) noexcept: p(glglT::addressof(m)), own(false) {}
    lock_base(Mutex &m, adopt_lock_t) noexcept: p(glglT::addressof(m)), own(true) {}
    lock_base(Mutex &m, try_to_lock_t):
        p(glglT::addressof(m)), own(reinterpret_cast<Lock *>(this)->try_lock_aux()) {}

    template <typename R, typename P>
    lock_base(Mutex &m, const std::chrono::duration<R, P> &d):
        p(glglT::addressof(m)), own(reinterpret_cast<Lock *>(this)->try_lock_for_aux(d)) {}

    template <typename C, typename D>
    lock_base(Mutex &m, const std::chrono::time_point<C, D> &t):
        p(glglT::addressof(m)), own(reinterpret_cast<Lock *>(this)->try_lock_until_aux(t)) {}

    ~lock_base() { if (p && own) reinterpret_cast<Lock *>(this)->unlock_aux(); }

    lock_base &operator=(lock_base &&o)
    {
        swap(o);
        return *this;
    }

    void lock()
    {
        this->check();
        reinterpret_cast<Lock *>(this)->lock_aux();
        own = true;
    }

    bool try_lock()
    {
        this->check();
        own = reinterpret_cast<Lock *>(this)->try_lock_aux();
        return own;
    }

    template <typename R, typename P>
    bool try_lock_for(const std::chrono::duration<R, P> &d)
    {
        this->check();
        own = reinterpret_cast<Lock *>(this)->try_lock_for_aux(d);
        return own;
    }

    template <typename C, typename D>
    bool try_lock_until(const std::chrono::time_point<C, D> &t)
    {
        this->check();
        own = reinterpret_cast<Lock *>(this)->try_lock_until_aux(t);
        return own;
    }

    void unlock()
    {
        if (p == nullptr)
            throw std::system_error(int(std::errc::operation_not_permitted),
                                    std::generic_category(), "no associated mutex");
        if (own) {
            own = false;
            reinterpret_cast<Lock *>(this)->unlock_aux();
        }
    }

    void swap(lock_base &o) noexcept
    {
        glglT::swap(p, o.p);
        glglT::swap(own, o.own);
    }

    Mutex *release() noexcept
    {
        Mutex *r = p;
        p = nullptr;
        own = false;
        return r;
    }

    Mutex *mutex() const noexcept { return p; }
    bool own_lock() const noexcept { return own; }
    operator bool() const noexcept { return own; }
private:
    void check()
    {
        if (p == nullptr)
            throw std::system_error(int(std::errc::operation_not_permitted),
                                    std::generic_category(), "no associated mutex");
        if (own)
            throw std::system_error(int(std::errc::resource_deadlock_would_occur),
                                    std::generic_category(), "mutex is already locked");
    }
};

template <typename Mutex>
class unique_lock : public lock_base<Mutex, unique_lock<Mutex>> {
    friend class lock_base<Mutex, unique_lock>;
public:
    typedef Mutex mutex_type;

    constexpr unique_lock() noexcept: lock_base<Mutex, unique_lock>() {}
    unique_lock(unique_lock &&o) noexcept: lock_base<Mutex, unique_lock>(glglT::move(o)) {}
    explicit unique_lock(Mutex &m): lock_base<Mutex, unique_lock>(m) {}
    unique_lock(Mutex &m, defer_lock_t) noexcept: lock_base<Mutex, unique_lock>(m, defer_lock) {}
    unique_lock(Mutex &m, adopt_lock_t) noexcept: lock_base<Mutex, unique_lock>(m, adopt_lock) {}
    unique_lock(Mutex &m, try_to_lock_t): lock_base<Mutex, unique_lock>(m, try_to_lock) {}

    template <typename R, typename P>
    unique_lock(Mutex &m, const std::chrono::duration<R, P> &d):
        lock_base<Mutex, unique_lock>(m, d) {}

    template <typename C, typename D>
    unique_lock(Mutex &m, const std::chrono::time_point<C, D> &t):
        lock_base<Mutex, unique_lock>(m, t) {}

    unique_lock &operator=(unique_lock &&o)
    {
        lock_base<Mutex, unique_lock>::operator=(glglT::move(o));
        return *this;
    }
private:
    void lock_aux() { this->p->lock(); }
    void unlock_aux() { this->p->unlock(); }
    bool try_lock_aux() { return this->p->try_lock(); }
    template <typename R, typename P>
    bool try_lock_for_aux(const std::chrono::duration<R, P> &d)
    {
        return this->p->try_lock_for(d);
    }

    template <typename C, typename D>
    bool try_lock_until_aux(const std::chrono::time_point<C, D> &t)
    {
        return this->p->try_lock_until(t);
    }
};

template <typename Mutex> inline
void swap(unique_lock<Mutex> &u1, unique_lock<Mutex> &u2) noexcept
{
    u1.swap(u2);
}

template <typename Mutex>
class shared_lock : public lock_base<Mutex, shared_lock<Mutex>> {
    friend class lock_base<Mutex, shared_lock>;
public:
    typedef Mutex mutex_type;

    constexpr shared_lock() noexcept: lock_base<Mutex, shared_lock>() {}
    shared_lock(shared_lock &&o) noexcept: lock_base<Mutex, shared_lock>(glglT::move(o)) {}
    explicit shared_lock(Mutex &m): lock_base<Mutex, shared_lock>(m) {}
    shared_lock(Mutex &m, defer_lock_t) noexcept: lock_base<Mutex, shared_lock>(m, defer_lock) {}
    shared_lock(Mutex &m, adopt_lock_t) noexcept: lock_base<Mutex, shared_lock>(m, adopt_lock) {}
    shared_lock(Mutex &m, try_to_lock_t): lock_base<Mutex, shared_lock>(m, try_to_lock) {}

    template <typename R, typename P>
    shared_lock(Mutex &m, const std::chrono::duration<R, P> &d):
        lock_base<Mutex, shared_lock>(m, d) {}

    template <typename C, typename D>
    shared_lock(Mutex &m, const std::chrono::time_point<C, D> &t):
        lock_base<Mutex, shared_lock>(m, t) {}

    shared_lock &operator=(shared_lock &&o)
    {
        lock_base<Mutex, shared_lock>::operator=(glglT::move(o));
        return *this;
    }
private:
    void lock_aux() { this->p->lock_shared(); }
    void unlock_aux() { this->p->unlock_shared(); }
    bool try_lock_aux() { return this->p->try_lock_shared(); }
    template <typename R, typename P>
    bool try_lock_for_aux(const std::chrono::duration<R, P> &d)
    {
        return this->p->try_lock_shared_for(d);
    }

    template <typename C, typename D>
    bool try_lock_until_aux(const std::chrono::time_point<C, D> &t)
    {
        return this->p->try_lock_shared_until(t);
    }
};

template <typename Mutex> inline
void swap(shared_lock<Mutex> &s1, shared_lock<Mutex> &s2) noexcept
{
    s1.swap(s2);
}

} // namespace glglT

#endif // GLGL_BASIC_MUTEX_H
