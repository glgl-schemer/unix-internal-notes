﻿#ifndef GLGL_FUNCTION_OBJECT_H
#define GLGL_FUNCTION_OBJECT_H

#include "glgl_shared_ptr.h"
#include "glgl_base_algo.h"
#include "glgl_hash.h"

namespace glglT {

template <typename T>
struct less :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 < t2)) { return t1 < t2; }
};

template <typename T>
struct less_equal :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 <= t2)) { return t1 <= t2; }
};

template <typename T>
struct greater :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 > t2)) { return t1 > t2; }
};

template <typename T>
struct greater_equal :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 >= t2)) { return t1 >= t2; }
};

template <typename T>
struct equal_to :function_type<bool (T, T)> {
     bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 == t2)) { return t1 == t2; }
};

template <typename T>
struct not_equal_to :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 != t2)) { return t1 != t2; }
};

template <typename T>
struct plus :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 + t2)) { return t1 + t2; }
};

template <typename T>
struct minus :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 - t2)) { return t1 - t2; }
};

template <typename T>
struct multiplies :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 * t2)) { return t1 * t2; }
};

template <typename T>
struct divides :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 / t2)) { return t1 / t2; }
};

template <typename T>
struct modulus :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 % t2)) { return t1 % t2; }
};

template <typename T>
struct negate :function_type<T (T)> {
    T operator()(const T &t)
        const noexcept(noexcept(-t)) { return -t; }
};

template <typename T>
struct logical_and :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 && t2)) { return t1 && t2; }
};

template <typename T>
struct logical_or :function_type<bool (T, T)> {
    bool operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 || t2)) { return t1 || t2; }
};

template <typename T>
struct logical_not :function_type<bool (T)> {
    bool operator()(const T &t)
        const noexcept(noexcept(!t)) { return !t; }
};

template <typename T>
struct bit_and :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 & t2)) { return t1 & t2; }
};

template <typename T>
struct bit_or :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 | t2)) { return t1 | t2; }
};

template <typename T>
struct bit_xor :function_type<T (T, T)> {
    T operator()(const T &t1, const T &t2)
        const noexcept(noexcept(t1 ^ t2)) { return t1 ^ t2; }
};

template <typename Iter,
          typename Pred = equal_to<typename iterator_traits<Iter>::value_type>>
class default_searcher {
    Iter b, e;
    Pred p;
public:
    default_searcher(Iter first, Iter last, Pred pred = Pred()):
        b(first), e(glglT::real_last(first, last)), p(pred) {}

    template <typename It>
    pair<It, It> operator()(It first, It last) const
    {
        const auto d = glglT::distance(b, e);
        auto t = glglT::search(first, last, b, e, p);
        return glglT::make_pair(t, glglT::next(t, d));
    }
};

template <typename Iter,
          typename Pred = equal_to<typename iterator_traits<Iter>::value_type>>
class knuth_morris_pratt_searcher {
    shared_ptr<pair<Iter, size_t>> t;
    Iter b, e;
    Pred p;
public:
    knuth_morris_pratt_searcher(Iter first, Iter last, Pred pred = Pred()):
        t(nullptr), b(first), e(glglT::real_last(first, last)), p(pred)
    {
        auto tmp = first;
        if (first != last) {
            t.reset(new pair<Iter, size_t>[glglT::distance(first, last)],
                    [](pair<Iter, size_t> *p){ delete[] p; });
            size_t i = 0, j = 1;
            t.get()[0] = glglT::make_pair(first, i);
            for (Iter iter = first; ++iter != last; ++j) {
                bool b;
                if (!(b = pred(*first, *iter))) {
                    t.get()[j] = glglT::make_pair(first, i);
                    auto p = t.get()[i];
                    while (!(b = pred(*(p.first), *iter)) &&
                           p.second != t.get()[p.second].second)
                        p = t.get()[p.second];
                    first = p.first;
                    i = p.second + b;
                } else
                    t.get()[j] = t.get()[i++];
                if (b)
                    ++first;
            }
        }
    }

    template <typename It>
    pair<It, It> operator()(It first, It last) const
    {
        last = glglT::real_last(first, last);
        auto start = first;
        size_t j = 0;
        for (auto iter = b; iter != e; ++first) {
            if (first == last)
                return glglT::make_pair(first, first);
            bool b;
            if (!(b = this->p(*first, *iter))) {
                auto p = t.get()[j];
                while (!(b = this->p(*(p.first), *first)) &&
                       p.second != t.get()[p.second].second)
                    p = t.get()[p.second];
                iter = p.first;
                start = glglT::next(start, j - p.second + !b);
                j = p.second;
            }
            if (b)
                ++iter, ++j;
        }
        return glglT::make_pair(start, first);
    }
};


template <typename Iter,
          typename Hash = hash<typename iterator_traits<Iter>::value_type>,
          typename Pred = equal_to<typename iterator_traits<Iter>::value_type>>
class boyer_moore_searcher {
    shared_ptr<pair<Iter, size_t>> t;
    size_t sz;
    Pred p;
    // map
};

} // namespace glglT

#endif // GLGL_FUNCTION_OBJECT_H
