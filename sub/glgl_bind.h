#ifndef GLGL_BIND_H
#define GLGL_BIND_H

#include "glgl_tuple_im.h"
#include "glgl_functional_base.h"

namespace glglT {

template <typename> class reference_wrapper;

template <int> struct place_holder {};

namespace placeholders {

constexpr place_holder<1> _1 = place_holder<1>();
constexpr place_holder<2> _2 = place_holder<2>();
constexpr place_holder<3> _3 = place_holder<3>();
constexpr place_holder<4> _4 = place_holder<4>();
constexpr place_holder<5> _5 = place_holder<5>();
constexpr place_holder<6> _6 = place_holder<6>();
constexpr place_holder<7> _7 = place_holder<7>();
constexpr place_holder<8> _8 = place_holder<8>();
constexpr place_holder<9> _9 = place_holder<9>();
constexpr place_holder<10> _10 = place_holder<10>();
constexpr place_holder<11> _11 = place_holder<11>();
constexpr place_holder<12> _12 = place_holder<12>();
constexpr place_holder<13> _13 = place_holder<13>();
constexpr place_holder<14> _14 = place_holder<14>();
constexpr place_holder<15> _15 = place_holder<15>();
constexpr place_holder<16> _16 = place_holder<16>();
constexpr place_holder<17> _17 = place_holder<17>();
constexpr place_holder<18> _18 = place_holder<18>();
constexpr place_holder<19> _19 = place_holder<19>();
constexpr place_holder<20> _20 = place_holder<20>();
constexpr place_holder<21> _21 = place_holder<21>();
constexpr place_holder<22> _22 = place_holder<22>();
constexpr place_holder<23> _23 = place_holder<23>();
constexpr place_holder<24> _24 = place_holder<24>();
constexpr place_holder<25> _25 = place_holder<25>();
constexpr place_holder<26> _26 = place_holder<26>();
constexpr place_holder<27> _27 = place_holder<27>();
constexpr place_holder<28> _28 = place_holder<28>();
constexpr place_holder<29> _29 = place_holder<29>();

} // namespace placeholders

template <typename> struct is_placeholder_helper:
    integral_constant<int, 0>
{};

template <int I> struct is_placeholder_helper<place_holder<I>>:
    integral_constant<int, I>
{};

template <typename T>
struct is_placeholder: is_placeholder_helper<
    typename remove_cv<typename remove_reference<T>::type>::type
> {};

namespace BIND_HELPER {

template <int I1, int I2>
struct min_int_t: integral_constant<int, (I1 < I2 ? I1 : I2)> {};

template <int I>
struct min_int_t<0, I>: integral_constant<int, I> {};

template <int I>
struct min_int_t<I, 0>: integral_constant<int, I> {};

template<>
struct min_int_t<0, 0>: integral_constant<int, 0> {};

template <typename T, typename... Args> struct min_place_holder_t;

template <typename T>
struct min_place_holder_t<T>: is_placeholder<T> {};

template <typename T, typename... Args>
struct min_place_holder_t:
    min_int_t<is_placeholder<T>::value, min_place_holder_t<Args...>::value>
{};

template <int MIN, int I>
struct replace_place_holder {
    template <typename... BArgs> static
    auto foo(tuple<BArgs&&...> bind_args, place_holder<I>) noexcept
        -> decltype(get<I - MIN>(glglT::move(bind_args)))
    {
        return glglT::get<I - MIN>(glglT::move(bind_args));
    }
};

template <int MIN>
struct replace_place_holder<MIN, 0> {
    template <typename T, typename BArgs> static
    T&& foo(BArgs&& bind_args, T &&t) noexcept
    {
        return glglT::forward<T>(t);
    }

    template <typename T, typename BArgs> static
    T& foo(BArgs&& bind_args, reference_wrapper<T> t) noexcept
    {
        return (T &)t;
    }
};

template <typename... BArgs, typename... Args, size_t... Is> constexpr
auto make_args(tuple<BArgs&&...> bind_args,
               tuple<Args...>& args, index_holder<Is...>) noexcept
    -> decltype(glglT::forward_as_tuple
                (replace_place_holder
                 <min_place_holder_t<Args...>::value,
                    is_placeholder<decltype(get<Is>(args))>::value
                 >::foo(glglT::move(bind_args), get<Is>(args))...));

template <typename F, typename... BArgs,
          typename... Args, size_t... Is> inline
auto call_helper(F &&f, tuple<BArgs&&...> bind_args,
                 tuple<Args...>& args, index_holder<Is...> ih)
    noexcept(noexcept(glglT::invoke
                      (glglT::forward<F>(f),
                       glglT::get<Is>(glglT::move(BIND_HELPER::make_args(bind_args, args, ih)))...)))
    -> decltype(glglT::invoke
                (glglT::forward<F>(f),
                 glglT::get<Is>(glglT::move(BIND_HELPER::make_args(bind_args, args, ih)))...))
{
    constexpr int MIN = min_place_holder_t<Args...>::value;
    auto a = glglT::forward_as_tuple(
        replace_place_holder<
            MIN, is_placeholder<decltype(glglT::get<Is>(args))>::value
        >::foo(glglT::move(bind_args), glglT::get<Is>(args))...);
    return glglT::invoke(glglT::forward<F>(f), glglT::get<Is>(glglT::move(a))...);
}

} // namespace BIND_HELPER

template <typename F, typename... Args>
class bind_class: public may_result_type<function_type<F>> {
    tuple<Args...> tuple_args;
    F f;
public:
    bind_class(F c, Args... args) noexcept(
        noexcept(F(glglT::move(c))) && noexcept(tuple<Args...>(glglT::move(args)...))):
            f(glglT::move(c)), tuple_args(glglT::move(args)...) {}

    template <typename... Os>
    auto operator()(Os&&... os) noexcept(noexcept(
                BIND_HELPER::call_helper(
                    f, glglT::forward_as_tuple(glglT::forward<Os>(os)...),
                    tuple_args, decltype(make_single_index(tuple_args))())))
        -> decltype(BIND_HELPER::call_helper
                    (f, glglT::forward_as_tuple(glglT::forward<Os>(os)...),
                     tuple_args, decltype(make_single_index(tuple_args))()))
    {
        return BIND_HELPER::call_helper(
                f, glglT::forward_as_tuple(glglT::forward<Os>(os)...),
                tuple_args, decltype(make_single_index(tuple_args))());
    }
};

template <typename F, typename... Args> inline
bind_class<F, Args...> bind(F f, Args... args)
    noexcept(noexcept(bind_class<F, Args...>(glglT::move(f), glglT::move(args)...)))
{
    return bind_class<F, Args...>(glglT::move(f), glglT::move(args)...);
}

template <typename T>
struct is_bind_helper: false_type {};

template <typename... Ts>
struct is_bind_helper<bind_class<Ts...>>: true_type {};

template <typename T>
struct is_bind_expression: is_bind_helper<
    typename remove_cv<
        typename remove_reference<T>::type
    >::type
> {};

} // namespace glglT


#endif // GLGL_BIND_H
