#ifndef GLGL_TYPE_HELPER_H
#define GLGL_TYPE_HELPER_H

#include <type_traits>

namespace glglT {

using std::integral_constant;

using std::true_type;

using std::false_type;

} // namespace glglT


#endif // GLGL_TYPE_HELPER_H
