#ifndef GLGL_STREAM_ITERATOR_H
#define GLGL_STREAM_ITERATOR_H

#include <string>
#include "glgl_basic_iterator.h"

namespace glglT {

template <typename, typename, typename, typename> class istream_iterator;

template <typename T, typename C, typename Traits, typename Diff> inline
bool operator==(const istream_iterator<T, C, Traits, Diff> &,
                const istream_iterator<T, C, Traits, Diff> &) noexcept;

template <typename T, typename C = char,
          typename Traits = std::char_traits<C>, typename Diff = ptrdiff_t>
class istream_iterator:
    public iterator<input_iterator_tag, T, Diff, const T *, const T &> {
    friend
    bool operator==<T, C, Traits, Diff>(const istream_iterator &,
                                        const istream_iterator &) noexcept;

    T value;
    std::basic_istream<C, Traits> *sptr;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef std::basic_istream<C, Traits> istream_type;

    constexpr istream_iterator() noexcept(noexcept(T())): sptr(nullptr) {}
    istream_iterator(istream_type &is): sptr(&is) { is >> value; }
    istream_iterator(const istream_iterator &ii)
        noexcept(noexcept(T(ii.value))) = default;

    const T &operator*() const { return value; }
    const T *operator->() const { return &value; }
    istream_iterator &operator++()
    {
        if (sptr && *sptr) *sptr >> value;
        return *this;
    }

    istream_iterator operator++(int)
    {
        istream_iterator ii(*this);
        if (sptr && *sptr) *sptr >> value;
        return ii;
    }
};

template <typename T, typename C, typename Traits, typename Diff> inline
bool operator==(const istream_iterator<T, C, Traits, Diff> &ii1,
                const istream_iterator<T, C, Traits, Diff> &ii2) noexcept
{
    return ii1.sptr == ii2.sptr ||
        ( (ii1.sptr == nullptr || !*(ii1.sptr)) &&
          (ii2.sptr == nullptr || !*(ii2.sptr) ) );
}

template <typename T, typename C, typename Traits, typename Diff> inline
bool operator!=(const istream_iterator<T, C, Traits, Diff> &ii1,
                const istream_iterator<T, C, Traits, Diff> &ii2) noexcept
{
    return !(ii1 == ii2);
}

template <typename T, typename C = char, typename Traits = std::char_traits<C>>
class ostream_iterator:
    public iterator<output_iterator_tag, void, void, void, void> {
    const C *s;
    std::basic_ostream<C, Traits> *sptr;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef std::basic_ostream<C, Traits> ostream_type;

    ostream_iterator(ostream_type &os, const C *d): s(d), sptr(&os) {}
    ostream_iterator(ostream_type &os): s(""), sptr(&os) {}

    ostream_iterator &operator=(const T &t)
    {
        *sptr << t << s;
        return *this;
    }

    ostream_iterator &operator*() noexcept { return *this; }
    ostream_iterator &operator++() noexcept { return *this; }
    ostream_iterator &operator++(int) noexcept { return *this; }
};

template <typename, typename> class istreambuf_iterator;

template <typename C>
struct hold_char_t {
    C c;
    constexpr hold_char_t(C ch): c(ch) {}
    C operator*() const noexcept { return c; }
    C *operator->() const noexcept { return &c; }
};

template <typename C, typename Traits = std::char_traits<C>>
class istreambuf_iterator:
    public iterator<input_iterator_tag, C, typename Traits::off_type, hold_char_t<C>, C> {
    std::basic_streambuf<C, Traits> *buf;
    C c;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef typename Traits::int_type int_type;
    typedef std::basic_streambuf<C, Traits> streambuf_type;
    typedef std::basic_istream<C, Traits> istream_type;

    constexpr istreambuf_iterator() noexcept: buf(nullptr) {}
    istreambuf_iterator(std::basic_istream<C, Traits> &is) noexcept:
        buf(is.rdbuf()), c(buf->sgetc()) {}
    istreambuf_iterator(std::basic_streambuf<C, Traits> *s) noexcept: buf(s) {}
    istreambuf_iterator(const istreambuf_iterator &) noexcept = default;

    C operator*() const { return c; }
    hold_char_t<C> operator->() const { return c; }

    istreambuf_iterator &operator++()
    {
        buf->sbumpc();
        c = buf->sgetc();
        return *this;
    }

    hold_char_t<C> operator++(int)
    {
        C tmp = buf->sbumpc();
        c = buf->sgetc();
        return tmp;
    }

    bool equal(const istreambuf_iterator &ii) const
    {
        return (buf == nullptr || buf->sgetc() == Traits::eof()) ==
            (ii.buf == nullptr || ii.buf->sgetc() == Traits::eof());
    }
};

template <typename C, typename Traits> inline
bool operator==(const istreambuf_iterator<C, Traits> &ii1,
                const istreambuf_iterator<C, Traits> &ii2) noexcept
{
    return ii1.equal(ii2);
}

template <typename C, typename Traits> inline
bool operator!=(const istreambuf_iterator<C, Traits> &ii1,
                const istreambuf_iterator<C, Traits> &ii2) noexcept
{
    return !(ii1 == ii2);
}

template <typename C, typename Traits = std::char_traits<C>>
class ostreambuf_iterator:
    public iterator<output_iterator_tag, C, typename Traits::off_type, C *, C> {
    bool fail;
    std::basic_streambuf<C, Traits> *buf;
public:
    typedef C char_type;
    typedef Traits traits_type;
    typedef std::basic_streambuf<C, Traits> streambuf_type;
    typedef std::basic_ostream<C, Traits> ostream_type;

    ostreambuf_iterator(streambuf_type *s) noexcept:
        fail(s == nullptr), buf(s) {}
    ostreambuf_iterator(ostream_type &os) noexcept:
        fail(false), buf(os.rdbuf()) {}

    ostreambuf_iterator &operator=(C c)
    {
        if (!fail && buf->sputc(c) == Traits::eof())
            fail == true;
        return *this;
    }

    ostreambuf_iterator &operator*() noexcept { return *this; }
    ostreambuf_iterator &operator++() noexcept { return *this; }
    ostreambuf_iterator &operator++(int) noexcept { return *this; }
    bool failed() const noexcept { return fail; }
};

} // namespace glglT

#endif // GLGL_STREAM_ITERATOR_H
