#ifndef GLGL_FUNCTION_REFERENCE_H
#define GLGL_FUNCTION_REFERENCE_H

#include "glgl_functional_base.h"

namespace glglT {

template <typename T>
class reference_wrapper: public function_type<T> {
    T *p;
public:
    typedef T type;
    reference_wrapper(T &t) noexcept: p(glglT::addressof(t)) {}
    reference_wrapper(T &&) = delete;
    reference_wrapper(const reference_wrapper &r) noexcept: p(r.p) {}

    T &get() const noexcept { return *p; }
    operator T & () const noexcept { return *p; }

    template <typename... Args>
    auto operator()(Args&&... args) const
        noexcept(noexcept(glglT::invoke(*p, glglT::forward<Args>(args)...)))
            -> decltype(glglT::invoke(*p, glglT::forward<Args>(args)...))
    {
        return glglT::invoke(*p, glglT::forward<Args>(args)...);
    }
};

template <typename T, bool B>
template <typename ClassT>
auto mem_fn_base<T, B>::operator()(reference_wrapper<ClassT> r) const noexcept
    -> decltype(glglT::declval<ClassT &>().*glglT::declval<T &>())
{
    return r.get().*pmf;
}

template <typename T>
template <typename ClassT, typename... Args>
auto mem_fn_base<T, true>::operator()(reference_wrapper<ClassT> r, Args&&... args) const
    noexcept(noexcept((glglT::declval<ClassT &>().*glglT::declval<T &>())(glglT::forward<Args>(args)...)))
        -> decltype((glglT::declval<ClassT &>().*glglT::declval<T &>())(glglT::forward<Args>(args)...))
{
    return (r.get().*pmf)(glglT::forward<Args>(args)...);
}

template <typename T> inline
reference_wrapper<T> ref(T &t) noexcept
{
    return reference_wrapper<T>(t);
}

template <typename T> inline
reference_wrapper<T> ref(reference_wrapper<T> t) noexcept
{
    return reference_wrapper<T>(t);
}

template <typename T> void ref(T &&) = delete;

template <typename T> inline
reference_wrapper<const T> cref(const T &t) noexcept
{
    return reference_wrapper<const T>(t);
}

template <typename T> inline
reference_wrapper<const T> cref(reference_wrapper<const T> t) noexcept
{
    return reference_wrapper<const T>(t);
}

template <typename T> void cref(const T &&) = delete;

} // namespace glglT


#endif // GLGL_FUNCTION_REFERENCE_H
