﻿#ifndef GLGL_TUPLE_IM_H
#define GLGL_TUPLE_IM_H

#include "glgl_memory_tool.h"

namespace glglT {

template <typename, typename> struct pair;

template <typename, typename...> struct tuple_helper;

template <typename T1, typename T2>
struct tuple_helper<T1, T2> {
    T1 t1;
    T2 t2;

    tuple_helper() = default;

    tuple_helper(T1 o1, T2 o2)
        noexcept(noexcept(T1(glglT::forward<T1>(o1))) &&
                 noexcept(T2(glglT::forward<T2>(o2)))):
            t1(glglT::forward<T1>(o1)), t2(glglT::forward<T2>(o2)) {}

    template <typename Other1, typename Other2>
    tuple_helper(const pair<Other1, Other2> &);
    template <typename Other1, typename Other2>
    tuple_helper(pair<Other1, Other2> &&);

    template <typename Other1, typename Other2>
    tuple_helper &operator=(const pair<Other1, Other2> &);
    template <typename Other1, typename Other2>
    tuple_helper &operator=(pair<Other1, Other2> &&);

    tuple_helper(const tuple_helper &th)
        noexcept(noexcept(T1(th.t1)) && noexcept(T2(th.t2))):
            t1(th.t1), t2(th.t2) {}

    tuple_helper(tuple_helper &&th)
        noexcept(noexcept(T1(glglT::forward<T1>(th.t1))) &&
                 noexcept(T2(glglT::forward<T2>(th.t2)))):
            t1(glglT::forward<T1>(th.t1)), t2(glglT::forward<T2>(th.t2)) {}

    template <typename Other1, typename Other2>
    tuple_helper(const tuple_helper<Other1, Other2> &th)
        noexcept(noexcept(T1(th.t1)) && noexcept(T2(th.t2))):
            t1(th.t1), t2(th.t2) {}

    template <typename Other1, typename Other2>
    tuple_helper(tuple_helper<Other1, Other2> &&th)
        noexcept(noexcept(T1(glglT::forward<Other1>(th.t1))) &&
                 noexcept(T2(glglT::forward<Other2>(th.t2)))):
            t1(glglT::forward<Other1>(th.t1)), t2(glglT::forward<Other2>(th.t2)) {}

    tuple_helper &operator=(const tuple_helper &th)
        noexcept(noexcept(t1 = th.t1) && noexcept(th = th.t2))
    {
        t1 = th.t1;
        t2 = th.t2;
        return *this;
    }

    tuple_helper &operator=(tuple_helper &&th)
        noexcept(noexcept(t1 = glglT::forward<T1>(th.t1)) &&
                 noexcept(t2 = glglT::forward<T2>(th.t2)))
    {
        t1 = glglT::forward<T1>(th.t1);
        t2 = glglT::forward<T2>(th.t2);
        return *this;
    }

    template <typename Other1, typename Other2>
    tuple_helper &operator=(const tuple_helper<Other1, Other2> &th)
        noexcept(noexcept(t1 = th.t1) && noexcept(t2 = th.t2))
    {
        t1 = th.t1;
        t2 = th.t2;
        return *this;
    }

    template <typename Other1, typename Other2>
    tuple_helper &operator=(tuple_helper<Other1, Other2> &&th)
        noexcept(noexcept(t1 = glglT::forward<Other1>(th.t1)) &&
                 noexcept(t2 = glglT::forward<Other2>(th.t2)))
    {
        t1 = glglT::forward<Other1>(th.t1);
        t2 = glglT::forward<Other2>(th.t2);
        return *this;
    }

    void swap(tuple_helper &th) noexcept(
        noexcept(swap(t1, th.t1)) && noexcept(swap(t2, th.t2)))
    {
        using glglT::swap;
        swap(t1, th.t1);
        swap(t2, th.t2);
    }
};

template <typename T, typename... OtherArgs>
struct tuple_helper {
    T t;
    tuple_helper<OtherArgs...> ot;

    tuple_helper() = default;

    tuple_helper(T arg, OtherArgs... args) noexcept(
        noexcept(T(glglT::forward<T>(arg))) &&
        noexcept(tuple_helper<OtherArgs...>(glglT::forward<OtherArgs>(args)...))
    ):  t(glglT::forward<T>(arg)), ot(glglT::forward<OtherArgs>(args)...) {}

    template <typename Arg, typename... Args>
    tuple_helper(const tuple_helper<Arg, Args...> &th) noexcept(
        noexcept(T(th.t)) &&
        noexcept(tuple_helper<OtherArgs...>(th.ot))
    ):  t(th.t), ot(th.ot) {}

    template <typename Arg, typename... Args>
    tuple_helper(tuple_helper<Arg, Args...> &&th) noexcept(
        noexcept(T(glglT::forward<Arg>(th.t))) &&
        noexcept(tuple_helper<OtherArgs...>(glglT::move(th.ot)))
    ):  t(glglT::forward<Arg>(th.t)), ot(glglT::move(th.ot)) {}


    tuple_helper(const tuple_helper &th) noexcept(
        noexcept(T(th.t)) &&
        noexcept(tuple_helper<OtherArgs...>(th.ot))
    ):  t(th.t), ot(th.ot) {}

    tuple_helper(tuple_helper &&th) noexcept(
        noexcept(T(glglT::forward<T>(th.t))) &&
        noexcept(tuple_helper<OtherArgs...>(glglT::move(th.ot)))
    ):  t(glglT::forward<T>(th.t)), ot(glglT::move(th.ot)) {}

    tuple_helper &operator=(const tuple_helper &th) noexcept(
        noexcept(t = th.t) &&
        noexcept(ot = th.ot))
    {
        t = th.t;
        ot = th.ot;
        return *this;
    }

    tuple_helper &operator=(tuple_helper &&th) noexcept(
        noexcept(t = glglT::forward<T>(th.t)) &&
        noexcept(ot = glglT::move(th.ot)))
    {
        t = glglT::forward<T>(th.t);
        ot = glglT::move(th.ot);
        return *this;
    }

    template <typename Other, typename... Others>
    tuple_helper &operator=(const tuple_helper<Other, Others...> &th)
        noexcept(noexcept(t = th.t) &&
                 noexcept(ot = th.ot))
    {
        t = th.t;
        ot = th.ot;
        return *this;
    }

    template <typename Other, typename... Others>
    tuple_helper &operator=(tuple_helper<Other, Others...> &&th)
        noexcept(noexcept(t = glglT::forward<Other>(th.t)) &&
                 noexcept(ot = glglT::move(th.ot)))
    {
        t = glglT::forward<Other>(th.t);
        ot = glglT::move(th.ot);
        return *this;
    }

    void swap(tuple_helper &th) noexcept(
        noexcept(swap(t, th.t)) &&
        noexcept(ot.swap(th.ot)))
    {
        using glglT::swap;
        swap(t, th.t);
        ot.swap(th.ot);
    }
};

template <typename T>
struct tuple_helper<T> {
    T t;

    tuple_helper() = default;

    explicit
    tuple_helper(T o) noexcept(noexcept(T(glglT::forward<T>(o)))): t(glglT::forward<T>(o)) {}

    template <typename Other>
    tuple_helper(const tuple_helper<Other> &th)
        noexcept(noexcept(T(th.t))): t(th.t) {}

    template <typename Other>
    tuple_helper(tuple_helper<Other> &&th)
        noexcept(noexcept(T(glglT::forward<Other>(th.t)))): t(glglT::forward<Other>(th.t)) {}

    tuple_helper(const tuple_helper &th)
        noexcept(noexcept(T(th.t))): t(th.t) {}
    tuple_helper(tuple_helper &&th)
        noexcept(noexcept(T(glglT::forward<T>(th.t)))): t(glglT::forward<T>(th.t)) {}

    tuple_helper &operator=(const tuple_helper &th)
        noexcept(noexcept(t = th.t))
    {
        t = th.t;
        return *this;
    }

    tuple_helper &operator=(tuple_helper &&th)
        noexcept(noexcept(t = glglT::forward<T>(th.t)))
    {
        t = glglT::forward<T>(th.t);
        return *this;
    }

    template <typename Other>
    tuple_helper &operator=(const tuple_helper<Other> &th)
        noexcept(noexcept(t = th.t))
    {
        t = th.t;
        return *this;
    }

    template <typename Other>
    tuple_helper &operator=(tuple_helper<Other> &&th)
        noexcept(noexcept(t = glglT::forward<Other>(th.t)))
    {
        t = glglT::forward<Other>(th.t);
        return *this;
    }

    void swap(tuple_helper &th) noexcept(noexcept(swap(t, th.t)))
    {
        using glglT::swap;
        swap(t, th.t);
    }
};

template <typename... Args>
struct tuple {
    tuple_helper<Args...> data;

    tuple() = default;

    explicit
    tuple(Args... args) noexcept(noexcept(
        tuple_helper<Args...>(glglT::forward<Args>(args)...))):
        data(glglT::forward<Args>(args)...) {}

    template <typename... OtherArgs>
    tuple(const tuple<OtherArgs...> &t) noexcept(noexcept(
        tuple_helper<Args...>(t.data))):
        data(t.data) {}

    template <typename... OtherArgs>
    tuple(tuple<OtherArgs...> &&t) noexcept(noexcept(
        tuple_helper<Args...>(glglT::move(t.data)))):
        data(glglT::move(t.data)) {}

    template <typename Other1, typename Other2>
    tuple(const pair<Other1, Other2> &p):
        data(p) {}

    template <typename Other1, typename Other2>
    tuple(pair<Other1, Other2> &&p):
        data(glglT::move(p)) {}

    tuple(const tuple &) = default;
    tuple(tuple &&) = default;
    tuple &operator=(const tuple &) = default;
    tuple &operator=(tuple &&) = default;

    template <typename Other1, typename Other2>
    tuple &operator=(const pair<Other1, Other2> &p)
        noexcept(noexcept(data = p))
    {
        data = p;
        return *this;
    }

    template <typename Other1, typename Other2>
    tuple &operator=(pair<Other1, Other2> &&p)
        noexcept(noexcept(data = glglT::move(p)))
    {
        data = glglT::move(p);
        return *this;
    }

    template <typename... OtherArgs>
    tuple &operator=(const tuple<OtherArgs...> &t)
        noexcept(noexcept(data = t.data))
    {
        data = t.data;
        return *this;
    }

    template <typename... OtherArgs>
    tuple &operator=(tuple<OtherArgs...> &&t)
        noexcept(noexcept(data = glglT::move(t.data)))
    {
        data = glglT::move(t.data);
        return *this;
    }

    void swap(tuple &t) noexcept(noexcept(data.swap(t.data)))
    {
        data.swap(t.data);
    }
};

template <>
struct tuple<> {
    constexpr tuple() noexcept = default;
    void swap(tuple &) {}
    template <typename T>
    tuple &operator=(T &&) noexcept { return *this; }
    tuple &operator*() noexcept { return *this; }
    tuple &operator++() noexcept { return *this; }
    tuple &operator++(int) noexcept { return *this; }
};

namespace {

tuple<> ignore = tuple<>();

}

template <typename... Args> inline
void swap(tuple<Args...> &t1, tuple<Args...> &t2)
    noexcept(noexcept(t1.swap(t2)))
{
    t1.swap(t2);
}

template <typename T>
using remove_reference_cv_t =
    typename remove_reference<typename remove_cv<T>::type>::type;

template <typename... Args> inline
tuple<remove_reference_cv_t<Args>...> make_tuple(Args&&... args) noexcept(
    noexcept(tuple<remove_reference_cv_t<Args>...>(glglT::forward<Args>(args)...)))
{
    return tuple<remove_reference_cv_t<Args>...>(glglT::forward<Args>(args)...);
}

template <typename... Args> inline
tuple<Args &...> tie(Args &... refs) noexcept
{
    return tuple<Args &...>(refs...);
}

template <typename... Args> inline
tuple<Args&&...> forward_as_tuple(Args&&... refs) noexcept
{
    return tuple<Args&&...>(glglT::forward<Args>(refs)...);
}

template <typename> struct tuple_size: integral_constant<size_t, 1> {};

template <typename... Args>
struct tuple_size<tuple<Args...>>:
    integral_constant<size_t, sizeof...(Args)>
{};

template <size_t N, typename T> struct tuple_element;

template <typename T>
struct tuple_element<0, T> {
    typedef T type;
};

template <typename T>
struct tuple_element<0, const T> {
    typedef typename add_const<T>::type type;
};

template <typename T>
struct tuple_element<0, volatile T> {
    typedef typename add_volatile<T>::type type;
};

template <typename T>
struct tuple_element<0, const volatile T> {
    typedef typename add_cv<T>::type type;
};

template <typename T>
struct tuple_element<0, tuple<T>> {
    typedef T type;
};

template <typename T>
struct tuple_element<0, const tuple<T>> {
    typedef typename add_const<T>::type type;
};

template <typename T>
struct tuple_element<0, volatile tuple<T>> {
    typedef typename add_volatile<T>::type type;
};

template <typename T>
struct tuple_element<0, const volatile tuple<T>> {
    typedef typename add_cv<T>::type type;
};

template <typename T1, typename T2>
struct tuple_element<0, tuple<T1, T2>> {
    typedef T1 type;
};

template <typename T1, typename T2>
struct tuple_element<0, const tuple<T1, T2>> {
    typedef typename add_const<T1>::type type;
};

template <typename T1, typename T2>
struct tuple_element<0, volatile tuple<T1, T2>> {
    typedef typename add_volatile<T1>::type type;
};

template <typename T1, typename T2>
struct tuple_element<0, const volatile tuple<T1, T2>> {
    typedef typename add_cv<T1>::type type;
};

template <typename T1, typename T2>
struct tuple_element<1, tuple<T1, T2>> {
    typedef T2 type;
};

template <typename T1, typename T2>
struct tuple_element<1, const tuple<T1, T2>> {
    typedef typename add_const<T2>::type type;
};

template <typename T1, typename T2>
struct tuple_element<1, volatile tuple<T1, T2>> {
    typedef typename add_volatile<T2>::type type;
};

template <typename T1, typename T2>
struct tuple_element<1, const volatile tuple<T1, T2>> {
    typedef typename add_cv<T2>::type type;
};

template <typename T, typename... Args>
struct tuple_element<0, tuple<T, Args...>> {
    typedef T type;
};

template <typename T, typename... Args>
struct tuple_element<0, const tuple<T, Args...>> {
    typedef typename add_const<T>::type type;
};

template <typename T, typename... Args>
struct tuple_element<0, volatile tuple<T, Args...>> {
    typedef typename add_volatile<T>::type type;
};

template <typename T, typename... Args>
struct tuple_element<0, const volatile tuple<T, Args...>> {
    typedef typename add_cv<T>::type type;
};
template <size_t N, typename T, typename... Args>
struct tuple_element<N, tuple<T, Args...>> {
    typedef typename tuple_element<N - 1, tuple<Args...>>::type type;
};

template <size_t N, typename T, typename... Args>
struct tuple_element<N, const tuple<T, Args...>> {
    typedef typename tuple_element<N - 1, const tuple<Args...>>::type type;
};

template <size_t N, typename T, typename... Args>
struct tuple_element<N, volatile tuple<T, Args...>> {
    typedef typename tuple_element<N - 1, volatile tuple<Args...>>::type type;
};

template <size_t N, typename T, typename... Args>
struct tuple_element<N, const volatile tuple<T, Args...>> {
    typedef typename tuple_element<
        N - 1, const volatile tuple<Args...>
    >::type type;
};

template <size_t N>
struct tuple_helper_get {
    template <typename T, typename... Args> static
    auto get_ref(tuple_helper<T, Args...> &t) noexcept
        -> decltype(tuple_helper_get<N - 1>::get_ref(t.ot))
    {
        return tuple_helper_get<N - 1>::get_ref(t.ot);
    }

    template <typename T, typename... Args> static
    auto get_ref(tuple_helper<T, Args...> &&t) noexcept
        -> decltype(tuple_helper_get<N - 1>::get_ref(glglT::move(t.ot)))
    {
        return tuple_helper_get<N - 1>::get_ref(glglT::move(t.ot));
    }

    template <typename T, typename... Args> static
    auto get_ref(const tuple_helper<T, Args...> &t) noexcept
        -> decltype(tuple_helper_get<N - 1>::get_ref(t.ot))
    {
        return tuple_helper_get<N - 1>::get_ref(t.ot);
    }
};

template <>
struct tuple_helper_get<0> {
    template <typename T> static
    T &get_ref(tuple_helper<T> &t, int) noexcept
    {
        return t.t;
    }

    template <typename T> static
    T &&get_ref(tuple_helper<T> &&t, int) noexcept
    {
        return glglT::forward<T>(t.t);
    }

    template <typename T> static
    auto get_ref(const tuple_helper<T> &t, int) noexcept
        -> typename add_const<T>::type
    {
        return t.t;
    }

    template <typename T1, typename T2> static
    T1 &get_ref(tuple_helper<T1, T2> &t) noexcept
    {
        return t.t1;
    }

    template <typename T1, typename T2> static
    T1 &&get_ref(tuple_helper<T1, T2> &&t) noexcept
    {
        return glglT::forward<T1>(t.t1);
    }

    template <typename T1, typename T2> static
    auto get_ref(const tuple_helper<T1, T2> &t) noexcept
        -> typename add_const<T1>::type
    {
        return t.t1;
    }

    template <typename T, typename... Args> static
    T &get_ref(tuple_helper<T, Args...> &t) noexcept
    {
        return t.t;
    }

    template <typename T, typename... Args> static
    T &&get_ref(tuple_helper<T, Args...> &&t) noexcept
    {
        return glglT::forward<T>(t.t);
    }

    template <typename T, typename... Args> static
    auto get_ref(const tuple_helper<T, Args...> &t) noexcept
        -> typename add_const<T>::type
    {
        return t.t;
    }
};

template <>
struct tuple_helper_get<1> {
    template <typename T1, typename T2> static
    T2 &get_ref(tuple_helper<T1, T2> &t) noexcept
    {
        return t.t2;
    }

    template <typename T1, typename T2> static
    T2 &&get_ref(tuple_helper<T1, T2> &&t) noexcept
    {
        return glglT::forward<T2>(t.t2);
    }

    template <typename T1, typename T2> static
    auto get_ref(const tuple_helper<T1, T2> &t) noexcept
        -> typename add_const<T2>::type
    {
        return t.t2;
    }

    template <typename T, typename... Args> static
    auto get_ref(tuple_helper<T, Args...> &t) noexcept
        -> decltype(tuple_helper_get<0>::get_ref(t.ot))
    {
        return tuple_helper_get<0>::get_ref(t.ot);
    }

    template <typename T, typename... Args> static
    auto get_ref(tuple_helper<T, Args...> &&t) noexcept
        -> decltype(tuple_helper_get<0>::get_ref(t.ot))
    {
        return tuple_helper_get<0>::get_ref(t.ot);
    }

    template <typename T, typename... Args> static
    auto get_ref(const tuple_helper<T, Args...> &t) noexcept
        -> decltype(tuple_helper_get<0>::get_ref(t.ot))
    {
        return tuple_helper_get<0>::get_ref(t.ot);
    }
};

template <size_t N, typename T> inline
T &&get(T&& t) noexcept
{
    return glglT::forward<T>(t);
}

template <size_t N, typename T1, typename T2, typename... Args> inline
auto get(tuple<T1, T2, Args...> &t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(t.data))
{
    return tuple_helper_get<N>::get_ref(t.data);
}

template <size_t N, typename T1, typename T2, typename... Args> inline
auto get(tuple<T1, T2, Args...> &&t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(glglT::move(t.data)))
{
    return tuple_helper_get<N>::get_ref(glglT::move(t.data));
}

template <size_t N, typename T1, typename T2, typename... Args> inline
auto get(const tuple<T1, T2, Args...> &t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(t.data))
{
    return tuple_helper_get<N>::get_ref(t.data);
}

template <size_t N, typename T> inline
auto get(tuple<T> &t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(t.data, 0))
{
    return tuple_helper_get<N>::get_ref(t.data, 0);
}

template <size_t N, typename T> inline
auto get(tuple<T> &&t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(glglT::move(t.data), 0))
{
    return tuple_helper_get<N>::get_ref(glglT::move(t.data), 0);
}

template <size_t N, typename T> inline
auto get(const tuple<T> &t) noexcept
    -> decltype(tuple_helper_get<N>::get_ref(t.data, 0))
{
    return tuple_helper_get<N>::get_ref(t.data, 0);
}

constexpr bool operator==(const tuple<> t1, const tuple<> t2) noexcept
{
    return true;
}

template <typename T> inline
bool operator==(const tuple_helper<T> &t1, const tuple_helper<T> &t2) noexcept
{
    return t1.t == t2.t;
}

template <typename T1, typename T2> inline
bool operator==(const tuple_helper<T1, T2> &t1,
                const tuple_helper<T1, T2> &t2) noexcept
{
    return t1.t1 == t2.t1 && t1.t2 == t2.t2;
}

template <typename T, typename... Args> inline
bool operator==(const tuple_helper<T, Args...> &t1,
                const tuple_helper<T, Args...> &t2) noexcept
{
    return t1.t == t2.t && t1.ot == t2.ot;
}

template <typename... Args> inline
bool operator==(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return t1.data == t2.data;
}

template <typename... Args> inline
bool operator!=(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return !(t1 == t2);
}

constexpr bool operator<(const tuple<> &t1, const tuple<> &t2) noexcept
{
    return false;
}

template <typename T> inline
bool operator<(const tuple_helper<T> &t1, const tuple_helper<T> &t2) noexcept
{
    return t1.t < t2.t;
}

template <typename T1, typename T2> inline
bool operator<(const tuple_helper<T1, T2> &t1,
               const tuple_helper<T1, T2> &t2) noexcept
{
    return t1.t1 < t2.t1 || (t1.t1 == t2.t1 && t1.t2 < t2.t2);
}

template <typename T, typename... Args> inline
bool operator<(const tuple_helper<T, Args...> &t1,
               const tuple_helper<T, Args...> &t2) noexcept
{
    return t1.t < t2.t || (t1.t == t2.t && t1.ot < t2.ot);
}

template <typename... Args> inline
bool operator<(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return t1.data < t2.data;
}

template <typename... Args> inline
bool operator>(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return t2 < t1;
}

template <typename... Args> inline
bool operator<=(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return !(t1 > t2);
}

template <typename... Args> inline
bool operator>=(const tuple<Args...> &t1, const tuple<Args...> &t2) noexcept
{
    return !(t1 < t2);
}

template <size_t... Is> struct index_holder {
    constexpr index_holder() noexcept = default;
};

template <typename T> constexpr
index_holder<0> make_single_index(const T &) noexcept;

template <typename T> constexpr
index_holder<0> make_single_index(const tuple_helper<T> &) noexcept;

template <typename T1, typename T2> constexpr
index_holder<0, 1> make_single_index(const tuple_helper<T1, T2> &) noexcept;

template <typename T1, typename T2> constexpr
index_holder<0, 1> make_single_index(const pair<T1, T2> &) noexcept;

template <size_t... Iprev> constexpr
auto add_one_index(index_holder<Iprev...>) noexcept
    -> index_holder<0, (Iprev + 1)...>;

template <typename T, typename... Args> constexpr
auto make_single_index(const tuple_helper<T, Args...> &t) noexcept
    -> decltype(glglT::add_one_index(decltype(make_single_index(t.ot))()));

template <typename T, typename... Args> constexpr
auto make_single_index(const tuple<T, Args...> &t) noexcept
    -> decltype(glglT::make_single_index(t.data));

template <size_t... Is> constexpr
index_holder<Is...> cat_index(index_holder<Is...>) noexcept;

template <size_t... I1, size_t... I2> constexpr
auto cat_index(index_holder<I1...>, index_holder<I2...>) noexcept
    -> index_holder<I1..., I2...>;

template <size_t... I1, size_t... I2, typename... IHs> constexpr
auto cat_index(index_holder<I1...>, index_holder<I2...>, IHs... ihs) noexcept
    -> decltype(glglT::cat_index(index_holder<I1..., I2...>(), ihs...));

template <typename... Tuples> constexpr
auto make_level2_index(Tuples&&... tuples) noexcept
    -> decltype(glglT::cat_index(decltype(glglT::make_single_index(tuples))()...));

template <size_t I, typename T> constexpr
index_holder<I> make_multiple_index(const T &) noexcept;

template <size_t I, typename T> constexpr
index_holder<I> make_multiple_index(const tuple_helper<T> &) noexcept;

template <size_t I, typename T1, typename T2> constexpr
index_holder<I, I> make_multiple_index(const tuple_helper<T1, T2> &) noexcept;

template <size_t I, typename T1, typename T2> constexpr
index_holder<I, I> make_multiple_index(const pair<T1, T2> &) noexcept;

template <size_t I, size_t... Is> constexpr
auto add_equal_index(index_holder<I, Is...>) noexcept
    -> index_holder<I, I, Is...>;

template <size_t I, typename T, typename... Args> constexpr
auto make_multiple_index(const tuple_helper<T, Args...> &t) noexcept
    -> decltype(glglT::add_equal_index(decltype(make_multiple_index<I>(t.ot))()));

template <size_t I, typename T, typename... Args> constexpr
auto make_multiple_index(const tuple<T, Args...> &t) noexcept
    -> decltype(glglT::make_multiple_index<I>(t.data));

template <size_t... Is, typename... Tuples> constexpr
auto make_level1_index_helper(index_holder<Is...>, Tuples&&... tuples) noexcept
    -> decltype(glglT::cat_index(decltype(glglT::make_multiple_index<Is>(tuples))()...));

template <typename... Tuples> constexpr
auto make_level1_index(Tuples&&... tuples) noexcept
    -> decltype(glglT::make_level1_index_helper
                (decltype(glglT::make_single_index
                          (glglT::forward_as_tuple(glglT::forward<Tuples>(tuples)...)))(),
                 glglT::forward<Tuples>(tuples)...));

template <size_t... Is1, size_t... Is2, typename... Tuples> inline
auto final_tuple(index_holder<Is1...>,
                 index_holder<Is2...>, tuple<Tuples...> &&t)
    -> decltype(forward_as_tuple(get<Is2>(get<Is1>(glglT::move(t)))...))
{
    return glglT::forward_as_tuple(glglT::get<Is2>(glglT::get<Is1>(glglT::move(t)))...);
}

template <typename... Tuples> inline
auto tuple_cat(Tuples&&... tuples)
    -> decltype(glglT::final_tuple
                (decltype(glglT::make_level1_index(glglT::forward<Tuples>(tuples)...))(),
                 decltype(glglT::make_level2_index(glglT::forward<Tuples>(tuples)...))(),
                 glglT::forward_as_tuple(glglT::forward<Tuples>(tuples)...)))
{
    return glglT::final_tuple(decltype
                       (glglT::make_level1_index(glglT::forward<Tuples>(tuples)...))(),
                       decltype
                       (glglT::make_level2_index(glglT::forward<Tuples>(tuples)...))(),
                       glglT::forward_as_tuple(glglT::forward<Tuples>(tuples)...));
}

template <typename T, typename Tuple, size_t... Is>
T make_from_tuple_helper(Tuple &&t, index_holder<Is...>)
{
    return T(glglT::get<Is>(glglT::forward<Tuple>(t))...);
}

template <typename T, typename Tuple>
T make_from_tuple(Tuple &&t)
{
    return glglT::make_from_tuple_helper(glglT::forward<Tuple>(t),
                                         glglT::make_single_index(t));
}

template <typename F, typename Tuple, size_t... Is>
auto apply_helper(F &&f, Tuple &&t, index_holder<Is...>)
    -> decltype(glglT::invoke(glglT::forward<F>(f),
                              glglT::get<Is>(glglT::forward<Tuple>(t))...))
{
    return glglT::invoke(glglT::forward<F>(f),
                         glglT::get<Is>(glglT::forward<Tuple>(t))...);
}

template <typename F, typename Tuple>
auto apply(F &&f, Tuple &&t)
    -> decltype(glglT::apply_helper(glglT::forward<F>(f), glglT::forward<Tuple>(t),
                                    glglT::make_single_index(t)))
{
    return glglT::apply_helper(glglT::forward<F>(f), glglT::forward<Tuple>(t),
                               glglT::make_single_index(t));
}

template <size_t N> struct make_index {
    typedef decltype(glglT::add_one_index(typename make_index<N - 1>::type())) type;
};

template <> struct make_index<0> { typedef index_holder<> type; };

} // namespace glglT


#endif // GLGL_TUPLE_IM_H
