#ifndef GLGL_TREE_BASE_H
#define GLGL_TREE_BASE_H

#include <initializer_list>
#include "glgl_pair.h"
#include "glgl_allocator.h"
#include "glgl_allocator_traits.h"
#include "glgl_function_object.h"
#include "glgl_other_iterator.h"

namespace glglT {

template <typename V, typename Alloc>
struct tree_node_t {
    tree_node_t() = default;
    template <typename... Args>
    explicit tree_node_t(Args&&... args): v(glglT::forward<Args>(args)...) {}
    V v;
    bool is_red;
    typename allocator_traits<Alloc>::
        template rebind_traits<tree_node_t>::pointer p;
    typename allocator_traits<Alloc>::
        template rebind_traits<tree_node_t>::pointer l;
    typename allocator_traits<Alloc>::
        template rebind_traits<tree_node_t>::pointer r;
};

template <typename, typename, typename, typename> class tree_base;
template <typename, typename> class tree_iterator;

template <typename V, typename RPtr>
class tree_const_iterator: public iterator<bidirectional_iterator_tag, const V> {
    template <typename, typename, typename, typename> friend class tree_base;
    friend class tree_iterator<V, RPtr>;

    RPtr curr, null;
public:
    constexpr tree_const_iterator() noexcept {}
    tree_const_iterator(RPtr rp, RPtr n) noexcept: curr(rp), null(n) {}
    const V &operator*() const noexcept { return curr->v; }
    const V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    explicit operator tree_iterator<V, RPtr>() const noexcept;
    tree_const_iterator &operator++() noexcept
    {
        if (curr == null)
            curr = curr->r;
        else if (curr->r == null) {
            do {
                if (curr->p->r != curr) {
                    curr = curr->p;
                    break;
                }   curr = curr->p;
            } while (curr != null);
        } else {
            curr = curr->r;
            while (curr->l != null)
                curr = curr->l;
        }
        return *this;
    }

    tree_const_iterator &operator--() noexcept
    {
        if (curr == null)
            curr = curr->l;
        else if (curr->l == null) {
            do {
                if (curr->p->l != curr) {
                    curr = curr->p;
                    break;
                }   curr = curr->p;
            } while (curr != null);
        } else {
            curr = curr->l;
            while (curr->r != null)
                curr = curr->r;
        }
        return *this;
    }

    tree_const_iterator operator++(int) noexcept
    {
        tree_const_iterator r = *this;
        this->operator++();
        return r;
    }

    tree_const_iterator operator--(int) noexcept
    {
        tree_const_iterator r = *this;
        this->operator--();
        return r;
    }

    bool operator==(const tree_const_iterator &r) const noexcept
    {
        return curr == r.curr;
    }
};

template <typename V, typename P> inline
bool operator!=(const tree_const_iterator<V, P> &r1,
                const tree_const_iterator<V, P> &r2) noexcept
{
    return r1.base() != r2.base();
}

template <typename V, typename RPtr>
class tree_iterator: public iterator<bidirectional_iterator_tag, V>  {
    template <typename, typename, typename, typename> friend class tree_base;
    friend class tree_const_iterator<V, RPtr>;

    RPtr curr, null;
public:
    constexpr tree_iterator() noexcept {}
    tree_iterator(RPtr rp, RPtr n) noexcept: curr(rp), null(n) {}
    V &operator*() const noexcept { return curr->v; }
    V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    operator tree_const_iterator<V, RPtr>() const noexcept
    {
        return tree_const_iterator<V, RPtr>(curr, null);
    }

    tree_iterator &operator++() noexcept
    {
        if (curr == null)
            curr = curr->r;
        else if (curr->r == null) {
            do {
                if (curr->p->r != curr) {
                    curr = curr->p;
                    break;
                }   curr = curr->p;
            } while (curr != null);
        } else {
            curr = curr->r;
            while (curr->l != null)
                curr = curr->l;
        }
        return *this;
    }

    tree_iterator &operator--() noexcept
    {
        if (curr == null)
            curr = curr->l;
        else if (curr->l == null) {
            do {
                if (curr->p->l != curr) {
                    curr = curr->p;
                    break;
                }   curr = curr->p;
            } while (curr != null);
        } else {
            curr = curr->l;
            while (curr->r != null)
                curr = curr->r;
        }
        return *this;
    }

    tree_iterator operator++(int) noexcept
    {
        tree_iterator r = *this;
        this->operator++();
        return r;
    }

    tree_iterator operator--(int) noexcept
    {
        tree_iterator r = *this;
        this->operator--();
        return r;
    }

    bool operator==(const tree_const_iterator<V, RPtr> &r) const noexcept
    {
        return curr == r.curr;
    }
};

template <typename V, typename P> inline
bool operator!=(const tree_iterator<V, P> &r1,
                const tree_iterator<V, P> &r2) noexcept
{
    return r1.base() != r2.base();
}

template <typename V, typename P> inline
bool operator!=(const tree_const_iterator<V, P> &r1,
                const tree_iterator<V, P> &r2) noexcept
{
    return r1.base() != r2.base();
}

template <typename V, typename P> inline
bool operator!=(const tree_iterator<V, P> &r1,
                const tree_const_iterator<V, P> &r2) noexcept
{
    return r1.base() != r2.base();
}

template <typename V, typename P> inline
tree_const_iterator<V, P>::operator tree_iterator<V, P>() const noexcept
{
    return tree_iterator<V, P>(curr, null);
}

template <typename, typename, typename> class set;
template <typename, typename, typename> class multiset;
template <typename, typename, typename, typename> class map;
template <typename, typename, typename, typename> class multimap;

template <typename, typename> struct tree_type_adoptor;

template <typename K, typename Comp, typename Alloc, typename RPtr>
struct tree_type_adoptor<set<K, Comp, Alloc>, RPtr> {
    typedef K key_type;
    typedef tree_const_iterator<K, RPtr> iterator;
    typedef pair<iterator, bool> insert_return_type;
};

template <typename K, typename Comp, typename Alloc, typename RPtr>
struct tree_type_adoptor<multiset<K, Comp, Alloc>, RPtr> {
    typedef K key_type;
    typedef tree_const_iterator<K, RPtr> iterator;
    typedef iterator insert_return_type;
};

template <typename K, typename T, typename Comp, typename Alloc, typename RPtr>
struct tree_type_adoptor<map<K, T, Comp, Alloc>, RPtr> {
    typedef K key_type;
    typedef tree_iterator<pair<const K, T>, RPtr> iterator;
    typedef pair<iterator, bool> insert_return_type;
};

template <typename K, typename T, typename Comp, typename Alloc, typename RPtr>
struct tree_type_adoptor<multimap<K, T, Comp, Alloc>, RPtr> {
    typedef K key_type;
    typedef tree_iterator<pair<const K, T>, RPtr> iterator;
    typedef iterator insert_return_type;
};

template <typename Tree, typename V, typename Comp, typename Alloc>
class tree_base {
    typedef typename allocator_traits<Alloc>::
        template rebind_alloc<tree_node_t<V, Alloc>> rebind_alloc;
    struct move_tag {};
protected:
    typedef typename allocator_traits<Alloc>::
        template rebind_traits<tree_node_t<V, Alloc>> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;
    typedef typename tree_type_adoptor<Tree, rebind_ptr>::
        insert_return_type insert_return_type;

    Comp c;
    rebind_alloc a;
    rebind_ptr eot;
    size_t len;
public:
    typedef typename tree_type_adoptor<Tree, rebind_ptr>::key_type key_type;
    typedef V value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef Comp key_compare;
    typedef Alloc allocator_type;
    typedef V & reference;
    typedef const V & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef typename tree_type_adoptor<Tree, rebind_ptr>::iterator iterator;
    typedef tree_const_iterator<V, rebind_ptr> const_iterator;
    typedef glglT::reverse_iterator<iterator> reverse_iterator;
    typedef glglT::reverse_iterator<const_iterator> const_reverse_iterator;
protected:
    tree_base(const Comp &cp, const Alloc &ac): c(cp), a(ac), len()
    {
        eot = rebind_traits::allocate(a, 1);
        eot->p = eot->l = eot->r = eot;
    }

    explicit tree_base(const Alloc &ac): c(), a(ac), len()
    {
        eot = rebind_traits::allocate(a, 1);
        eot->p = eot->l = eot->r = eot;
    }

    template <typename It>
    tree_base(It b, It e, const Comp &cp, const Alloc &ac): c(cp), a(ac), len()
    {
        eot = rebind_traits::allocate(a, 1);
        eot->p = eot->l = eot->r = eot;
        try {
            if (b != e)
                for (iterator itr = Tree::get(this->emplace(*b)); ++b != e;)
                    itr = this->emplace_hint(itr, *b);
        } catch(...) {
            this->clear();
            throw;
        }
    }

    tree_base(const tree_base &o): c(o.c),
        a(rebind_traits::select_on_container_copy_construction(o.a)), len(o.len)
    {
        eot = rebind_traits::allocate(a, 1);
        eot->l = eot->r = eot;
        const auto p = this->build_from_other(o.eot->p, o);
        eot->p = p;
        p->p = eot;
    }

    tree_base(const tree_base &o, const Alloc &ac): c(o.c), a(ac), len(o.len)
    {
        eot = rebind_traits::allocate(a, 1);
        eot->l = eot->r = eot;
        const auto p = this->build_from_other(o.eot->p, o);
        eot->p = p;
        p->p = eot;
    }

    tree_base(tree_base &&o): c(glglT::move(o.c)), a(glglT::move(o.a)), len(o.len)
    {
        eot = rebind_traits::allocate(a, 1);
        eot->p = eot->l = eot->r = eot;
        glglT::swap(eot, o.eot);
    }

    tree_base(tree_base &&o, const Alloc &ac): c(glglT::move(o.c)), a(ac), len(o.len)
    {
        eot = rebind_traits::allocate(a, 1);
        eot->l = eot->r = eot;
        if (a == o.a) {
            eot->p = eot;
            glglT::swap(eot, o.eot);
        } else {
            const auto p = this->build_from_other(o.eot->p, o, move_tag());
            eot->p = p;
            p->p = eot;
        }
    }

    tree_base(std::initializer_list<V> il, const Comp &cp, const Alloc &ac):
        tree_base(il.begin(), il.end(), cp, ac) {}

    ~tree_base() { this->free(); rebind_traits::deallocate(a, eot, 1); }

    tree_base &operator=(const tree_base &o)
    {
        if (eot == o.eot)
            return *this;
        this->clear();
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        c = o.c;
        len = o.len;
        const auto p = this->build_from_other(o.eot->p, o);
        eot->p = p;
        p->p = eot;
        return *this;
    }

    tree_base &operator=(tree_base &&o)
    {
        if (eot == o.eot)
            return *this;
        this->clear();
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        c = o.c;
        len = o.len;
        if (a != o.a) {
            const auto p = this->build_from_other(o.eot->p, o, move_tag());
            eot->p = p;
            p->p = eot;
        } else
            glglT::swap(eot, o.eot);
        return *this;
    }

    tree_base &operator=(std::initializer_list<V> il)
    {
        this->clear();
        auto b = il.begin();
        const auto e = il.end();
        try {
            if (b != e)
                for (iterator itr = Tree::get(this->emplace(*b)); ++b != e;)
                    itr = this->emplace_hint(itr, *b);
        } catch(...) {
            this->clear();
            throw;
        }
        return *this;
    }
public:
    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    iterator begin() noexcept { return iterator(eot->r, eot); }
    const_iterator begin() const noexcept { return this->cbegin(); }
    const_iterator cbegin() const noexcept
    {
        return const_iterator(eot->r, eot);
    }

    iterator end() noexcept { return iterator(eot, eot); }
    const_iterator end() const noexcept { return const_iterator(eot, eot); }
    const_iterator cend() const noexcept { return const_iterator(eot, eot); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(this->end()); }
    const_reverse_iterator rbegin() const noexcept { return this->crbegin(); }
    const_reverse_iterator crbegin() const noexcept
    {
        return const_reference_iterator(this->cend());
    }

    reverse_iterator rend() noexcept { return reverse_iterator(this->begin()); }
    const_iterator rend() const noexcept { return this->crend(); }
    const_reverse_iterator crend() const noexcept
    {
        return const_reverse_iterator(this->cbegin());
    }

    bool empty() const noexcept { return len == 0; }
    size_type size() const noexcept { return len; }
    size_type max_size() const noexcept { return rebind_traits::max_size(a); }

    void clear() noexcept
    {
        this->free();
        len = 0;
        eot->p = eot->l = eot->r = eot;
    }

    template <typename P>
    insert_return_type insert(P &&p) { return this->emplace(glglT::forward<P>(p)); }
    insert_return_type insert(const V &v) { return this->emplace(v); }

    iterator insert(const_iterator hint, const V &v)
    {
        return this->emplace_hint(hint, v);
    }

    template <typename P>
    iterator insert(const_iterator hint, P &&p)
    {
        return this->emplace_hint(hint, glglT::forward<P>(p));
    }

    void insert(std::initializer_list<V> il) { this->insert(il.begin(), il.end()); }
    template <typename It>
    void insert(It b, It e)
    {
        if (b != e)
            for (iterator itr = Tree::get(this->emplace(*b)); ++b != e;)
                itr = this->emplace_hint(itr, *b);
    }

    template <typename... Args>
    insert_return_type emplace(Args&&... args)
    {
        rebind_ptr new_node = this->get_new_node(glglT::forward<Args>(args)...);
        try {
            return static_cast<Tree *>(this)->insert_equal(eot->p, new_node);
        } catch(...) {
            rebind_traits::destroy(a, new_node);
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }
    }

    template <typename... Args>
    iterator emplace_hint(const_iterator hint, Args&&... args)
    {
        rebind_ptr new_node = this->get_new_node(glglT::forward<Args>(args)...);
        try {
            if (hint.curr == eot) {
                return Tree::get(static_cast<Tree *>(this)->
                                 insert_equal(eot->p, new_node));
            } else {
                const auto c = static_cast<Tree *>(this)->value_comp();
                if (c(new_node->v, hint.curr->v)) {
                    if (!c(hint.curr->v, eot->p->v)) {
                        const bool b(!c(new_node->v, eot->p->v));
                        static_cast<Tree *>(this)->insert_equal(eot->p, new_node, b);
                    } else {
                        for (auto p_hint = hint.curr->p, prev_p = hint.curr;
                             p_hint != eot; prev_p = p_hint, p_hint = p_hint->p) {
                            if (!c(new_node->v, p_hint->v))
                                break;
                            if (p_hint->r == prev_p)
                                hint.curr = p_hint;
                        }
                        static_cast<Tree *>(this)->insert_equal(hint.curr, new_node, false);
                    }
                } else if (!c(eot->p->v, hint.curr->v)) {
                    const bool b(!c(new_node->v, eot->p->v));
                    static_cast<Tree *>(this)->insert_equal(eot->p, new_node, b);
                } else {
                    for (auto p_hint = hint.curr->p, prev_p = hint.curr;
                         p_hint != eot; prev_p = p_hint, p_hint = p_hint->p) {
                        if (c(new_node->v, p_hint->v))
                            break;
                        if (p_hint->l == prev_p)
                            hint.curr = p_hint;
                    }
                    static_cast<Tree *>(this)->insert_equal(hint.curr, new_node, true);
                }
            }
        } catch(...) {
            rebind_traits::destroy(a, new_node);
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }
        return iterator(new_node, eot);
    }

    iterator erase(const_iterator itr)
    {
        this->erase((itr++).curr);
        return static_cast<iterator>(itr);
    }

    iterator erase(const_iterator f, const_iterator l)
    {
        if (f != l) {
            iterator itr(static_cast<iterator>(f));
            do {
                itr = this->erase(itr);
            } while (itr != l);
            return itr;
        }   return static_cast<iterator>(l);
    }

    size_type erase(const key_type &k)
    {
        const auto p = this->equal_range(k);
        const auto n = glglT::distance(p.first, p.second);
        this->erase(p.first, p.second);
        return n;
    }

    void swap(Tree &o)
    {
        using glglT::swap;
        swap(c, o.c);
        glglT::alloc_swap(a, o.a, typename rebind_traits::
                          propagate_on_container_swap());
        glglT::swap(eot, o.eot);
        glglT::swap(len, o.len);
    }

    iterator find(const key_type &k)
    {
        return static_cast<iterator>(const_cast<const tree_base *>(this)->find(k));
    }

    const_iterator find(const key_type &k) const
    {
        rebind_ptr p = eot->p;
        while (p != eot)
            if (this->c(Tree::get_key(p->v), k))
                p = p->r;
            else if (this->c(k, Tree::get_key(p->v)))
                p = p->l;
            else break;
        return const_iterator(p, eot);
    }

    pair<iterator, iterator>
    equal_range(const key_type &k)
    {
        const auto p =
            static_cast<const Tree *>(this)->equal_range(k);
        return glglT::make_pair(static_cast<iterator>(p.first),
                                static_cast<iterator>(p.second));
    }

    iterator lower_bound(const key_type &k)
    {
        return static_cast<iterator>(const_cast<const tree_base *>(this)->
                                     lower_bound(k));
    }

    const_iterator lower_bound(const key_type &k) const
    {
        return this->not_smaller(k);
    }

    iterator upper_bound(const key_type &k)
    {
        return static_cast<iterator>(const_cast<const tree_base *>(this)->
                                     upper_bound(k));
    }

    const_iterator upper_bound(const key_type &k) const
    {
        return this->bigger(k);
    }
protected:
    void insert_fix(rebind_ptr n, rebind_ptr p) noexcept
    {
        for (n->is_red = true; p != eot->p && p->is_red; n = p, p = p->p) {
            const rebind_ptr nextp = p->p;
            const bool is_left = p == nextp->l;
            if ((n == p->l) ^ is_left)
                is_left ? this->left_rotate(p, n) : this->right_rotate(p, n);
            else n = p;
            p = nextp;
            const rebind_ptr b = is_left ? p->r : p->l;
            n->is_red = false;
            p->is_red = true;
            if (!b->is_red) {
                is_left ? this->right_rotate(p, n) : this->left_rotate(p, n);
                break;
            }
            b->is_red = false;
        }
        eot->p->is_red = false;
    }

    const_iterator not_smaller(const key_type &k) const
    {
        rebind_ptr p = eot->p, tmp = eot;
        while (p != eot)
            if (this->c(Tree::get_key(p->v), k)) {
                tmp = p;
                p = p->r;
            } else
                p = p->l;
        const_iterator res(tmp, eot);
        return ++res;
    }

    const_iterator bigger(const key_type &k) const
    {
        rebind_ptr p = eot->p, tmp = eot;
        while (p != eot)
            if (this->c(k, Tree::get_key(p->v))) {
                tmp = p;
                p = p->l;
            } else
                p = p->r;
        return const_iterator(tmp, eot);
    }
private:
    void erase(rebind_ptr n) noexcept
    {
        for (rebind_ptr p = n->p; ; p = n->p) {
            const bool no_left = n->l == eot, no_right = n->r == eot;
            const bool is_root = p == eot;
            const bool is_left = p->l == n;
            if (no_left || no_right) {
                if (is_root) {
                    if (no_right)
                        eot->l = eot->p = n->l;
                    if (no_left)
                        eot->r = eot->p = n->r;
                } else if (is_left) {
                    p->l = no_left ? n->r : n->l;
                    if (eot->r == n) {
                        if (!no_right) {
                            eot->r = n->r;
                            while (eot->r->l != eot)
                                eot->r = eot->r->l;
                        } else
                            eot->r = p;
                    }
                } else {
                    p->r = no_left ? n->r : n->l;
                    if (eot->l == n) {
                        if (!no_left) {
                            eot->l = n->l;
                            while (eot->l->r != eot)
                                eot->l = eot->l->r;
                        } else
                            eot->l = p;
                    }
                }
                if (!no_left)
                    n->l->p = p;
                else if (!no_right)
                    n->r->p = p;
            } else {
                rebind_ptr r = n->r;
                while (r->l != eot)
                    r = r->l;
                glglT::swap(r->is_red, n->is_red);
                r->l = n->l;
                n->l = eot;
                r->l->p = r;
                const auto r_is_n_child = n->r == r;
                if (r_is_n_child) {
                    r->p = n->p;
                    n->p = r;
                    n->r = r->r;
                    r->r = n;
                } else {
                    glglT::swap(r->p, n->p);
                    glglT::swap(r->r, n->r);
                    n->p->l = n;
                    r->r->p = r;
                }
                if (n->r != eot)
                    n->r->p = n;
                (is_root ? eot->p : is_left ? p->l : p->r) = r;
                continue;
            }   --len;
            if (!n->is_red)
                this->erase_fix(p, is_left);
            rebind_traits::destroy(a, n);
            rebind_traits::deallocate(a, n, 1);
            return;
        }
    }

    void erase_fix(rebind_ptr p, bool is_left) noexcept
    {
        for (rebind_ptr black_p = is_left ? p->l : p->r; p != eot;) {
            if (black_p->is_red) {
                black_p->is_red = false;
                return;
            }
            rebind_ptr b = is_left ? p->r : p->l;
            if (b->is_red) {
                b->is_red = false;
                p->is_red = true;
                if (is_left) {
                    this->left_rotate(p, b);
                    b = p->r;
                } else {
                    this->right_rotate(p, b);
                    b = p->l;
                }
            }
            rebind_ptr c = is_left ? b->r : b->l;
            if (!c->is_red) {
                if (is_left ? !b->l->is_red : !b->r->is_red) {
                    b->is_red = true;
                    black_p = p;
                    p = p->p;
                    is_left = black_p == p->l;
                    continue;
                }
                is_left ? this->right_rotate(b, b->l) : this->left_rotate(b, b->r);
                c = b;
                b = b->p;
                b->is_red = false;
            }
            is_left ? this->left_rotate(p, b) : this->right_rotate(p, b);
            c->is_red = false;
            glglT::swap(p->is_red, b->is_red);
            eot->p->is_red = false;
            break;
        }
    }

    template <typename... Args>
    rebind_ptr get_new_node(Args&&... args)
    {
        rebind_ptr new_node = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, new_node, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }   return new_node;
    }

    rebind_ptr get_new_node_copy(const V &v) { return this->get_new_node(v); }
    rebind_ptr get_new_node_copy(const V &v, move_tag)
    {
        return this->get_new_node(glglT::move(v));
    }

    template <typename... MayMove>
    rebind_ptr build_from_other(rebind_ptr p, const tree_base &o, MayMove&&... mm)
    {
        if (p == o.eot)
            return eot;
        const rebind_ptr l = this->build_from_other(p->l, o, glglT::forward<MayMove>(mm)...);
        const rebind_ptr r = this->build_from_other(p->r, o, glglT::forward<MayMove>(mm)...);
        rebind_ptr node;
        try {
            node = this->get_new_node_copy(p->v, glglT::forward<MayMove>(mm)...);
        } catch(...) {
            if (l != eot) this->free(l);
            if (r != eot) this->free(r);
            eot->p = eot->l = eot->r = eot;
            len = 0;
            throw;
        }
        if (o.eot->l == p) eot->l = node;
        if (o.eot->r == p) eot->r = node;
        l->p = r->p = node;
        node->is_red = p->is_red;
        node->l = l; node->r = r;
        return node;
    }

    void free() noexcept { if (len) this->free(eot->p); }
    void free(rebind_ptr root) noexcept
    {
        for (rebind_ptr p = root, tmp; ; ) {
            while (p->l != eot)
                p = p->l;
            if (p->r != eot)
                p = p->r;
            else
                while (true) {
                    const bool is_left = p == p->p->l;
                    tmp = p;
                    p = p->p;
                    rebind_traits::destroy(a, tmp);
                    rebind_traits::deallocate(a, tmp, 1);
                    if (p == eot)
                        return;
                    if (is_left) {
                        if (p->r != eot) {
                            p = p->r;
                            break;
                        }
                    }
                }
        }
    }

    void left_rotate(rebind_ptr p, rebind_ptr c) noexcept
    {
        p->r = c->l;
        if (c->l != eot)
            c->l->p = p;
        c->l = p;
        (p->p == eot ? eot->p : p->p->l == p ? p->p->l : p->p->r) = c;
        c->p = p->p;
        p->p = c;
    }

    void right_rotate(rebind_ptr p, rebind_ptr c) noexcept
    {
        p->l = c->r;
        if (c->r != eot)
            c->r->p = p;
        c->r = p;
        (p->p == eot ? eot->p : p->p->l == p ? p->p->l : p->p->r) = c;
        c->p = p->p;
        p->p = c;
    }
};

template <typename T, typename V, typename C, typename A>
bool operator==(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    auto i = t1.size();
    if (i != t2.size())
        return false;
    const auto c = static_cast<const T &>(t1).value_comp();
    for (auto it1 = t1.begin(), it2 = t2.begin(); i-- != 0; ++it1, ++it2) {
        if (c(*it1, *it2))
            return false;
        if (c(*it2, *it1))
            return false;
    }   return true;
}

template <typename T, typename V, typename C, typename A> inline
bool operator!=(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    return !(t1 == t2);
}

template <typename T, typename V, typename C, typename A>
bool operator<(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    const auto e1 = t1.end(), e2 = t2.end();
    const auto c = static_cast<const T &>(t1).value_comp();
    for (auto it1 = t1.begin(), it2 = t2.begin(); ; ++it1, ++it2) {
        if (it1 == e1)
            return it2 != e2;
        if (it2 == e2)
            return false;
        if (c(*it1, *it2))
            return true;
        if (c(*it2, *it1))
            return false;
    }
}

template <typename T, typename V, typename C, typename A> inline
bool operator>(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    return t2 < t1;
}

template <typename T, typename V, typename C, typename A> inline
bool operator>=(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    return !(t1 < t2);
}

template <typename T, typename V, typename C, typename A> inline
bool operator<=(const tree_base<T, V, C, A> &t1, const tree_base<T, V, C, A> &t2)
{
    return !(t2 < t1);
}

} // namespace glglT

#endif // GLGL_TREE_BASE_H
