﻿#ifndef GLGL_REL_OPS_H
#define GLGL_REL_OPS_H

namespace glglT {

namespace rel_ops {

template <typename T> inline
bool operator!=(const T &t1, const T &t2) noexcept(noexcept(t1 == t2))
{
    return !(t1 == t2);
}

template <typename T> inline
bool operator>=(const T &t1, const T &t2) noexcept(noexcept(t1 < t2))
{
    return !(t1 < t2);
}

template <typename T> inline
bool operator>(const T &t1, const T &t2) noexcept(noexcept(t1 < t2))
{
    return t2 < t1;
}

template <typename T> inline
bool operator<=(const T &t1, const T &t2) noexcept(noexcept(t1 < t2))
{
    return !(t2 < t1);
}

} // namespace rel_ops


} // namespace glglT


#endif // GLGL_REL_OPS_H
