#ifndef GLGL_HASH_H
#define GLGL_HASH_H

#include <cstdint>
#include <cstdio>
#include "glgl_functional_base.h"

namespace glglT {

template <typename> struct hash;

struct fast_hash_base {
    typedef true_type is_fast_hash_type;
};

struct slow_hash_base {
    typedef false_type is_fast_hash_type;
};

template <>
struct hash<bool>: function_type<size_t (bool)>, fast_hash_base {
    size_t operator()(bool v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<char>: function_type<size_t (char)>, fast_hash_base {
    size_t operator()(char v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<signed char>: function_type<size_t (signed char)>, fast_hash_base {
    size_t operator()(signed char v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<unsigned char>: function_type<size_t (unsigned char)>, fast_hash_base {
    size_t operator()(unsigned char v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<char16_t>: function_type<size_t (char16_t)>, fast_hash_base {
    size_t operator()(char16_t v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<char32_t>: function_type<size_t (char32_t)>, fast_hash_base {
    size_t operator()(char32_t v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<wchar_t>: function_type<size_t (wchar_t)>, fast_hash_base {
    size_t operator()(wchar_t v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<short>: function_type<size_t (short)>, fast_hash_base {
    size_t operator()(short v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<unsigned short>: function_type<size_t (unsigned short)>, fast_hash_base {
    size_t operator()(unsigned short v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<int>: function_type<size_t (int)>, fast_hash_base {
    size_t operator()(int v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<unsigned int>: function_type<size_t (unsigned int)>, fast_hash_base {
    size_t operator()(unsigned int v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<long>: function_type<size_t (long)>, fast_hash_base {
    size_t operator()(long v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<unsigned long>: function_type<size_t (unsigned long)>, fast_hash_base {
    size_t operator()(unsigned long v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<long long>: function_type<size_t (long long)>, fast_hash_base {
    size_t operator()(long long v) const noexcept
        { return static_cast<size_t>(v); }
};

template <>
struct hash<unsigned long long>: function_type<size_t (unsigned long long)>, fast_hash_base {
    size_t operator()(unsigned long long v) const noexcept
        { return static_cast<size_t>(v); }
};

template <typename T>
struct hash<T *>: function_type<size_t (T *)>, fast_hash_base {
    size_t operator()(T *p) const noexcept
        { return reinterpret_cast<size_t>(p); }
};

constexpr uint32_t FNVprime = 16777619;
constexpr uint32_t FNVoffset = 2166136261;

template <typename C>
struct FNVhash {
    static size_t _1a(const C *s, size_t len) noexcept
    {
        uint32_t h = FNVoffset;
        while (len-- != 0) {
#if CHAR_BIT == 8
            h ^= *s++;
#else
            h ^= uint8_t(*s++);
#endif // CHAR_BIT
            h *= FNVprime;
        }
        return static_cast<size_t>(h);
    }
};

template <> struct FNVhash<wchar_t> {
    static size_t _1a(const wchar_t *s, size_t len) noexcept
    {
        return FNVhash<char>::_1a(reinterpret_cast<const char *>(s),
                                  sizeof(wchar_t) * len);
    }
};

template <> struct FNVhash<char16_t> {
    static size_t _1a(const char16_t *s, size_t len) noexcept
    {
        return FNVhash<char>::_1a(reinterpret_cast<const char *>(s),
                                  sizeof(char16_t) * len);
    }
};

template <> struct FNVhash<char32_t> {
    static size_t _1a(const char32_t *s, size_t len) noexcept
    {
        return FNVhash<char>::_1a(reinterpret_cast<const char *>(s),
                                  sizeof(char32_t) * len);
    }
};

template <>
struct hash<float>: function_type<size_t (float)>, slow_hash_base {
    size_t operator()(float f) const noexcept
    {
        char buf[20];
        const size_t len = std::snprintf(buf, 20, "%.6e", f);
        return FNVhash<char>::_1a(buf, len);
    }
};

template <>
struct hash<double>: function_type<size_t (double)>, slow_hash_base {
    size_t operator()(double d) const noexcept
    {
        char buf[20];
        const size_t len = std::snprintf(buf, 20, "%.10e", d);
        return FNVhash<char>::_1a(buf, len);
    }
};

template <>
struct hash<long double>: function_type<size_t (long double)>, slow_hash_base {
    size_t operator()(double d) const noexcept
    {
        char buf[20];
        const size_t len = std::snprintf(buf, 20, "%.10le", d);
        return FNVhash<char>::_1a(buf, len);
    }
};

template <typename> struct is_fast_hash: false_type {};

template <typename T>
struct is_fast_hash<hash<T>>: integral_constant<bool,
    hash<T>::is_fast_hash_type::value && noexcept(declval<hash<T>>()(declval<T>()))>
{};

} // namespace glglT


#endif // GLGL_HASH_H
