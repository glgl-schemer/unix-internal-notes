#ifndef GLGL_TYPE_CATEGORY_H
#define GLGL_TYPE_CATEGORY_H

#include <cstddef>
#include "glgl_type_helper.h"

namespace glglT {

template <typename T>
struct remove_reference {
    typedef T type;
};

template <typename T>
struct remove_reference<T &> {
    typedef T type;
};

template <typename T>
struct remove_reference<T &&> {
    typedef T type;
};

template <typename T>
struct remove_const {
    typedef T type;
};

template <typename T>
struct remove_const<const T> {
    typedef T type;
};

template <typename T>
struct remove_volatile {
    typedef T type;
};

template <typename T>
struct remove_volatile<volatile T> {
    typedef T type;
};

template <typename T> struct remove_cv {
    typedef typename remove_volatile<typename remove_const<T>::type>::type type;
};

template <typename T>
struct remove_pointer {
    typedef T type;
};

template <typename T>
struct remove_pointer<T *> {
    typedef T type;
};

template <typename T>
struct make_signed;

template <> struct make_signed<bool> { typedef bool type; };

template <> struct make_signed<char> {
    typedef signed char type;
};

template <> struct make_signed<unsigned char> {
    typedef signed char type;
};

template <> struct make_signed<unsigned short> {
    typedef short type;
};

template <> struct make_signed<unsigned int> {
    typedef int type;
};

template <> struct make_signed<unsigned long> {
    typedef long type;
};

template <> struct make_signed<unsigned long long> {
    typedef long long type;
};

template <typename T>
struct make_unsigned;

template <> struct make_unsigned<bool> { typedef bool type; };

template <> struct make_unsigned<char> {
    typedef unsigned char type;
};

template <> struct make_unsigned<signed char> {
    typedef unsigned char type;
};

template <> struct make_unsigned<short> {
    typedef unsigned short type;
};

template <> struct make_unsigned<int> {
    typedef unsigned int type;
};

template <> struct make_unsigned<long> {
    typedef unsigned long type;
};

template <> struct make_unsigned<long long> {
    typedef unsigned long long type;
};

template <typename T>
struct remove_extent {
    typedef T type;
};

template <typename T>
struct remove_extent<T []> {
    typedef T type;
};

template <typename T, size_t N>
struct remove_extent<T[N]> {
    typedef T type;
};

template <typename T>
struct remove_all_extents {
    typedef T type;
};

template <typename T>
struct remove_all_extents<T []> {
    typedef typename remove_all_extents<T>::type type;
};

template <typename T, size_t N>
struct remove_all_extents<T[N]> {
    typedef typename remove_all_extents<T>::type type;
};

template <bool, typename = void>
struct enable_if {};

template <typename T>
struct enable_if<true, T> {
    typedef T type;
};

template <bool, typename T1, typename T2>
struct conditional {
    typedef T1 type;
};

template <typename T1, typename T2>
struct conditional<false, T1, T2> {
    typedef T2 type;
};

template <typename, typename> struct is_same_helper: false_type {};

template <typename T> struct is_same_helper<T, T>: true_type{};

template <typename T1, typename T2>
struct is_same: is_same_helper<
    typename remove_cv<T1>::type,
    typename remove_cv<T2>::type>
{};

template <typename T>
struct is_null_pointer: is_same<T, decltype(nullptr)> {};

template <typename> struct is_const: false_type {};

template <typename T> struct is_const<const T>: true_type {};

template <typename> struct is_volatile: false_type {};

template <typename T> struct is_volatile<volatile T>: true_type {};

template <typename> struct is_void_helper: false_type {};

template <> struct is_void_helper<void>: true_type {};

template <typename T>
struct is_void: is_void_helper<typename remove_cv<T>::type> {};

template <typename> struct is_integral_helper: false_type {};

template <> struct is_integral_helper<bool>: true_type {};

template <> struct is_integral_helper<char>: true_type {};

template <> struct is_integral_helper<wchar_t>: true_type {};

template <> struct is_integral_helper<char16_t>: true_type {};

template <> struct is_integral_helper<char32_t>: true_type {};

template <typename> struct is_signed_helper: false_type {};

template <> struct is_signed_helper<signed char>: true_type {};

template <> struct is_signed_helper<short>: true_type {};

template <> struct is_signed_helper<int>: true_type {};

template <> struct is_signed_helper<long>: true_type {};

template <> struct is_signed_helper<long long>: true_type {};

template <typename> struct is_unsigned_helper: false_type {};

template <> struct is_unsigned_helper<unsigned char>: true_type {};

template <> struct is_unsigned_helper<unsigned short>: true_type {};

template <> struct is_unsigned_helper<unsigned int>: true_type {};

template <> struct is_unsigned_helper<unsigned long>: true_type {};

template <> struct is_unsigned_helper<unsigned long long>: true_type {};

template <typename T>
struct is_signed: is_signed_helper<typename remove_cv<T>::type> {};

template <typename T>
struct is_unsigned: is_unsigned_helper<typename remove_cv<T>::type> {};

template <typename T>
struct is_integral: integral_constant<bool,
    is_signed<T>::value || is_unsigned<T>::value ||
    is_integral_helper<typename remove_cv<T>::type>::value>
{};

template <typename> struct is_float_helper: false_type {};

template <> struct is_float_helper<float>: true_type {};

template <> struct is_float_helper<double>: true_type {};

template <> struct is_float_helper<long double>: true_type {};

template <typename T>
struct is_floating_point: is_float_helper<typename remove_cv<T>::type> {};

template <typename T>
struct is_arithmetic: integral_constant<bool,
    is_integral<T>::value || is_floating_point<T>::value>
{};

template <typename T>
struct is_fundamental: integral_constant<bool,
    is_arithmetic<T>::value || is_void<T>::value ||
    is_same<T, decltype(nullptr)>::value>
{};

template <typename T>
struct is_compound:
    integral_constant<bool, !is_fundamental<T>::value>
{};

template <typename> struct is_lvalue_reference: false_type {};

template <typename T> struct is_lvalue_reference<T &>: true_type {};

template <typename> struct is_rvalue_reference: false_type {};

template <typename T> struct is_rvalue_reference<T &&>: true_type {};

template <typename T>
struct is_reference: integral_constant<bool,
    is_lvalue_reference<T>::value || is_rvalue_reference<T>::value>
{};

template <typename> struct is_array: false_type {};

template <typename T> struct is_array<T []>: true_type {};

template <typename T, size_t N> struct is_array<T [N]>: true_type {};

template <typename> struct is_pointer: false_type {};

template <typename T> struct is_pointer<T *>: true_type {};

template <typename> struct is_function: false_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...)>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...)>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) volatile>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) volatile>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const volatile>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const volatile>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) volatile &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) volatile &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const volatile &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const volatile &>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) volatile &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) volatile &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args...) const volatile &&>: true_type {};

template <typename ResT, typename... Args>
struct is_function<ResT (Args..., ...) const volatile &&>: true_type {};

template <typename> struct is_member_function_pointer: false_type {};

template <typename T, typename ClassT>
struct is_member_function_pointer<T ClassT::*>: is_function<T> {};

template <typename> struct is_member_object_pointer: false_type {};

template <typename T, typename ClassT>
struct is_member_object_pointer<T ClassT::*>:
    integral_constant<bool, !is_function<T>::value>
{};

template <typename> struct is_member_pointer: false_type {};

template <typename T, typename ClassT>
struct is_member_pointer<T ClassT::*>: true_type {};

template <typename T>
struct is_object: integral_constant<bool,
    !is_function<T>::value && !is_reference<T>::value &&
    !is_void<T>::value>
{};

template <typename T>
struct is_empty: integral_constant<bool, sizeof(T) == 1> {};

template <typename, typename>
constexpr void test_match(...);

template <typename E, typename I> constexpr
auto test_match(int) -> typename conditional<
    sizeof(E) == sizeof(I) && (E(-1) < E(0) == I(-1) < I(0)), I, void
>::type;

template <typename E, typename NOW, typename... Is>
struct find_match {
    typedef NOW type;
};

template <typename E, typename I, typename... Is>
struct find_match<E, void, I, Is...>: find_match<E,
    decltype(test_match<E, I>(0)),
Is...> {};

template <typename E, typename I>
struct find_match<E, void, I> {
    typedef decltype(test_match<E, I>(0)) type;
};

template <typename E>
struct uderlying_type {
    typedef typename find_match<
        E, void, bool, char, signed char, unsigned char,
        short, int, long, long long,
        unsigned short, unsigned int, unsigned long, unsigned long long
    >::type type;
};

template <typename T>
struct is_enum: integral_constant<bool, !is_integral<T>::value &&
    !is_void_helper<typename uderlying_type
        <typename remove_reference<typename remove_cv<T>::type>::type>::type
>::value> {};

template <typename T, typename I = typename uderlying_type<T>::type>
struct make_signed_helper {
    typedef typename make_signed<I>::type type;
};

template <typename T>
struct make_signed_helper<T, void> { typedef T type; };

template <typename T> struct make_signed: make_signed_helper<T> {};

template <typename T, typename I = typename uderlying_type<T>::type>
struct make_unsigned_helper {
    typedef typename make_unsigned<I>::type type;
};

template <typename T>
struct make_unsigned_helper<T, void> { typedef T type; };

template <typename T> struct make_unsigned: make_unsigned_helper<T> {};

template <typename T>
struct is_scalar: integral_constant<bool,
    is_arithmetic<T>::value || is_enum<T>::value || is_pointer<T>::value ||
    is_member_pointer<T>::value || is_null_pointer<T>::value
> {};

template <typename, size_t = 0>
struct extent: integral_constant<size_t, 0> {};

template <typename T, size_t N, size_t Which>
struct extent<T[N], Which>: integral_constant<size_t,
    Which == 0 ? N : extent<T, Which - 1>::value>
{};

template <typename T, size_t Which>
struct extent<T[], Which>: integral_constant<size_t,
    Which == 0 ? 0 : extent<T, Which - 1>::value>
{};

template <typename>
struct rank: integral_constant<size_t, 0> {};

template <typename T>
struct rank<T[]>: integral_constant<size_t, rank<T>::value + 1> {};

template <typename T, size_t N>
struct rank<T[N]>: integral_constant<size_t, rank<T>::value + 1> {};

template <typename T, bool = !is_void<T>::value>
struct add_lvalue_reference {
    typedef T & type;
};

template <typename T>
struct add_lvalue_reference<T, false> {
    typedef T type;
};

template <typename T, bool = !is_void<T>::value>
struct add_rvalue_reference {
    typedef T && type;
};

template <typename T>
struct add_rvalue_reference<T, false> {
    typedef T type;
};

template <typename T>
struct add_const {
    typedef const T type;
};

template <typename T>
struct add_const<const T> {
    typedef const T type;
};

template <typename T>
struct add_volatile {
    typedef volatile T type;
};

template <typename T>
struct add_volatile<volatile T> {
    typedef volatile T type;
};

template <typename T> struct add_cv {
    typedef typename add_volatile<typename add_const<T>::type>::type type;
};

template <typename T>
struct add_pointer {
    typedef T * type;
};

template <typename T>
struct add_pointer<T &> {
    typedef T * type;
};

template <typename T>
struct add_pointer<T &&> {
    typedef T * type;
};

} // namespace glglT

#endif // GLGL_TYPE_CATEGORY_H
