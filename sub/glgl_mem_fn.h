﻿#ifndef GLGL_MEN_FN_H
#define GLGL_MEN_FN_H

#include "glgl_functional_base.h"

namespace glglT {

template <typename T>
struct mem_fn_class: function_type<T>, mem_fn_base<T> {
    explicit constexpr mem_fn_class(T p) noexcept: mem_fn_base<T>(p) {}
};

template <typename T> inline
mem_fn_class<T> mem_fn(T pmf) noexcept
{
    return mem_fn_class<T>(pmf);
}

} // namespace glglT

#endif // GLGL_MEN_FN_H
