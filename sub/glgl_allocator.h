#ifndef GLGL_ALLOCATIOR_H
#define GLGL_ALLOCATIOR_H

#include <memory>
#include "glgl_utility_function.h"

namespace glglT {

using std::allocator;

template <typename A>
struct alloc_use_ptrdiff_t {
    template <typename T> static constexpr
    auto value(const T &e, const T &b) -> decltype(e - b) { return e - b; }
};

template <typename D>
struct alloc_use_ptrdiff_t<allocator<D>> {
    template <typename T> static constexpr
    auto value(const T &e, const T &b) -> decltype(e - b) { return 0; }
};

} // namespace glglT


#endif // GLGL_ALLOCATIOR_H
