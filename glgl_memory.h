#ifndef GLGL_MEMORY_H
#define GLGL_MEMORY_H

#include "sub/glgl_allocator.h"
#include "sub/glgl_allocator_traits.h"
#include "sub/glgl_memory_tool.h"
#include "sub/glgl_shared_ptr.h"
#include "sub/glgl_unique_ptr.h"

namespace glglT {

template <typename T>
template <typename O, typename D> inline
shared_ptr<T>::shared_ptr(unique_ptr<O, D> &&p):
    ptr(p.ptr), state(new shared_state_t{1, 0,make_deleter(p.ptr, glglT::move(p.deleter))})
{ p.ptr = nullptr; }

template <typename T>
template <typename O, typename D> inline
shared_ptr<T> &shared_ptr<T>::operator=(unique_ptr<O, D> &&p)
{
    if (state) {
        if (state->load_own_count() == 1) {
            (*(state->d))(ptr);
            state->sub_own_count()
            if (state->load_ref_count() == 0)
                delete state;
        } else
            state->sub_own_count();
    }
    ptr = p.ptr;
    state = new shared_state_t{1, 0, make_deleter(p.ptr, glglT::move(p.deleter))};
    p.ptr = nullptr;
}

} // namespace glglT

#endif // GLGL_MEMORY_H
