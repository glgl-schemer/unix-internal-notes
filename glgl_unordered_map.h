#ifndef GLGL_UNORDERED_MAP
#define GLGL_UNORDERED_MAP

#include <stdexcept>
#include "sub/glgl_bucket_base.h"
#include "glgl_tuple.h"

namespace glglT {

template <typename K, typename T, typename Hash = hash<K>,
          typename Eq = equal_to<K>, typename Alloc = allocator<pair<const K, T>>>
class unordered_map:
    public bucket_base<unordered_map<K, T, Hash, Eq, Alloc>,
                       pair<const K, T>, Hash, Eq, Alloc> {
    friend class bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        rebind_ptr rebind_ptr;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        rebind_traits rebind_traits;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        insert_return_type insert_return_type;
public:
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        key_type key_type;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        value_type value_type;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        size_type size_type;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        iterator iterator;
    typedef typename bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::
        const_iterator const_iterator;

    explicit unordered_map(size_type l, const Hash &hh = Hash(), const Eq &e = Eq(),
                           const Alloc &ac = Alloc()):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(l, hh, e, ac) {}

    explicit unordered_map(const Alloc &ac = Alloc()):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(ac) {}

    template <typename It>
    unordered_map(It ib, It ie, size_type l = 11, const Hash &hh = Hash(),
                  const Eq &e = Eq(), const Alloc &ac = Alloc()):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(ib, ie, l, hh, e, ac) {}

    unordered_map(const unordered_map &o):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(o) {}

    unordered_map(const unordered_map &o, const Alloc &ac):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(o, ac) {}

    unordered_map(unordered_map &&o):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(glglT::move(o)) {}

    unordered_map(unordered_map &&o, const Alloc &ac):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(glglT::move(o), ac) {}

    unordered_map(std::initializer_list<pair<const K, T>> il, size_type l = 11,
                  const Hash &hh = Hash(), const Eq &e = Eq(), const Alloc &ac = Alloc()):
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>(il, l, hh, e, ac) {}

    unordered_map &operator=(const unordered_map &o)
    {
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::operator=(o);
        return *this;
    }

    unordered_map &operator=(unordered_map &&o)
    {
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    unordered_map &operator=(std::initializer_list<pair<const K, T>> il)
    {
        bucket_base<unordered_map, pair<const K, T>, Hash, Eq, Alloc>::operator=(il);
        return *this;
    }

    T &at(const key_type &k)
    {
        const auto it = this->find(k);
        if (it == this->end())
            throw std::out_of_range("not found in unordered_map");
        return it->second;
    }

    const T &at(const key_type &k) const
    {
        const auto it = this->find(k);
        if (it == this->end())
            throw std::out_of_range("not found in unordered_map");
        return it->second;
    }

    T &operator[](const key_type &k)
    {
        auto it = this->find(k);
        if (it == this->end())
            it = this->emplace_hint(it, glglT::piecewise_construct,
                                    glglT::forward_as_tuple(k), glglT::tuple<>());
        return it->second;
    }

    T &operator[](key_type &&k)
    {
        auto it = this->find(k);
        if (it == this->end())
            it = this->emplace_hint(it, glglT::piecewise_construct,
                                    glglT::forward_as_tuple(glglT::move(k)),
                                    glglT::tuple<>());
        return it->second;
    }

    size_type count(const key_type &k) const
    {
        return this->find(k) != this->end();
    }

    pair<iterator, iterator> equal_range(const key_type &k)
    {
        auto itr = this->find(k);
        if (itr != this->end()) {
            const auto it1 = itr;
            return glglT::make_pair(it1, ++itr);
        } else
            return glglT::make_pair(itr, itr);
    }
private:
    static iterator get(insert_return_type &&r) noexcept
    {
        return glglT::move(r.first);
    }

    static const key_type &get_key(const value_type &v) noexcept { return v.first; }

    insert_return_type insert_equal(rebind_ptr node, size_t hval)
    {
        size_t which;
        if (this->len != 0) {
            which = hval % this->len;
            if (this->table[which].p != nullptr) {
                auto p = this->table[which].p;
                do {
                    if (this->eq(p->v.first, node->v.first)) {
                        rebind_traits::destroy(this->a, node);
                        rebind_traits::deallocate(this->a, node, 1);
                        return glglT::make_pair(iterator(this->table, this->len,
                                                         which, p), false);
                    }   p = p->n;
                } while (p != nullptr);
            }
        }
        if (this->len * this->factor < ++this->ecnt) {
            const auto new_len = glglT::bucket_primes(this->next_i);
            if (this->len != new_len) {
                try {
                    this->rehash_aux(new_len);
                } catch(...) {
                    --this->ecnt;
                    throw;
                }
                this->len = new_len;
                which = hval % new_len;
                ++this->next_i;
            }
        }
        if (this->table[which].p == nullptr) {
            this->table[which].p = node;
            this->table[which].n = this->first_have;
            if (this->first_have != this->len)
                this->table[this->first_have].b = which;
            this->first_have = which;
            node->n = nullptr;
        } else
            node->n = this->table[which].p;
        this->table[which].p = node;
        ++this->table[which].c;
        return glglT::make_pair(iterator(this->table, this->len, which, node), true);
    }

    void void_insert_equal(rebind_ptr node, size_t hval)
    {
        const auto which = hval % this->len;
        if (this->table[which].p == nullptr) {
            this->table[which].p = node;
            this->table[which].n = this->first_have;
            if (this->first_have != this->len)
                this->table[this->first_have].b = which;
            this->first_have = which;
            node->n = nullptr;
        } else {
            auto p = this->table[which].p;
            do {
                if (this->eq(p->v.first, node->v.first)) {
                    rebind_traits::destroy(this->a, node);
                    rebind_traits::deallocate(this->a, node, 1);
                    return;
                }   p = p->n;
            } while (p != nullptr);
            node->n = this->table[which].p;
        }
        this->table[which].p = node;
        ++this->table[which].c;
        ++this->ecnt;
    }
};

template <typename K, typename T, typename H, typename E, typename A> inline
void swap(unordered_map<K, T, H, E, A> &b1, unordered_map<K, T, H, E, A> &b2)
{
    b1.swap(b2);
}

template <typename K, typename T, typename Hash = hash<K>,
          typename Eq = equal_to<K>, typename Alloc = allocator<pair<const K, T>>>
class unordered_multimap:
    public bucket_base<unordered_multimap<K, T, Hash, Eq, Alloc>,
                       pair<const K, T>, Hash, Eq, Alloc> {
    friend class bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        rebind_ptr rebind_ptr;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        rebind_traits rebind_traits;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        insert_return_type insert_return_type;
public:
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        key_type key_type;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        value_type value_type;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        size_type size_type;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        iterator iterator;
    typedef typename bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::
        const_iterator const_iterator;

    explicit unordered_multimap(size_type l, const Hash &hh = Hash(), const Eq &e = Eq(),
                                const Alloc &ac = Alloc()):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(l, hh, e, ac) {}

    explicit unordered_multimap(const Alloc &ac = Alloc()):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(ac) {}

    template <typename It>
    unordered_multimap(It ib, It ie, size_type l = 11, const Hash &hh = Hash(),
                       const Eq &e = Eq(), const Alloc &ac = Alloc()):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(ib, ie, l, hh, e, ac) {}

    unordered_multimap(const unordered_multimap &o):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(o) {}

    unordered_multimap(const unordered_multimap &o, const Alloc &ac):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(o, ac) {}

    unordered_multimap(unordered_multimap &&o):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(glglT::move(o)) {}

    unordered_multimap(unordered_multimap &&o, const Alloc &ac):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(glglT::move(o), ac) {}

    unordered_multimap(std::initializer_list<pair<const K, T>> il, size_type l = 11,
                       const Hash &hh = Hash(), const Eq &e = Eq(), const Alloc &ac = Alloc()):
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>(il, l, hh, e, ac) {}

    unordered_multimap &operator=(const unordered_multimap &o)
    {
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::operator=(o);
        return *this;
    }

    unordered_multimap &operator=(unordered_multimap &&o)
    {
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    unordered_multimap &operator=(std::initializer_list<pair<const K, T>> il)
    {
        bucket_base<unordered_multimap, pair<const K, T>, Hash, Eq, Alloc>::operator=(il);
        return *this;
    }

    size_type count(const key_type &k) const
    {
        const auto itr = this->find(k);
        if (itr == this->end())
            return 0;
        size_type i = 1;
        for (auto p = itr.base()->n; p != nullptr && this->eq(p->v.first, k); ++i);
        return i;
    }

    pair<iterator, iterator> equal_range(const key_type &k)
    {
        auto itr = this->find(k);
        if (itr != this->end()) {
            const auto it1 = itr;
            for (auto p = itr.base()->n; p != nullptr && this->eq(p->v.first, k); p = p->n)
                ++itr;
            return glglT::make_pair(it1, ++itr);
        } else
            return glglT::make_pair(itr, itr);
    }
private:
    static iterator get(insert_return_type &&r) noexcept { return glglT::move(r); }

    static const key_type &get_key(const value_type &v) noexcept { return v.first; }

    insert_return_type insert_equal(rebind_ptr node, size_t hval)
    {
        if (this->len * this->factor < ++this->ecnt) {
            const auto new_len = glglT::bucket_primes(this->next_i);
            if (this->len != new_len) {
                try {
                    this->rehash_aux(new_len);
                } catch(...) {
                    --this->ecnt;
                    throw;
                }
                this->len = new_len;
                ++this->next_i;
            }
        }
        const auto which = hval % this->len;
        if (this->table[which].p == nullptr) {
            this->table[which].p = node;
            this->table[which].n = this->first_have;
            if (this->first_have != this->len)
                this->table[this->first_have].b = which;
            this->first_have = which;
            node->n = nullptr;
        } else {
            for (auto p = this->table[which].p, n = p->n; ; p = n, n = n->n) {
                try {
                    if (n == nullptr || this->eq(p->v.first, node->v.first)) {
                        p->n = node; node->n = n;
                        break;
                    }
                } catch(...) {
                    --this->ecnt;
                    throw;
                }
            }
        }
        ++this->table[which].c;
        return iterator(this->table, this->len, which, node);
    }

    void void_insert_equal(rebind_ptr node, size_t hval)
    {
        ++this->ecnt;
        const auto which = hval % this->len;
        if (this->table[which].p == nullptr) {
            this->table[which].p = node;
            this->table[which].n = this->first_have;
            if (this->first_have != this->len)
                this->table[this->first_have].b = which;
            this->first_have = which;
            node->n = nullptr;
        } else {
            for (auto p = this->table[which].p, n = p->n; ; p = n, n = n->n) {
                try {
                    if (n == nullptr || this->eq(p->v.first, node->v.first)) {
                        p->n = node; node->n = n;
                        break;
                    }
                } catch(...) {
                    --this->ecnt;
                    throw;
                }
            }
        }
        ++this->table[which].c;
    }
};

template <typename K, typename T, typename H, typename E, typename A> inline
void swap(unordered_multimap<K, T, H, E, A> &b1, unordered_multimap<K, H, T, E, A> &b2)
{
    b1.swap(b2);
}

} // namespace glglT

#endif // GLGL_UNORDERED_MAP
