#ifndef GLGL_CONDITION_VARIABLE_H
#define GLGL_CONDITION_VARIABLE_H

#include "glgl_mutex.h"

namespace glglT {

enum class cv_status: bool { timeout = false, no_timeout = true };

class condition_variable_any {
    struct cv_aux {
        cv_aux *next;
        std::atomic_bool already;
    };

    struct timed_cv_aux {
        cv_aux elem;
        timed_cv_aux *next;
    };

    std::atomic<cv_aux *> head;
    std::atomic<timed_cv_aux *> need_free;
public:
    constexpr condition_variable_any(): head(nullptr), need_free(nullptr) {}
    condition_variable_any(const condition_variable_any &) = delete;
    ~condition_variable_any()
    {
        for (timed_cv_aux *p = need_free.load(std::memory_order_relaxed), *next; p != nullptr;) {
            next = p->next;
            delete p;
            p = next;
        }
    }

    void notify_one() noexcept
    {
        while (true) {
            cv_aux *p = head.load(std::memory_order_acquire);
            if (p) {
                while (!head.compare_exchange_weak(p, p->next, std::memory_order_acq_rel, std::memory_order_acquire));
                if (p->already.exchange(true, std::memory_order_release))
                    continue;
            }
            return;
        }
    }

    void notify_all() noexcept
    {
        for (cv_aux *p = head.exchange(nullptr, std::memory_order_acq_rel), *next; p != nullptr;) {
            next = p->next;
            p->already.store(true, std::memory_order_release);
            p = next;
        }
    }

    template <typename Lock, typename Pred>
    void wait(Lock &lock, Pred pred) { while (!pred()) this->wait(lock); }
    template <typename Lock>
    void wait(Lock &lock)
    {
        lock.unlock();
        cv_aux elem{ head.load(std::memory_order_acquire), ATOMIC_VAR_INIT(false) };
        while (!head.compare_exchange_weak(elem.next, &elem, std::memory_order_acq_rel, std::memory_order_acquire));
        while (!elem.already.load(std::memory_order_acquire))
            std::this_thread::yield();
        lock.lock();
    }

    template <typename Lock, typename R, typename P>
    cv_status wait_for(Lock &lock, const std::chrono::duration<R, P> &d)
    {
        return this->wait_until(lock, std::chrono::system_clock::now() + d);
    }

    template <typename Lock, typename R, typename P, typename Pred>
    bool wait_for(Lock &lock, const std::chrono::duration<R, P> &d, Pred pred)
    {
        return this->wait_until(lock, std::chrono::system_clock::now() + d, glglT::move(pred));
    }

    template <typename Lock, typename C, typename D>
    cv_status wait_until(Lock &lock, const std::chrono::time_point<C, D> &p)
    {
        timed_cv_aux * cv = ::new (std::nothrow) timed_cv_aux{
            { head.load(std::memory_order_acquire), ATOMIC_VAR_INIT(false) }, nullptr
        };
        if (cv == nullptr)
            throw std::system_error(int(std::errc::not_enough_memory), std::generic_category());
        while (!head.compare_exchange_weak(cv->elem.next, &(cv->elem), std::memory_order_acq_rel, std::memory_order_acquire));
        std::exception_ptr eptr = nullptr;
        try {
            std::this_thread::sleep_until(p);
            if (cv->elem.already.exchange(true, std::memory_order_acquire))
                return delete cv, cv_status::no_timeout;
        } catch(...) {
            eptr = std::current_exception();
        }
        cv->next = need_free.load(std::memory_order_acquire);
        while (!need_free.compare_exchange_weak(cv->next, cv, std::memory_order_acq_rel, std::memory_order_acquire));
        if (eptr != nullptr)
            std::rethrow_exception(eptr);
        return cv_status::timeout;
    }

    template <typename Lock, typename R, typename P, typename Pred>
    bool wait_until(Lock &lock, const std::chrono::time_point<R, P> &p, Pred pred)
    {
        this->wait_until(lock, p);
        return pred();
    }
};

inline void notify_all_at_thread_exit(condition_variable_any &cv, unique_lock<glglT::mutex> lock)
{
    static thread_local struct aux_at_thread_exit {
        struct lock_aux {
            unique_lock<glglT::mutex> lock;
            condition_variable_any *cv_p;
            lock_aux *next;
        } *locks;

        aux_at_thread_exit(): locks(nullptr) {}

        ~aux_at_thread_exit()
        {
            for (lock_aux *p = locks, *next; p != nullptr; p = next) {
                auto cv_p = p->cv_p;
                next = p->next;
                delete p;
                cv_p->notify_all();
            }
        }
    } aux;

    auto lock_p = ::new (std::nothrow) aux_at_thread_exit::lock_aux{
        glglT::move(lock), &cv, nullptr
    };
    if (lock_p == nullptr)
        throw std::system_error(int(std::errc::not_enough_memory), std::generic_category());
    lock_p->next = aux.locks;
    aux.locks = lock_p;
}

} // namespace glglT

#endif // GLGL_CONDITION_VARIABLE_H
